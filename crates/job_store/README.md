# `eternaltwin_job_store`

This crate provides implementations for the `JobStore` interface. Its role is to store the state of background jobs.
