import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { SharedModule } from "../../shared/shared.module.mjs";
import { TwinoidHomeView } from "./twinoid-home.view.mjs";
import { TwinoidRoutingModule } from "./twinoid-routing.module.mjs";
import { TwinoidUserComponent } from "./twinoid-user.component.mjs";
import { TwinoidUserView } from "./twinoid-user.view.mjs";

@NgModule({
  declarations: [TwinoidHomeView, TwinoidUserComponent, TwinoidUserView],
  imports: [
    CommonModule,
    TwinoidRoutingModule,
    SharedModule,
  ],
})
export class TwinoidModule {
}
