# 2022-09: This Month In Eternaltwin #12

[Eternaltwin](https://eternaltwin.org) is a project with the goal to preserve
Motion Twin's games and player communities.

# Eternaltwin

All the games were moved to the new deployment system. Project leaders can
now upload new versions any time they wish.

# eMush

Welcome to this section dedicated to [eMush](https://emush.eternaltwin.org/),
the project to save Mush.

## What's new?

The last time we shared some news was in July-August 2022. We just started to
work on the diseases for the **Disease Update**.

![eMush1](./emush1.png)

We continued to work on this update. We are now happy to announce that **all the
diseases** - physical, psychological or wounds - **are now working**!

![eMush2](./emush2.png)

Another big feature was the addition of an **administrator panel**.

![eMush3](./emush3.png)

It will let us manage game settings more easily: we won't have to reset the
database each time.

This brings us closer to our medium term goal: **an open alpha test period**. 🤞

## Coming next

Now that diseases are done, we're working on how to actually get them. Most
ways are already complete: **random chance, food poisoning, STIs, spores...**

![eMush4](./emush4.png)

The main missing piece are wounds caused by **weapons**. It will be our next
area of focus for the update.

We also still have to provide tooltips about disease symptoms (it may be useful)
and then we'll be done with the **Disease Update**!

(So a new alpha is pretty close. 👀)

## Great! When? How to play?

We can't provide a date at this time. We'll keep you informed as soon as we
have fixed a date.

If you can't wait, the best way to keep in touch and follow progress is to join
[the Eternaltwin discord](https://discord.gg/SMJavjs3gk). It also shows your
support!

eMush is an Open Source project, our progress is also [available on Gitlab](https://gitlab.com/eternaltwin/mush/mush/-/milestones/8#tab-issues).

Thank you for your attention, and see you soon aboard the Daedalus!

# Neoparc

It's the unofficial start of the **clan war** on [Neoparc](https://neoparc.eternaltwin.org/)!

Hello Dinoz master!

We have some updates for your beloved clan. You may now start to fill your totems
and score points for the ongoing clan war.

Be careful, this first clan war may still need som adjustments and balancing.
There may be a couple issues at the beginning. Until the next update, I wish
you good luck with your diggings!

Watch out, war is not the only danger. There are rumours about spooky spirits
roaming over Dinoland! Join us the **Halloween event**!

Oh, one last thing: you may now download your Dinoz data from the "My Account" menu.

Have fun.

# MyHordes

Play to [MyHordes](https://myhordes.eu/).

_radio crackle_

Hello everyone, I hope all is well for you, and especially think of going out wi _scrambled_

![MyHordes - Wireless technology ad](./myhordes.png)

The hunt for bugs continues, our team of developers, accompanied by 2 guardians
and a scout, travel all over the World-Beyond to provide you with the most
_scrambled_

Preparations for the 15th season are underway, be sure it's goi _scrambled_

In addition, I am told that the shamans have finally understood that watering
the same area twice is useless! _radio crackle_

_cough_ _radio crackle_

# Closing words

You can send [your messages for October](https://gitlab.com/eternaltwin/etwin/-/issues/54).

This article was edited by: Demurgos, Dylan57, Evian, Fer, Jonathan, Patate404, and Schroedinger.
