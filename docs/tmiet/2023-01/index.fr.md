# 2023-01 : Ce mois-ci sur Eternaltwin n°16

[Eternaltwin](https://eternaltwin.org) est un projet visant à préserver les jeux
et communautés de joueurs de Motion Twin.

# Eternaltwin

_Evian_ a corrigé Skywar dans [l'application Eternaltwin](https://eternaltwin.org/docs/desktop),
version 0.6.6.

# eMush

Bienvenue à tous dans cette section de consacrés à
[🍄 eMush](https://emush.eternaltwin.org/), le projet de sauvegarde de Mush.

## Quoi de neuf ?

L'année dernière, nous vous parlions de la possibilité pour les administrateurs de eMush de créer
des configurations pour des parties personnalisées.

![eMush](./emush0.fr.png)

Nous avons le plaisir de vous annoncer que cette fonctionnalité est quasiment
fonctionnelle ! 🎉 Il s'agit d'un grand pas pour lancer l'open alpha de eMush.

En effet, grâce à ce formidable panel admin nous serons capables d'ajouter de
nouveaux objets, personnages, armes... sans réinitialiser la base de données à
chaque mise à jour !

Nous avons ainsi pu nous focaliser sur la prochaine étape, afficher vos futurs
exploits. Une image valant mille mots, nous vous laissons juges de notre avancée :

![eMush](./emush1.fr.png)

![eMush](./emush2.fr.png)

![eMush](./emush3.fr.png)

Pas mal, non ? 😄

## Prochainement

Pour rappel, voici les objectifs que nous nous étions donnés pour la prochaine
version d'eMush :

- 🇬🇧 la traduction du jeu en anglais ✅
- 🗃️ l'archivage des vaisseaux terminés, afin de garder les traces de vos exploits 🚧
- 💾 les migrations de notre base de données, afin de déployer de nouvelles
  fonctionnalités sans détruire les vaisseaux en cours 🚧

Comme vous pouvez le constater, nous sommes très heureux de voir que ces
objectifs sont en passe d'être complétés.

Nous devons maintenant résoudre quelques légers problèmes techniques, mais nous
n'avons jamais été aussi proches de l'open alpha.

## Super ! Quand ça ?

Nous continuerons à vous communiquer nos avancées à travers cette gazette,
mais le meilleur moyen d'être informé reste de rejoindre [le discord d'Eternaltwin](https://discord.gg/SMJavjs3gk).

eMush étant un projet open-source, nos avancées sont également disponibles sur GitLab,
en particulier [l'avancement de la mise à jour **Spores for All!**](https://gitlab.com/eternaltwin/mush/mush/-/milestones/12#tab-issues).

Merci de nous avoir lu, et à bientôt sur le Daedalus ! ❤️

# MyHordes

Jouer à [MyHordes](https://myhordes.eu/).

_crépitement radio_

Bonjour tout le monde, j'espère que tout va bien pour vous pendant cette saison
froide _brouillé_

![MyHordes - Pub pour technologie sans fil](./myhordes.fr.png)

Grâce au travail acharné de nos développeurs, la saison 15 est imminente.
Pour rappel _brouillé_ tout sera prêt pour le 11 février. Alors préparez-vous à
_brouillé_

Vous serez également heureux de constater que l'Autel de la rédemption n'est
pas endommagé et rouillé comme toujours, après tout, nous ne pouvons pas laisser
ces morts-vivants tout gâcher. _crépitement radio_

Un petit mot sur les 43 actes de cannibalistes... _brouillé_ Mmmmh... Non rien
à ajouter.

Je pense qu'il n'y a rien d'autre, Ben ? _brouillé_ Mais ferme cette putain de
porte ! _brouillé_

# Eternal DinoRPG

Bonjour tout le monde et merci de votre attention pour cette partie dédiée à [Eternal DinoRPG](https://dinorpg.eternaltwin.org/).

La version 0.5.0 est sortie il y a quelques jours déjà. Celle vous a amené le
Pigmou ainsi que les premières missions et nous comptons déjà environ 60 Pigmou
et 30 missions complétées !

La prochaine version est déjà en cours de conception et n'apportera pas de
nouvelles fonctionnalités mais du contenu afin que vous puissiez parler à
d'autres PNJ et faire d'autres missions !

# Eternal Kingdom

Bonjour tout le monde. Voici des nouvelles d'[EternalKingdom](https://kingdom.eternaltwin.org/) !

Suite au déploiement de la version pre-Alpha 5 qui a apporté de nombreux
changements sur le site. ([voir les détails](https://eternaltwin.org/forum/threads/9074673d-a530-4780-9fc2-4ebc8743152a))
Vous êtes une centaine de joueurs à l'avoir rejoint pour ensuite tester la ville
et la carte. Merci pour tout cet engouement !

Un nouveau membre nous a rejoints : _NSunshine_, il travaille sur une mis à jour
de la page de transfert d'unités. _Talsi_ travaille sur la refonte de la carte
(nouveau moteur graphique, visuel évolutif des villes, généraux, ...). It travaille
aussi sur les noms de villes traduits. Nous travaillons sur la mise à jour
Alpha 5 qui se concentrera sur les généraux (transfert d'unités, déplacements),
la santé et le commerce.

Je tiens à remercier tous nos testeurs pour leurs messages de soutien et
leurs rapports de bug ponctuels ! À très bientôt !

# Eternalfest

C'est bientôt le 10ème anniversaire d'[Eternalfest](https://eternalfest.net),
le projet a commencé le 5 mars 2013. Pour fêter ça, on a sorti une _énorme_
mise à jour en janvier.

**Vous pouvez désormais voir vos objets récupérés au fil des années dans le
nouvel onglet _Mon Profil_.** C'était la fonctionnalité la plus demandée, elle
est enfin là ! 🎉 Nous continuerons d'améliorer cette page au fil des
prochaines mises à jour.

L'autre ajout majeur est la gestion des traductions en jeu. Le premier
jeu utilisant cette fonctionnalité sortira en février.

Il y a tas d'autres fonctionnalités. Vous pouvez ajouter les jeux aux favoris,
les jeux peuvent avoir plusieurs versions en parallèle (ex. "main" et "beta"),
il est possible de planifier plusieurs mises à jour à l'avance, etc.

Ces fonctionnalités sont possibles grâce à _Demurgos_ et _Moulins_ qui ont
refait le système de gestion des jeux pour être plus flexible. C'était une tâche
de longue date qui est enfin finie. Cela permettra de finir la migration
d'Eternalfest vers le langage Rust. Ce nouveau système sera aussi importé dans
Eternaltwin et servira de base pour une intégration plus avancée des jeux
(profiles de jeu, récompenses).

# Neoparc

Bonjour [maîtres Dinoz](https://neoparc.eternaltwin.org) !

La première Guerre des Clans est officiellement démarrée ! Les détails
concernant son fonctionnement se trouvent dans la section aide du site.
Nous espérons que vous y trouverez gloire et richesse ! Bonne chance à vous
tous, et soyez sans pitié !

![Neoparc - Guerre des clans](./neoparc.png)

Nous avons publié plusieurs correctifs, en particulier pour le poste de missions
de Dinoville. Un Pruniac sera désormais automatiquement ajouté à l'inventaire
lorsque vous ferez éclore un œuf. Les récompenses de raid ont aussi été
réduites (ex. plus de potions d'Irma gratuites).

Finalement, la compétence Ingénieur a maintenant une utilité. Elle permet au
Dinoz qui la maîtrise d'avoir de meilleures durabilités sur les bières de
Dinojak, les charmes prismatiks et les focus vendus à la Caverne de l'Anomalie.

# Closing words

Vous pouvez envoyer [vos messages pour
l'édition de février](https://gitlab.com/eternaltwin/etwin/-/issues/58).

Cet article a été édité par : Bibni, Biosha, Demurgos, Dylan57, Evian,
Fer, Jonathan, Patate404 et Schroedinger.
