import {DOCUMENT} from "@angular/common";
import {Component, Inject, OnInit} from "@angular/core";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {AuthContext} from "@eternaltwin/core/auth/auth-context";
import {AuthType} from "@eternaltwin/core/auth/auth-type";
import {ObjectType} from "@eternaltwin/core/core/object-type";
import {HammerfestServer} from "@eternaltwin/core/hammerfest/hammerfest-server";
import {LinkToHammerfestMethod} from "@eternaltwin/core/user/link-to-hammerfest-method";
import {LinkToTwinoidMethod} from "@eternaltwin/core/user/link-to-twinoid-method";
import {MaybeCompleteUser} from "@eternaltwin/core/user/maybe-complete-user";
import {$RawUsername, RawUsername} from "@eternaltwin/core/user/raw-username";
import {$UserDisplayName, UserDisplayName} from "@eternaltwin/core/user/user-display-name";
import {UserId} from "@eternaltwin/core/user/user-id";
import {$Username, Username} from "@eternaltwin/core/user/username";
import {BehaviorSubject, NEVER as RX_NEVER, Observable, of as rxOf, Subscription} from "rxjs";
import {catchError as rxCatchError} from "rxjs/internal/operators/catchError";
import {map as rxMap, startWith as rxStartWith} from "rxjs/operators";

import {AuthService} from "../../modules/auth/auth.service.mjs";
import {UserService} from "../../modules/user/user.service.mjs";

const USER_NOT_FOUND: unique symbol = Symbol("USER_NOT_FOUND");
const TEXT_ENCODER: TextEncoder = new TextEncoder();

@Component({
  selector: "etwin-user-view",
  templateUrl: "./user-view.component.html",
  styleUrls: ["./user-view.component.scss"],
})
export class UserViewComponent implements OnInit {
  public readonly $UserDisplayName = $UserDisplayName;
  public readonly $RawUsername = $RawUsername;
  public readonly PASSWORD_LEN: number = 10;

  private readonly route: ActivatedRoute;

  public user$: Observable<MaybeCompleteUser | typeof USER_NOT_FOUND>;
  public readonly ObjectType = ObjectType;
  public readonly USER_NOT_FOUND = USER_NOT_FOUND;
  public isAdministrator$: Observable<boolean>;

  public readonly displayNameForm: FormGroup;
  public readonly displayName: FormControl;
  public readonly displayNameUserId: FormControl;
  public displayNameSubscription: Subscription | null = null;
  public displayNameServerError: Error | null = null;

  public readonly usernameForm: FormGroup;
  public readonly username: FormControl;
  public readonly usernameUserId: FormControl;
  public usernameSubscription: Subscription | null = null;
  public usernameServerError: Error | null = null;

  public readonly passwordForm: FormGroup;
  public readonly password: FormControl;
  public readonly passwordUserId: FormControl;
  public passwordSubscription: Subscription | null = null;
  public passwordServerError: Error | null = null;

  public readonly tidForm: FormGroup;
  public readonly tidId: FormControl;
  public readonly tidUserId: FormControl;
  public tidSubscription: Subscription | null = null;
  public tidServerError: Error | null = null;

  public readonly hfFrForm: FormGroup;
  public readonly hfFrId: FormControl;
  public readonly hfFrUserId: FormControl;
  public hfFrSubscription: Subscription | null = null;
  public hfFrServerError: Error | null = null;

  public readonly hfEnForm: FormGroup;
  public readonly hfEnId: FormControl;
  public readonly hfEnUserId: FormControl;
  public hfEnSubscription: Subscription | null = null;
  public hfEnServerError: Error | null = null;

  public readonly hfEsForm: FormGroup;
  public readonly hfEsId: FormControl;
  public readonly hfEsUserId: FormControl;
  public hfEsSubscription: Subscription | null = null;
  public hfEsServerError: Error | null = null;

  private readonly document: Document;
  private readonly user: UserService;

  constructor(
    auth: AuthService,
    route: ActivatedRoute,
    user: UserService,
    @Inject(DOCUMENT) document: Document,
  ) {
    this.document = document;
    this.route = route;
    this.user = user;
    this.user$ = RX_NEVER;
    this.isAdministrator$ = auth.auth().pipe(
      rxMap((acx: AuthContext) => acx.type === AuthType.User && acx.isAdministrator),
      rxStartWith(false),
      rxCatchError(() => rxOf(false)),
    );

    this.displayName = new FormControl(
      "",
      [Validators.required, Validators.minLength($UserDisplayName.minLength ?? 0), Validators.maxLength($UserDisplayName.maxLength), Validators.pattern($UserDisplayName.pattern!)],
    );
    this.displayNameUserId = new FormControl(
      "",
      [Validators.required],
    );
    this.displayNameForm = new FormGroup({
      displayName: this.displayName,
      userId: this.displayNameUserId,
    });

    this.username = new FormControl(
      "",
      [Validators.required, Validators.minLength($Username.minLength ?? 0), Validators.maxLength($Username.maxLength), Validators.pattern($Username.pattern!)],
    );
    this.usernameUserId = new FormControl(
      "",
      [Validators.required],
    );
    this.usernameForm = new FormGroup({
      username: this.username,
      userId: this.usernameUserId,
    });

    this.password = new FormControl(
      "",
      [Validators.required, Validators.minLength(this.PASSWORD_LEN)],
    );
    this.passwordUserId = new FormControl(
      "",
      [Validators.required],
    );
    this.passwordForm = new FormGroup({
      password: this.password,
      userId: this.passwordUserId,
    });

    this.tidId = new FormControl(
      "",
      [Validators.required],
    );
    this.tidUserId = new FormControl(
      "",
      [Validators.required],
    );
    this.tidForm = new FormGroup({
      tidId: this.tidId,
      userId: this.tidUserId,
    });

    this.hfFrId = new FormControl(
      "",
      [Validators.required],
    );
    this.hfFrUserId = new FormControl(
      "",
      [Validators.required],
    );
    this.hfFrForm = new FormGroup({
      hfFrId: this.hfFrId,
      userId: this.hfFrUserId,
    });

    this.hfEnId = new FormControl(
      "",
      [Validators.required],
    );
    this.hfEnUserId = new FormControl(
      "",
      [Validators.required],
    );
    this.hfEnForm = new FormGroup({
      hfEnId: this.hfEnId,
      userId: this.hfEnUserId,
    });

    this.hfEsId = new FormControl(
      "",
      [Validators.required],
    );
    this.hfEsUserId = new FormControl(
      "",
      [Validators.required],
    );
    this.hfEsForm = new FormGroup({
      hfEsId: this.hfEsId,
      userId: this.hfEsUserId,
    });
  }

  ngOnInit(): void {
    interface RouteData {
      user: MaybeCompleteUser | null;
    }

    const routeData$: Observable<RouteData> = this.route.data as any;
    this.user$ = routeData$.pipe(rxMap(({user}: RouteData) => {
      if (user !== null) {
        this.displayNameUserId.setValue(user.id ?? "");
        this.usernameUserId.setValue(user.id ?? "");
        this.passwordUserId.setValue(user.id ?? "");
        this.tidUserId.setValue(user.id ?? "");
        this.hfFrUserId.setValue(user.id ?? "");
        this.hfEnUserId.setValue(user.id ?? "");
        this.hfEsUserId.setValue(user.id ?? "");
        return user;
      } else {
        return USER_NOT_FOUND;
      }
    }));
  }

  public onSubmitDisplayName(event: Event) {
    event.preventDefault();
    if (this.displayNameSubscription !== null) {
      return;
    }
    const model: any = this.displayNameForm.getRawValue();
    const displayName: UserDisplayName = model.displayName;
    const userId: UserId = model.userId;
    const updateResult$ = this.user.updateUser(userId, {displayName});
    this.displayNameServerError = null;
    const subscription: Subscription = updateResult$.subscribe({
      next: (): void => {
        subscription.unsubscribe();
        this.displayNameSubscription = null;
        this.document.location.reload();
      },
      error: (err: Error): void => {
        subscription.unsubscribe();
        this.displayNameSubscription = null;
        this.displayNameServerError = err;
      },
      complete: (): void => {
        subscription.unsubscribe();
        this.displayNameSubscription = null;
      },
    });
    this.displayNameSubscription = subscription;
  }

  public onSubmitUsername(event: Event) {
    event.preventDefault();
    if (this.usernameSubscription !== null) {
      return;
    }
    const model: any = this.usernameForm.getRawValue();
    const rawUsername: RawUsername = model.username;
    const username: Username = rawUsername.toLowerCase();
    const userId: UserId = model.userId;
    const updateResult$ = this.user.updateUser(userId, {username});
    this.usernameServerError = null;
    const subscription: Subscription = updateResult$.subscribe({
      next: (): void => {
        subscription.unsubscribe();
        this.usernameSubscription = null;
        this.document.location.reload();
      },
      error: (err: Error): void => {
        subscription.unsubscribe();
        this.usernameSubscription = null;
        this.usernameServerError = err;
      },
      complete: (): void => {
        subscription.unsubscribe();
        this.usernameSubscription = null;
      },
    });
    this.usernameSubscription = subscription;
  }

  public onSubmitPassword(event: Event) {
    event.preventDefault();
    if (this.passwordSubscription !== null) {
      return;
    }
    const model: any = this.passwordForm.getRawValue();
    const passwordStr: string = model.password;
    const password: Uint8Array = TEXT_ENCODER.encode(passwordStr);
    const userId: UserId = model.userId;
    const updateResult$ = this.user.updateUser(userId, {password});
    this.passwordServerError = null;
    const subscription: Subscription = updateResult$.subscribe({
      next: (): void => {
        subscription.unsubscribe();
        this.passwordSubscription = null;
        this.document.location.reload();
      },
      error: (err: Error): void => {
        subscription.unsubscribe();
        this.passwordSubscription = null;
        this.passwordServerError = err;
      },
      complete: (): void => {
        subscription.unsubscribe();
        this.passwordSubscription = null;
      },
    });
    this.passwordSubscription = subscription;
  }

  public onSubmitTid(event: Event) {
    event.preventDefault();
    if (this.tidSubscription !== null) {
      return;
    }
    const model: any = this.tidForm.getRawValue();
    const twinoidUserId: string = model.tidId;
    const userId: UserId = model.userId;
    const updateResult$ = this.user.linkToTwinoid({method: LinkToTwinoidMethod.Ref, userId, twinoidUserId});
    this.tidServerError = null;
    const subscription: Subscription = updateResult$.subscribe({
      next: (): void => {
        subscription.unsubscribe();
        this.tidSubscription = null;
        this.document.location.reload();
      },
      error: (err: Error): void => {
        subscription.unsubscribe();
        this.tidSubscription = null;
        this.tidServerError = err;
      },
      complete: (): void => {
        subscription.unsubscribe();
        this.tidSubscription = null;
      },
    });
    this.tidSubscription = subscription;
  }

  public onSubmitHfFr(event: Event) {
    this.onSubmitHfAny(event, "hammerfest.fr");
  }

  public onSubmitHfEn(event: Event) {
    this.onSubmitHfAny(event, "hfest.net");
  }

  public onSubmitHfEs(event: Event) {
    this.onSubmitHfAny(event, "hammerfest.es");
  }

  public onSubmitHfAny(event: Event, hammerfestServer: HammerfestServer) {
    event.preventDefault();
    let model: any;
    let hammerfestUserId: string;
    switch (hammerfestServer) {
      case "hfest.net":
        if (this.hfEnSubscription !== null) {
          return;
        }
        model = this.hfEnForm.getRawValue();
        hammerfestUserId = model.hfEnId;
        break;
      case "hammerfest.es":
        if (this.hfEsSubscription !== null) {
          return;
        }
        model = this.hfEsForm.getRawValue();
        hammerfestUserId = model.hfEsId;
        break;
      case "hammerfest.fr":
        if (this.hfFrSubscription !== null) {
          return;
        }
        model = this.hfFrForm.getRawValue();
        hammerfestUserId = model.hfFrId;
        break;
    }
    const userId: UserId = model.userId;
    const updateResult$ = this.user.linkToHammerfest({method: LinkToHammerfestMethod.Ref, userId, hammerfestServer, hammerfestUserId});
    switch (hammerfestServer) {
      case "hfest.net": {
        this.hfEnServerError = null;
        const subscription: Subscription = updateResult$.subscribe({
          next: (): void => {
            subscription.unsubscribe();
            this.hfEnSubscription = null;
            this.document.location.reload();
          },
          error: (err: Error): void => {
            subscription.unsubscribe();
            this.hfEnSubscription = null;
            this.hfEnServerError = err;
          },
          complete: (): void => {
            subscription.unsubscribe();
            this.hfEnSubscription = null;
          },
        });
        this.hfEnSubscription = subscription;
        break;
      }
      case "hammerfest.es": {
        this.hfEsServerError = null;
        const subscription: Subscription = updateResult$.subscribe({
          next: (): void => {
            subscription.unsubscribe();
            this.hfEsSubscription = null;
            this.document.location.reload();
          },
          error: (err: Error): void => {
            subscription.unsubscribe();
            this.hfEsSubscription = null;
            this.hfEsServerError = err;
          },
          complete: (): void => {
            subscription.unsubscribe();
            this.hfEsSubscription = null;
          },
        });
        this.hfEsSubscription = subscription;
        break;
      }
      case "hammerfest.fr": {
        this.hfFrServerError = null;
        const subscription: Subscription = updateResult$.subscribe({
          next: (): void => {
            subscription.unsubscribe();
            this.hfFrSubscription = null;
            this.document.location.reload();
          },
          error: (err: Error): void => {
            subscription.unsubscribe();
            this.hfFrSubscription = null;
            this.hfFrServerError = err;
          },
          complete: (): void => {
            subscription.unsubscribe();
            this.hfFrSubscription = null;
          },
        });
        this.hfFrSubscription = subscription;
        break;
      }
    }
  }
}
