# 2022-01: This Month In Eternaltwin #4

[Eternaltwin](https://eternaltwin.org) is a project with the goal to preserve
Motion Twin's games and player communities.

# Eternaltwin

We completed the migration of all the [Eternaltwin](https://eternaltwin.org/)
services to Rust.

This migration was the main theme of 2021, and we should now reap the benefits:
reliability and performance.

## Release v0.10.0

We released the version **0.10.0** of [the Eternaltwin package](https://www.npmjs.com/package/@eternal-twin/cli).
We also updated [the PHP client](https://packagist.org/packages/eternaltwin/etwin) accordingly.
Clients for other platforms will be updated progressively.

This update is mostly about fixing some internal minor issues. You can read
[the full changes in this Merge Request](https://gitlab.com/eternaltwin/etwin/-/merge_requests/410).

## Next Features

We will work on closing existing issues, fixing translations and applying
some style changes.

The next feature will be fixing the account management issues: reporting
meaningful errors, fixing redirection issues, enabling password resets, etc.

We are also starting to design how to implement the shared reward system
(titles and icons).

# Neoparc

Hello dear Dinoz master from [Neoparc](https://neoparc.eternaltwin.org/)!

First of all, the weekly raids were rebalanced regarding timings and boss stats.

Most of the missing skills were added to the game.

7 brancd new skills are available, so each theoretical element total for
classic dinoz is balanced. Now it's your turn to discover them!

Finally, there were some changes to the bazar. You may now exchange items from
the secret den through the bazar.

Have a nice game!

# Eternal Kingdom

[Eternal Kingdom](https://kingdom.eternaltwin.org/) now lets you play with your
Eternaltwin account! Have fun and try to raise through the leaderboard!

## Main features

We now have an awesome logo (done by **Seidanoob#5281**).

There is now a log for your city: you no longer have to remember what you did.

The current pages were almost-completely translated into English and Spanish.
Thank you everyone.

You may now recruit your generals. 💯

The webpage footer was updated to match Eternaltwin. 💃

Some new pages start to appear event if they're not fully ready (leaderboard,
world selection, ...).

## Main improvements

You no longer will have to refresh the page following user actions. You can
stay on the building selection page and the buttons will get updated
automatically. The build option will be enabled once a builder is available
(or disable once you lose your last builder).

As a player/tester, please share your feedback on [the Eternal Kingdom
forum](https://eternaltwin.org/forum/sections/407e8a9e-8c5a-4177-b13c-9a7d746c7d24)
or directly [on Discord](https://discord.gg/ERc3svy) in the **#kingdom** channel.

If you want to help with development, we are looking for a BACKEND dev (PHP/Symfony
server) or frontend dev (Web: HTML/JS/CSS).

Thank you all for your support 😄

# MyHordes

_radio crackle_

Hello folks, and thank you for using our radio-phonic wireless system! I'm
Connor, and today I'm sending you a little message regarding
[MyHordes](https://myhordes.eu/).

![MyHordes - Wireless technology ad](./myhordes.png)

As you surely know, lately it has been rather calm. I hope you were able
to rest. And _scrambled_.

On another subject, I wanted to announce that you should find the game site on
search engines shortly normally. Hoping no zombies come at the same
time... Also _scrambled_

The Councils are in full swing in the cities. The game has been globally
optimized and is faster on all platforms. Everything is not perfect but devs
are progressing at their own pace, moreover in speaking of them I send you this
little advice, but I cannot prove its authenticity to you since it has been
copied countless times.

---

_Brainbox_: Hi everyone.

_Brainbox_: First order of business is to come up with some new content
for Season 15. Any ideas?

_Shaitan_: New golden pictos !

_Adri_: Fewer players per city, but shorter cities?

_Brainbox_: So... nobody has any better ideas, then?

_Dylan57_: I propose that we rework the whole system of heroes.

_Adri_: Is he there?

_Shaitan_: Well, it's not famous either.

_Nayr_: *eh eh*

_Dylan57_: Did you hear that?

_Fatigue on the other hand is front and center..._

_Brainbox_: Well since you are not helping me, I will look in the ideas box.

_Shaitan_: Oh no !

_Dylan57_: Seriously !

_Adri_: Nincompoop

_Brainbox_: We immediately go to the random draw.

_Brainbox_: Each of you comes to get a sheet with an idea written in my hand, please.

_Shaitan_: Why don't we do it with a poll for change?

_Dylan57_: Yeah genius, with 34 shitty ideas per second ...

_...No one will know what will happen to the papers. But the draw was made..._

---

That was all for now, see you the next time! _radio crackle_

# Eternal DinoRPG

Work on [eDinoRPG](https://dinorpg.eternaltwin.org/) is still going on.

We completed our goal for the end of year by implementing the DinoRPG "scraper",
a tool to archive data from the official website. It will let us archive
Dinoz skills, sacrificed dinoz, and any other data missing from the API.
We are now working on enabling it in the Eternaltwin server.

In the meantime we are already preparing the version 0.2.0!

As soon of account imports will be enabled, we will also enable: a load screen,
error pop-ups, a profile page, a language picker, a system to move the dinoz.

We are also working on leaderboards and epic rewards.

Until we publish everything, here is a small preview:

![Eternal DinoRPG movement preview](./edinorpg.gif)

# ePopotamo

The alpha version of the [ePopotamo](https://epopotamo.eternaltwin.org/)
Scrabble-like word game is the latest addition to the list of Eternaltwin games!
We are now officially open, you may try it out at [https://epopotamo.eternaltwin.org/](https://epopotamo.eternaltwin.org/).

Right now it's an alpha version. It means that you can check it out and report
issues. The basics are there: a single game, placing letters, checking words,
options, scores, etc. Over time we will improve the game until it is fully
ready.

Again, please report any issues you find!

# Closing words

We released a couple of games in 2021, and hope to release even more in 2022.
Thank you all for your help!

You can send [your messages for February](https://gitlab.com/eternaltwin/etwin/-/issues/39).

This article was edited by: Bibni, Biosha, Demurgos, Dylan57, Fabianh,
Jonathan, MisterSimple, Nadawoo, Patate404, and Schroedinger.
