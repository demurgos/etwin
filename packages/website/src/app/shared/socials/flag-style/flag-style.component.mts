import { CommonModule } from "@angular/common";
import { Component, Input } from "@angular/core";

@Component({
  selector: "flag-style",
  standalone: true,
  imports: [CommonModule],
  templateUrl: "./flag-style.component.html",
  styleUrls: ["./flag-style.component.scss"],
})
export class FlagStyleComponent {
  @Input() type: string = "";
}