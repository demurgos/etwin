import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { SharedModule } from "../shared/shared.module.mjs";
import { DocsContentComponent } from "./docs-content.component.mjs";
import { DocsNotFoundComponent } from "./docs-not-found.component.mjs";
import { DocsRoutingModule } from "./docs-routing.module.mjs";
import { DocsViewComponent } from "./docs-view.component.mjs";

@NgModule({
  declarations: [DocsContentComponent, DocsNotFoundComponent, DocsViewComponent],
  imports: [
    CommonModule,
    DocsRoutingModule,
    SharedModule,
  ],
})
export class DocsModule {
}
