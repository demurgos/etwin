use crate::core::{Instant, Request};
use crate::twinoid::{ArchivedTwinoidUser, TwinoidUserIdRef};
use crate::types::WeakError;
use thiserror::Error;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Error)]
pub enum GetUserError {
  #[error("user not found")]
  NotFound,
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetUser {
  pub user: TwinoidUserIdRef,
  pub time: Instant,
}

impl Request for GetUser {
  type Response = Result<ArchivedTwinoidUser, GetUserError>;
}
