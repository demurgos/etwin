import {CaseStyle} from "kryo";
import {$Date} from "kryo/date";
import {RecordIoType, RecordType} from "kryo/record";
import {Ucs2StringType} from "kryo/ucs2-string";
import {$UuidHex, UuidHex} from "kryo/uuid-hex";

import {$EmailAddress, EmailAddress} from "../email/email-address.mjs";

export interface SendMarktwinEmail {
  deadline: Date;
  idempotencyKey: UuidHex;
  recipient: EmailAddress;
  subject: string;
  body: string;
}

export const $SendMarktwinEmail: RecordIoType<SendMarktwinEmail> = new RecordType<SendMarktwinEmail>({
  properties: {
    deadline: {type: $Date},
    idempotencyKey: {type: $UuidHex},
    recipient: {type: $EmailAddress},
    subject: {type: new Ucs2StringType({minLength: 1, maxLength: 20, trimmed: true})},
    body: {type: new Ucs2StringType({minLength: 1, maxLength: 1000, trimmed: true})},
  },
  changeCase: CaseStyle.SnakeCase,
});
