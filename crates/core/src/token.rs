use crate::dinoparc::{DinoparcServer, DinoparcSessionKey, DinoparcUserIdRef, StoredDinoparcSession};
use crate::oauth::{TwinoidAccessToken, TwinoidRefreshToken};
use crate::types::WeakError;
use async_trait::async_trait;
#[cfg(feature = "serde")]
use eternaltwin_serde_tools::{Deserialize, Serialize};
use std::error::Error;
use std::ops::Deref;

/// Current Twinoid OAuth tokens
///
/// If the refresh token is revoked but an access token still exists, may
/// return `TwinoidOauth {access_token: Some, refresh_token: None}`.
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TwinoidOauth {
  pub access_token: Option<TwinoidAccessToken>,
  pub refresh_token: Option<TwinoidRefreshToken>,
}

#[derive(thiserror::Error, Debug)]
pub enum TouchDinoparcTokenError {
  #[error(transparent)]
  Other(WeakError),
}

impl TouchDinoparcTokenError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

#[derive(thiserror::Error, Debug)]
pub enum RevokeDinoparcTokenError {
  #[error(transparent)]
  Other(WeakError),
}

impl RevokeDinoparcTokenError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

#[derive(thiserror::Error, Debug)]
pub enum GetDinoparcTokenError {
  #[error(transparent)]
  Other(WeakError),
}

impl GetDinoparcTokenError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

#[async_trait]
pub trait TokenStore: Send + Sync {
  async fn touch_dinoparc(
    &self,
    user: DinoparcUserIdRef,
    key: &DinoparcSessionKey,
  ) -> Result<StoredDinoparcSession, TouchDinoparcTokenError>;
  async fn revoke_dinoparc(
    &self,
    server: DinoparcServer,
    key: &DinoparcSessionKey,
  ) -> Result<(), RevokeDinoparcTokenError>;
  async fn get_dinoparc(&self, user: DinoparcUserIdRef)
    -> Result<Option<StoredDinoparcSession>, GetDinoparcTokenError>;
}

/// Like [`Deref`], but the target has the bound [`TokenStore`]
pub trait TokenStoreRef: Send + Sync {
  type TokenStore: TokenStore + ?Sized;

  fn token_store(&self) -> &Self::TokenStore;
}

impl<TyRef> TokenStoreRef for TyRef
where
  TyRef: Deref + Send + Sync,
  TyRef::Target: TokenStore,
{
  type TokenStore = TyRef::Target;

  fn token_store(&self) -> &Self::TokenStore {
    self.deref()
  }
}
