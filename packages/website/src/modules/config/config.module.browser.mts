import { NgModule } from "@angular/core";

import { BrowserConfigService } from "./config.service.browser.mjs";
import { ConfigService } from "./config.service.mjs";

@NgModule({
  providers: [
    {provide: ConfigService, useClass: BrowserConfigService},
  ],
  imports: [],
})
export class BrowserConfigModule {
}
