# `eternaltwin_cli`

## Usage

```
cargo eternaltwin dinoparc
cargo eternaltwin dinorpg
cargo eternaltwin dump
cargo eternaltwin twinoid
# etc.
# get all commands with:
cargo eternaltwin --help
```
