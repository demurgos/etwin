pub mod backend;
pub mod frontend;
pub mod load;
pub mod mailer;
pub mod opentelemetry;
pub mod postgres;
pub mod scrypt;
pub mod seed;
pub mod sqlite;

use crate::backend::{BackendConfig, BackendConfigPatch};
use crate::frontend::{FrontendConfig, FrontendConfigPatch};
use crate::load::{ConfigRoot, ConfigRoots};
use crate::mailer::{MailerConfig, MailerConfigPatch};
use crate::opentelemetry::{OpentelemetryConfig, OpentelemetryConfigPatch};
use crate::postgres::{PostgresConfig, PostgresConfigPatch};
use crate::scrypt::{ScryptConfig, ScryptConfigPatch};
use crate::seed::{SeedConfig, SeedConfigPatch};
use crate::sqlite::{SqliteConfig, SqliteConfigPatch};
use eternaltwin_core::patch::SimplePatch;
use eternaltwin_core::types::{DisplayErrorChain, WeakError};
use serde::{Deserialize, Serialize};
use std::borrow::Borrow;
use std::collections::hash_map::Entry as HashMapEntry;
use std::collections::{HashMap, HashSet};
use std::convert::Infallible;
use std::fmt::Debug;
use std::fs;
use std::io;
use std::path::{Path, PathBuf};
use std::string::ToString;
use std::sync::RwLock;
use url::Url;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ConfigChain {
  pub items: Vec<ResolvedConfig>,
  pub default: BuiltinEternaltwinConfigProfile,
}

impl ConfigChain {
  pub fn merge(self) -> Config<ConfigSource> {
    let mut cur = Config::default_for_profile(self.default);
    for item in self.items.into_iter().rev() {
      cur = cur.patch(item.config, item.source.clone());
    }
    cur
  }

  #[cfg(test)]
  pub(crate) fn one(resolved: ResolvedConfig, default: BuiltinEternaltwinConfigProfile) -> Self {
    Self {
      items: vec![resolved],
      default,
    }
  }

  pub fn resolve_from_roots(roots: &[ConfigRoot], working_dir: &Path, profile: &EternaltwinConfigProfile) -> Self {
    let mut resolver = ConfigResolver::new();
    for root in roots.iter().rev() {
      resolver.add_root(root, working_dir)
    }
    Self {
      items: resolver.resolved,
      default: profile.as_builtin_or_dev(),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ResolvedConfig {
  pub source: ConfigSource,
  format: ConfigFormat,
  config: ConfigPatch,
}

impl ResolvedConfig {
  pub fn parse(s: &str, format: ConfigFormat, prefer_json: bool, source: ConfigSource) -> Result<Self, WeakError> {
    let (format, partial) = match format {
      ConfigFormat::Auto => {
        if prefer_json {
          match serde_json::from_str(s) {
            Ok(p) => (ConfigFormat::Json, p),
            Err(e) => toml::de::from_str(s)
              .map(|p| (ConfigFormat::Toml, p))
              .map_err(|_| WeakError::wrap(e))?,
          }
        } else {
          match toml::de::from_str(s) {
            Ok(p) => (ConfigFormat::Toml, p),
            Err(e) => serde_json::from_str(s)
              .map(|p| (ConfigFormat::Json, p))
              .map_err(|_| WeakError::wrap(e))?,
          }
        }
      }
      ConfigFormat::Json => (ConfigFormat::Json, serde_json::from_str(s).map_err(WeakError::wrap)?),
      ConfigFormat::Toml => (ConfigFormat::Toml, toml::de::from_str(s).map_err(WeakError::wrap)?),
    };
    Ok(Self {
      source,
      format,
      config: partial,
    })
  }

  pub fn extends(&self, working_dir: &Url) -> Vec<(Url, ConfigFormat)> {
    let extends = match self.config.extends.as_ref() {
      Some(extends) => extends,
      None => return Vec::new(),
    };

    let base_url = match &self.source {
      ConfigSource::File(Some(file)) => Url::from_file_path(file).expect("invalid file path"),
      _ => working_dir.clone(),
    };

    match extends {
      ConfigRefOrList::One(e) => Self::extends_inner(&base_url, &[e]),
      ConfigRefOrList::Many(e) => Self::extends_inner(&base_url, e),
    }
  }

  fn extends_inner<Cr: Borrow<ConfigRef>>(base_url: &Url, extends: &[Cr]) -> Vec<(Url, ConfigFormat)> {
    let mut result: Vec<(Url, ConfigFormat)> = Vec::new();
    for e in extends {
      let (url, format) = match e.borrow() {
        ConfigRef::Url(u) => (u, ConfigFormat::Auto),
        ConfigRef::Typed { url, format } => (url, *format),
      };
      let url = resolve_config_ref(base_url, url);
      result.push((url, format));
    }
    result
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
struct ConfigPatch {
  extends: Option<ConfigRefOrList>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  backend: SimplePatch<BackendConfigPatch>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  frontend: SimplePatch<FrontendConfigPatch>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  postgres: SimplePatch<PostgresConfigPatch>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  mailer: SimplePatch<MailerConfigPatch>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  scrypt: SimplePatch<ScryptConfigPatch>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  opentelemetry: SimplePatch<OpentelemetryConfigPatch>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  seed: SimplePatch<Option<SeedConfigPatch>>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  sqlite: SimplePatch<SqliteConfigPatch>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(untagged)]
enum ConfigRefOrList {
  One(ConfigRef),
  Many(Vec<ConfigRef>),
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(untagged)]
enum ConfigRef {
  Url(String),
  Typed { url: String, format: ConfigFormat },
}

struct FsCache {
  state: RwLock<HashMap<PathBuf, io::Result<String>>>,
}

impl FsCache {
  pub fn new() -> Self {
    Self {
      state: RwLock::new(HashMap::new()),
    }
  }

  pub fn read_to_string<F, R>(&self, p: &Path, f: F) -> R
  where
    F: for<'a> Fn(&'a io::Result<String>) -> R,
  {
    {
      if let Some(r) = self.state.read().expect("lock is poisoned").get(p) {
        return f(r);
      }
    }
    let mut s = self.state.write().expect("lock is poisoned");
    match s.entry(p.to_path_buf()) {
      HashMapEntry::Vacant(e) => {
        let p = e.key();
        let r = fs::read_to_string(p);
        let r = e.insert(r);
        f(r)
      }
      HashMapEntry::Occupied(e) => f(e.get()),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
enum TryResolveUrlError {
  #[error("resource not found")]
  NotFound,
  #[error("unexpected URL scheme {0:?}")]
  Scheme(String),
  #[error("unexpected io error")]
  Io(#[source] WeakError),
}

struct ConfigResolver {
  resolved: Vec<ResolvedConfig>,
  fs: FsCache,
}

impl ConfigResolver {
  pub(crate) fn new() -> Self {
    Self {
      resolved: Vec::new(),
      fs: FsCache::new(),
    }
  }

  pub(crate) fn add_root(&mut self, root: &ConfigRoot, working_dir: &Path) {
    let working_dir_url = Url::from_directory_path(working_dir).expect("invalid working dir");
    let resolved = match root {
      ConfigRoot::Data { data, format } => Some(self.add_data(data, format)),
      ConfigRoot::Search { patterns, format } => self.add_search(patterns, format, working_dir.to_path_buf()),
      ConfigRoot::Url { url, format } => Some(self.add_url(&working_dir_url, url, format)),
    };
    let resolved: &ResolvedConfig = match resolved {
      None => return,
      Some(resolved) => resolved,
    };
    let mut stack = resolved.extends(&working_dir_url);
    while let Some((url, format)) = stack.pop() {
      let dependency = self
        .try_resolve_url(&url, format)
        .expect("failed to resolve base config");
      stack.extend_from_slice(&dependency.extends(&working_dir_url));
      self.resolved.push(dependency);
    }
  }

  fn add_data(
    &mut self,
    data: &ConfigValue<String, LoadConfigSource>,
    format: &ConfigValue<ConfigFormat, LoadConfigSource>,
  ) -> &ResolvedConfig {
    let resolved_source: ConfigSource = match data.meta {
      LoadConfigSource::Args => ConfigSource::Cli,
      LoadConfigSource::Env => ConfigSource::Env,
      LoadConfigSource::Default(_) => ConfigSource::Default,
    };
    let resolved = ResolvedConfig::parse(&data.value, format.value, true, resolved_source).expect("invalid config");
    self.resolved.push(resolved);
    self.resolved.last().expect("`self.resolved` is non-empty")
  }

  fn add_search(
    &mut self,
    patterns: &ConfigValue<Vec<String>, LoadConfigSource>,
    format: &ConfigValue<ConfigFormat, LoadConfigSource>,
    start_dir: PathBuf,
  ) -> Option<&ResolvedConfig> {
    let mut visited: HashSet<PathBuf> = HashSet::new();
    let mut cur: PathBuf = start_dir;
    loop {
      let cur_url = Url::from_directory_path(&cur).expect("invalid cur search dir");
      for pat in &patterns.value {
        let is_relative_path = pat.starts_with("./") || pat.starts_with("../");
        if !is_relative_path {
          panic!(r#"search pattern must start with "./" or "../", received {pat}"#);
        }
        let target = Url::options().base_url(Some(&cur_url)).parse(pat);
        let target = match target {
          Ok(t) => t,
          Err(e) => panic!("failed to build file url from pattern {pat:?} and base {cur_url}: {e:?}"),
        };
        match self.try_resolve_url(&target, format.value) {
          Ok(r) => {
            self.resolved.push(r);
            return Some(self.resolved.last().expect("`self.resolved` is non-empty"));
          }
          Err(TryResolveUrlError::NotFound) => continue,
          Err(e) => panic!("read error for {target}: {}", DisplayErrorChain(&e)),
        };
      }
      let parent: Option<PathBuf> = cur.parent().map(|p| p.to_path_buf());
      if let Some(parent) = parent {
        visited.insert(cur);
        if visited.contains(parent.as_path()) {
          // Found cycle when iterating through parents; we assume that we
          // reached the root on an OS where Rust has trouble noticing it.
          return None;
        } else {
          cur = parent;
        }
      } else {
        // Reached root without finding any match
        return None;
      }
    }
  }

  fn add_url(
    &mut self,
    working_dir: &Url,
    url: &ConfigValue<String, LoadConfigSource>,
    format: &ConfigValue<ConfigFormat, LoadConfigSource>,
  ) -> &ResolvedConfig {
    let url = resolve_config_ref(working_dir, &url.value);
    match self.try_resolve_url(&url, format.value) {
      Ok(r) => {
        self.resolved.push(r);
        self.resolved.last().expect("`self.resolved` is non-empty")
      }
      Err(e) => panic!("read error for {url}: {}", DisplayErrorChain(&e)),
    }
  }

  fn try_resolve_url(&self, url: &Url, format: ConfigFormat) -> Result<ResolvedConfig, TryResolveUrlError> {
    match url.scheme() {
      "file" => {
        let path = url.to_file_path().expect("must be a file url");
        self.fs.read_to_string(&path, |r| match r.as_deref() {
          Ok(s) => {
            let resolved_source = ConfigSource::File(Some(path.clone()));
            let resolved = ResolvedConfig::parse(s, format, false, resolved_source).expect("config format error");
            Ok(resolved)
          }
          Err(e) => Err(match e.kind() {
            io::ErrorKind::NotFound => TryResolveUrlError::NotFound,
            _ => TryResolveUrlError::Io(WeakError::wrap(e)),
          }),
        })
      }
      s => Err(TryResolveUrlError::Scheme(s.to_string())),
    }
  }
}

fn resolve_config_ref(base: &Url, url: &str) -> Url {
  let parsed = if url.starts_with("./") || url.starts_with("../") {
    Url::options().base_url(Some(base)).parse(url)
  } else {
    Url::parse(url)
  };
  match parsed {
    Ok(u) => u,
    Err(e) => panic!("invalid url {url}: {}", DisplayErrorChain(&e)),
  }
}

/// Trait representing a config field.
///
/// A config field is path to a value. This is the main way to extract values
/// from a config object.
pub trait ConfigField<TyContainer, TyValue> {
  /// Path, for debug purposes
  fn path(&self) -> &[&str];

  fn get(&self, container: TyContainer) -> TyValue;
}

/// Value extracted from config.
///
/// The value is always tagged with metadata.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct ConfigValue<T, M = ()> {
  pub value: T,
  pub meta: M,
}

impl<T, M> ConfigValue<T, M>
where
  M: Default,
{
  pub fn new(value: T) -> Self {
    Self {
      value,
      meta: M::default(),
    }
  }
}

impl<T, M> ConfigValue<T, M> {
  pub fn new_meta(value: T, meta: M) -> Self {
    Self { value, meta }
  }
}

/// Entry from config: field and corresponding value.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ConfigEntry<F, V> {
  pub field: F,
  pub value: V,
}

pub type DebugConfigValue<T> = ConfigValue<T, ConfigMeta<T, LoadConfigSource>>;

pub type DebugConfigEntry<'a> = ConfigEntry<&'a [&'a str], DebugConfigValue<&'a dyn Debug>>;

/// Source for the bootstrap config of the loader
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum LoadConfigSource {
  Args,
  Env,
  Default(BuiltinEternaltwinConfigProfile),
}

/// Metadata for a config value
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ConfigMeta<V, S> {
  pub default: V,
  pub source: S,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum BuiltinEternaltwinConfigProfile {
  Production,
  Dev,
  Test,
  Sdk,
}

impl BuiltinEternaltwinConfigProfile {
  pub fn as_str(&self) -> &str {
    match &self {
      Self::Production => "production",
      Self::Dev => "dev",
      Self::Test => "test",
      Self::Sdk => "sdk",
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum EternaltwinConfigProfile {
  Production,
  Dev,
  Test,
  Sdk,
  Custom(String),
}

impl EternaltwinConfigProfile {
  pub fn as_str(&self) -> &str {
    match &self {
      Self::Production => "production",
      Self::Dev => "dev",
      Self::Test => "test",
      Self::Sdk => "sdk",
      Self::Custom(ref profile) => profile.as_str(),
    }
  }

  pub fn parse(s: &str) -> Self {
    match s {
      "production" => Self::Production,
      "dev" => Self::Dev,
      "test" => Self::Test,
      "sdk" => Self::Sdk,
      s => Self::Custom(s.to_string()),
    }
  }

  pub fn as_builtin_or_dev(&self) -> BuiltinEternaltwinConfigProfile {
    match &self {
      Self::Production => BuiltinEternaltwinConfigProfile::Production,
      Self::Dev => BuiltinEternaltwinConfigProfile::Dev,
      Self::Test => BuiltinEternaltwinConfigProfile::Test,
      Self::Sdk => BuiltinEternaltwinConfigProfile::Sdk,
      Self::Custom(_) => BuiltinEternaltwinConfigProfile::Dev,
    }
  }
}

impl core::str::FromStr for EternaltwinConfigProfile {
  type Err = Infallible;

  fn from_str(s: &str) -> Result<Self, Self::Err> {
    Ok(Self::parse(s))
  }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum ConfigFormat {
  Auto,
  Json,
  Toml,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("invalid config format: possible values are `auto`, `json`, `toml`")]
pub struct InvalidConfigFormat;

impl ConfigFormat {
  pub fn as_str(&self) -> &str {
    match &self {
      Self::Auto => "auto",
      Self::Json => "json",
      Self::Toml => "toml",
    }
  }

  pub fn parse(s: &str) -> Result<Self, InvalidConfigFormat> {
    match s {
      "auto" => Ok(Self::Auto),
      "json" => Ok(Self::Json),
      "toml" => Ok(Self::Toml),
      _ => Err(InvalidConfigFormat),
    }
  }
}

impl core::str::FromStr for ConfigFormat {
  type Err = InvalidConfigFormat;

  fn from_str(s: &str) -> Result<Self, Self::Err> {
    Self::parse(s)
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum ConfigSource {
  Default,
  File(Option<PathBuf>),
  Cli,
  Env,
}

pub enum InputConfigSource {
  Default,
  Cwd(PathBuf),
  Cli,
  Env,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub enum ClockConfig {
  System,
  Virtual,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub enum StoreConfig {
  Memory,
  Postgres,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub enum ClientConfig {
  Mock,
  Network,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub enum MailerType {
  Mock,
  Network,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct Config<TyMeta = ()> {
  pub backend: BackendConfig<TyMeta>,
  pub frontend: FrontendConfig<TyMeta>,
  pub postgres: PostgresConfig<TyMeta>,
  pub mailer: MailerConfig<TyMeta>,
  pub scrypt: ScryptConfig<TyMeta>,
  pub opentelemetry: OpentelemetryConfig<TyMeta>,
  pub seed: SeedConfig<TyMeta>,
  pub sqlite: SqliteConfig<TyMeta>,
}

impl Config<ConfigSource> {
  pub fn for_test() -> Self {
    let cwd = std::env::current_dir().expect("failed to resolve cwd");
    let profile = EternaltwinConfigProfile::Test;
    let roots = ConfigRoots::default_value(&profile);
    let chain = ConfigChain::resolve_from_roots(&roots, cwd.as_path(), &profile);
    chain.merge()
  }

  fn patch(mut self, config: ConfigPatch, meta: ConfigSource) -> Self {
    if let SimplePatch::Set(backend) = config.backend {
      self.backend = self.backend.patch(backend, meta.clone());
    }
    if let SimplePatch::Set(frontend) = config.frontend {
      self.frontend = self.frontend.patch(frontend, meta.clone());
    }
    if let SimplePatch::Set(postgres) = config.postgres {
      self.postgres = self.postgres.patch(postgres, meta.clone());
    }
    if let SimplePatch::Set(mailer) = config.mailer {
      self.mailer = self.mailer.patch(mailer, meta.clone());
    }
    if let SimplePatch::Set(scrypt) = config.scrypt {
      self.scrypt = self.scrypt.patch(scrypt, meta.clone());
    }
    if let SimplePatch::Set(opentelemetry) = config.opentelemetry {
      self.opentelemetry = self.opentelemetry.patch(opentelemetry, meta.clone());
    }
    if let SimplePatch::Set(seed) = config.seed {
      let patch = match seed {
        // An explicit `seed = null` is propagated to clear all the seed maps
        None => SeedConfigPatch {
          user: SimplePatch::Set(None),
          app: SimplePatch::Set(None),
          forum_section: SimplePatch::Set(None),
        },
        Some(patch) => patch,
      };
      self.seed = self.seed.patch(patch, meta.clone());
    }
    if let SimplePatch::Set(sqlite) = config.sqlite {
      self.sqlite = self.sqlite.patch(sqlite, meta.clone());
    }
    self
  }

  fn default_for_profile(profile: BuiltinEternaltwinConfigProfile) -> Self {
    Self {
      backend: BackendConfig::default_for_profile(profile),
      frontend: FrontendConfig::default_for_profile(profile),
      postgres: PostgresConfig::default_for_profile(profile),
      mailer: MailerConfig::default_for_profile(profile),
      scrypt: ScryptConfig::default_for_profile(profile),
      opentelemetry: OpentelemetryConfig::default_for_profile(profile),
      seed: SeedConfig::default_for_profile(profile),
      sqlite: SqliteConfig::default_for_profile(profile),
    }
  }
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn test_default_dev_config() {
    // override prod config with the one below, so we rebuild the dev config
    // language=toml
    const INPUT: &str = r#"
[backend]
listen = "[::]:50320"
secret = "dev"
clock = "System"
mailer = "Mock"
store = "Memory"
oauth_client = "Network"

[frontend]
port = 50321
uri = "http://localhost:50321/"
forum_posts_per_page = 10
forum_threads_per_page = 20

[postgres]
host = "localhost"
port = 5432
name = "eternaltwin.dev"
user = "eternaltwin.dev.main"
password = "dev"

[sqlite]
file = "eternaltwin.dev.sqlite"

[mailer]
host = "localhost"
username = "eternaltwin_mailer"
password = "dev"
sender = "support@eternaltwin.localhost"
headers = [
]

[scrypt]
max_time = "100ms"
max_mem_frac = 0.05

[opentelemetry]
enabled = true
grpc_proxy_port = 4317

[opentelemetry.attributes]
[opentelemetry.exporter]
[opentelemetry.exporter.stdout]
type = "Human"
color = "Auto"
target = "eternaltwin://stdout"

[seed]
[seed.user]
[seed.user.alice]
display_name = "Alice"
username = "alice"
password = "aaaaaaaaaa"
is_administrator = true
[seed.user.bob]
display_name = "Bob"
username = "bob"
password = "bbbbbbbbbb"
[seed.user.charlie]
display_name = "Charlie"
username = "charlie"
password = "cccccccccc"
[seed.user.dan]
display_name = "Dan"
username = "dan"
password = "dddddddddd"
[seed.user.eve]
display_name = "Eve"
username = "eve"
password = "eeeeeeeeee"
[seed.user.frank]
display_name = "Frank"
username = "frank"
password = "ffffffffff"

[seed.app]
[seed.app.brute_dev]
display_name = "LaBrute"
uri = "http://localhost:3000/"
oauth_callback = "http://localhost:3000/oauth/callback"
secret = "dev"

[seed.app.emush_dev]
display_name = "eMush"
uri = "http://emush.localhost/"
oauth_callback = "http://emush.localhost/oauth/callback"
secret = "dev"

[seed.app.eternalfest_dev]
display_name = "Eternalfest"
uri = "http://localhost:50313/"
oauth_callback = "http://localhost:50313/oauth/callback"
secret = "dev"

[seed.app.kadokadeo_dev]
display_name = "Kadokadeo"
uri = "http://kadokadeo.localhost/"
oauth_callback = "http://kadokadeo.localhost/oauth/callback"
secret = "dev"

[seed.app.kingdom_dev]
display_name = "Kingdom"
uri = "http://localhost:8000/"
oauth_callback = "http://localhost:8000/oauth/callback"
secret = "dev"

[seed.app.myhordes_dev]
display_name = "MyHordes"
uri = "http://myhordes.localhost/"
oauth_callback = "http://myhordes.localhost/twinoid"
secret = "dev"

[seed.app.neoparc_dev]
display_name = "NeoParc"
uri = "http://localhost:8880/"
oauth_callback = "http://localhost:8880/api/account/callback"
secret = "dev"

[seed.forum_section.main_en]
display_name = "Main Forum (en-US)"
locale = "en-US"

[seed.forum_section.main_fr]
display_name = "Forum Général (fr-FR)"
locale = "fr-FR"

[seed.forum_section.main_es]
display_name = "Foro principal (es-SP)"
locale = "es-SP"

[seed.forum_section.main_de]
display_name = "Hauptforum (de-DE)"
locale = "de-DE"

[seed.forum_section.main_eo]
display_name = "Ĉefa forumo (eo)"
locale = "eo"

[seed.forum_section.eternalfest_main]
display_name = "[Eternalfest] Le Panthéon"
locale = "fr-FR"

[seed.forum_section.emush_main]
display_name = "[eMush] Neron is watching you"
locale = "fr-FR"

[seed.forum_section.drpg_main]
display_name = "[DinoRPG] Jurassic Park"
locale = "fr-FR"

[seed.forum_section.myhordes_main]
display_name = "[Myhordes] Le Saloon"
locale = "fr-FR"

[seed.forum_section.kadokadeo_main]
display_name = "[Kadokadeo] Café des palabres"
locale = "fr-FR"

[seed.forum_section.kingdom_main]
display_name = "[Kingdom] La foire du trône"
locale = "fr-FR"

[seed.forum_section.na_main]
display_name = "[Naturalchimie] Le laboratoire"
locale = "fr-FR"

[seed.forum_section.sq_main]
display_name = "[Studioquiz] Le bar à questions"
locale = "fr-FR"

[seed.forum_section.ts_main]
display_name = "[Teacher Story] La salle des profs"
locale = "fr-FR"

[seed.forum_section.popotamo_main]
display_name = "[Popotamo] Le mot le plus long"
locale = "fr-FR"
"#;
    let actual = ResolvedConfig::parse(INPUT, ConfigFormat::Toml, false, ConfigSource::Default).unwrap();
    let actual = ConfigChain::one(actual, BuiltinEternaltwinConfigProfile::Production).merge();
    let expected = Config::default_for_profile(BuiltinEternaltwinConfigProfile::Dev);
    assert_eq!(actual, expected);
  }
}
