# Packagist PHP registry client

This Packagist PHP registry client is currently used for the needs of [https://eternaltwin.org/](https://eternaltwin.org/).
If you want to claim the name, send an email at `contact@eternaltwin.org`.

This client is based on [the official API documentation](https://packagist.org/apidoc).
