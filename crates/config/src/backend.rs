use crate::{
  BuiltinEternaltwinConfigProfile, ClientConfig, ClockConfig, ConfigSource, ConfigValue, MailerType, StoreConfig,
};
use eternaltwin_core::patch::SimplePatch;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct BackendConfig<TyMeta> {
  pub listen: ConfigValue<String, TyMeta>,
  pub port: ConfigValue<Option<u16>, TyMeta>,
  pub secret: ConfigValue<Option<String>, TyMeta>,
  pub clock: ConfigValue<ClockConfig, TyMeta>,
  pub mailer: ConfigValue<MailerType, TyMeta>,
  pub app_store: ConfigValue<StoreConfig, TyMeta>,
  pub auth_store: ConfigValue<StoreConfig, TyMeta>,
  pub dinoparc_store: ConfigValue<StoreConfig, TyMeta>,
  pub forum_store: ConfigValue<StoreConfig, TyMeta>,
  pub hammerfest_store: ConfigValue<StoreConfig, TyMeta>,
  pub job_store: ConfigValue<StoreConfig, TyMeta>,
  pub link_store: ConfigValue<StoreConfig, TyMeta>,
  pub mailer_store: ConfigValue<StoreConfig, TyMeta>,
  pub oauth_provider_store: ConfigValue<StoreConfig, TyMeta>,
  pub twinoid_store: ConfigValue<StoreConfig, TyMeta>,
  pub user_store: ConfigValue<StoreConfig, TyMeta>,
  pub oauth_client: ConfigValue<ClientConfig, TyMeta>,
}

impl<TyMeta> BackendConfig<TyMeta>
where
  TyMeta: Clone,
{
  pub(crate) fn patch(mut self, config: BackendConfigPatch, meta: TyMeta) -> Self {
    match (config.listen, config.port) {
      (SimplePatch::Set(listen), SimplePatch::Skip) => {
        self.listen = ConfigValue::new_meta(listen, meta.clone());
        self.port = ConfigValue::new_meta(None, meta.clone());
      }
      (SimplePatch::Skip, SimplePatch::Set(port)) => {
        // keep old listen interface
        self.port = ConfigValue::new_meta(Some(port), meta.clone());
      }
      (SimplePatch::Set(listen), SimplePatch::Set(port)) => {
        self.listen = ConfigValue::new_meta(listen, meta.clone());
        self.port = ConfigValue::new_meta(Some(port), meta.clone());
      }
      (SimplePatch::Skip, SimplePatch::Skip) => {
        // nothing to do
      }
    }
    if let SimplePatch::Set(secret) = config.secret {
      self.secret = ConfigValue::new_meta(secret, meta.clone());
    }
    if let SimplePatch::Set(clock) = config.clock {
      self.clock = ConfigValue::new_meta(clock, meta.clone());
    }
    if let SimplePatch::Set(mailer) = config.mailer {
      self.mailer = ConfigValue::new_meta(mailer, meta.clone());
    }
    if let SimplePatch::Set(store) = config.store {
      let value = ConfigValue::new_meta(store, meta.clone());
      self.app_store = value.clone();
      self.auth_store = value.clone();
      self.dinoparc_store = value.clone();
      self.forum_store = value.clone();
      self.hammerfest_store = value.clone();
      self.job_store = value.clone();
      self.link_store = value.clone();
      self.mailer_store = value.clone();
      self.oauth_provider_store = value.clone();
      self.twinoid_store = value.clone();
      self.user_store = value;
    }
    if let SimplePatch::Set(patch) = config.app_store {
      self.app_store = ConfigValue::new_meta(patch, meta.clone());
    }
    if let SimplePatch::Set(patch) = config.auth_store {
      self.auth_store = ConfigValue::new_meta(patch, meta.clone());
    }
    if let SimplePatch::Set(patch) = config.dinoparc_store {
      self.dinoparc_store = ConfigValue::new_meta(patch, meta.clone());
    }
    if let SimplePatch::Set(patch) = config.forum_store {
      self.forum_store = ConfigValue::new_meta(patch, meta.clone());
    }
    if let SimplePatch::Set(patch) = config.hammerfest_store {
      self.hammerfest_store = ConfigValue::new_meta(patch, meta.clone());
    }
    if let SimplePatch::Set(patch) = config.job_store {
      self.job_store = ConfigValue::new_meta(patch, meta.clone());
    }
    if let SimplePatch::Set(patch) = config.link_store {
      self.link_store = ConfigValue::new_meta(patch, meta.clone());
    }
    if let SimplePatch::Set(patch) = config.mailer_store {
      self.mailer_store = ConfigValue::new_meta(patch, meta.clone());
    }
    if let SimplePatch::Set(patch) = config.oauth_provider_store {
      self.oauth_provider_store = ConfigValue::new_meta(patch, meta.clone());
    }
    if let SimplePatch::Set(patch) = config.twinoid_store {
      self.twinoid_store = ConfigValue::new_meta(patch, meta.clone());
    }
    if let SimplePatch::Set(patch) = config.user_store {
      self.user_store = ConfigValue::new_meta(patch, meta.clone());
    }
    self
  }
}

impl BackendConfig<ConfigSource> {
  pub(crate) fn default_for_profile(profile: BuiltinEternaltwinConfigProfile) -> Self {
    let meta = ConfigSource::Default;

    use BuiltinEternaltwinConfigProfile::*;
    let store = match profile {
      Production | Test => ConfigValue::new_meta(StoreConfig::Postgres, meta.clone()),
      Dev | Sdk => ConfigValue::new_meta(StoreConfig::Memory, meta.clone()),
    };
    let client = match profile {
      Production | Dev => ConfigValue::new_meta(ClientConfig::Network, meta.clone()),
      Sdk | Test => ConfigValue::new_meta(ClientConfig::Mock, meta.clone()),
    };
    Self {
      listen: ConfigValue::new_meta(String::from("[::]:50320"), meta.clone()),
      port: ConfigValue::new_meta(None, meta.clone()),
      secret: match profile {
        Production => ConfigValue::new_meta(None, meta.clone()),
        Dev | Sdk | Test => ConfigValue::new_meta(Some("dev".to_string()), meta.clone()),
      },
      clock: match profile {
        Dev | Production => ConfigValue::new_meta(ClockConfig::System, meta.clone()),
        Sdk | Test => ConfigValue::new_meta(ClockConfig::Virtual, meta.clone()),
      },
      mailer: match profile {
        Production => ConfigValue::new_meta(MailerType::Network, meta.clone()),
        Dev | Sdk | Test => ConfigValue::new_meta(MailerType::Mock, meta.clone()),
      },
      app_store: store.clone(),
      auth_store: store.clone(),
      dinoparc_store: store.clone(),
      forum_store: store.clone(),
      hammerfest_store: store.clone(),
      job_store: store.clone(),
      link_store: store.clone(),
      mailer_store: store.clone(),
      oauth_provider_store: store.clone(),
      twinoid_store: store.clone(),
      user_store: store,
      oauth_client: client,
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct BackendConfigPatch {
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub listen: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub port: SimplePatch<u16>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub secret: SimplePatch<Option<String>>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub clock: SimplePatch<ClockConfig>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub mailer: SimplePatch<MailerType>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub store: SimplePatch<StoreConfig>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub app_store: SimplePatch<StoreConfig>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub auth_store: SimplePatch<StoreConfig>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub dinoparc_store: SimplePatch<StoreConfig>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub forum_store: SimplePatch<StoreConfig>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub hammerfest_store: SimplePatch<StoreConfig>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub job_store: SimplePatch<StoreConfig>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub link_store: SimplePatch<StoreConfig>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub mailer_store: SimplePatch<StoreConfig>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub oauth_provider_store: SimplePatch<StoreConfig>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub twinoid_store: SimplePatch<StoreConfig>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub user_store: SimplePatch<StoreConfig>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub oauth_client: SimplePatch<ClientConfig>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub client: SimplePatch<StoreConfig>,
}
