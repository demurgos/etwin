use self::create_session::{CreateSession, TwinoidClientCreateSessionError};
use self::get_me::GetMeShort;
use self::get_users::{GetUsers, GetUsersError};
use crate::core::{Handler, HtmlFragment};
use crate::twinoid::api::{
  Achievement, AchievementData, AchievementProjector, ContactProjector, LikeProjector, Missing, Projector, Select,
  ShortUser, Site, SiteInfo, SiteInfoProjector, SiteProjector, SiteUser, SiteUserProjector, Skip, Stat, StatProjector,
  UrlRef, User, UserProjector,
};
use crate::twinoid::client::get_me::GetMeShortError;
use crate::twinoid::TwinoidSession;
use async_trait::async_trait;
use std::ops::Deref;

pub mod create_session;
pub mod get_me;
pub mod get_site;
pub mod get_user;
pub mod get_users;

pub type FullStatProjector = StatProjector<
  // id
  Select,
  // score
  Select,
  // name
  Select,
  // icon
  Select,
  // description
  Select,
  // rare
  Select,
  // social
  Select,
>;
pub type FullStat = <FullStatProjector as Projector<Stat>>::Projection;

pub type StatMetadataProjector = StatProjector<
  // id
  Select,
  // score
  Skip,
  // name
  Select,
  // icon
  Select,
  // description
  Select,
  // rare
  Select,
  // social
  Select,
>;
pub type StatMetadata = <StatMetadataProjector as Projector<Stat>>::Projection;

impl<TyScore> Stat<String, TyScore, String, Option<UrlRef>, Option<HtmlFragment>, i32, bool> {
  pub fn into_stat_metadata(self) -> StatMetadata {
    Stat {
      id: self.id,
      score: Missing,
      name: self.name,
      icon: self.icon,
      description: self.description,
      rare: self.rare,
      social: self.social,
    }
  }
}

pub type FullAchievementProjector = AchievementProjector<
  // id
  Select,
  // name
  Select,
  // stat
  Select,
  // score
  Select,
  // points
  Select,
  // npoints
  Select,
  // description
  Select,
  // data
  Select,
  // date
  Select,
  // index
  Select,
>;
pub type FullAchievement = <FullAchievementProjector as Projector<Achievement>>::Projection;

pub type AchievementMetadataProjector = AchievementProjector<
  // id
  Select,
  // name
  Select,
  // stat
  Select,
  // score
  Select,
  // points
  Select,
  // npoints
  Select,
  // description
  Select,
  // data
  Select,
  // date
  Skip,
  // index
  Select,
>;
pub type AchievementMetadata = <AchievementMetadataProjector as Projector<Achievement>>::Projection;

impl<TyDate> Achievement<String, String, String, i32, i32, f64, HtmlFragment, AchievementData, TyDate, i32> {
  pub fn into_achievement_metadata(self) -> AchievementMetadata {
    Achievement {
      id: self.id,
      name: self.name,
      stat: self.stat,
      score: self.score,
      points: self.points,
      npoints: self.npoints,
      description: self.description,
      data: self.data,
      date: Missing,
      index: self.index,
    }
  }
}

pub type FullSiteUserProjector = SiteUserProjector<
  // site
  Select,
  // realId
  Select,
  // user
  Skip,
  // link
  Select,
  // stats
  FullStatProjector,
  // achievements
  FullAchievementProjector,
  // points
  Select,
  // npoints
  Select,
  // isBetaTester
  Skip,
>;
pub type FullSiteUser = <FullSiteUserProjector as Projector<SiteUser>>::Projection;

pub type FullUserRecProjector<TyContactUserProjector> = UserProjector<
  // id
  Select,
  // name
  Select,
  // picture
  Select,
  // locale
  Select,
  // title
  Select,
  // oldNames
  Select,
  // sites
  FullSiteUserProjector,
  // like
  SafeLikeProjector,
  // gender
  Select,
  // birthday
  Select,
  // city
  Select,
  // country
  Select,
  // desc
  Select,
  // status
  Select,
  // contacts
  ContactProjector<
    // friend
    Select,
    // user.id
    TyContactUserProjector,
  >,
  // groups
  Skip,
  // devApps
  Skip,
>;

/// Full user with one level of recursion through contacts
pub type FullUserProjector = FullUserRecProjector<Select>;
/// Full user with two levels of recursion through contacts
pub type FullUser2Projector = FullUserRecProjector<FullUserProjector>;
pub type FullUser = <FullUserProjector as Projector<User>>::Projection;
pub type FullUser2 = <FullUser2Projector as Projector<User>>::Projection;

pub type SafeUserRecProjector<TyContactUserProjector> = UserProjector<
  // id
  Select,
  // name
  Skip,
  // picture
  Select,
  // locale
  Select,
  // title
  Select,
  // oldNames
  Skip,
  // sites
  FullSiteUserProjector,
  // like
  SafeLikeProjector,
  // gender
  Select,
  // birthday
  Select,
  // city
  Skip,
  // country
  Skip,
  // desc
  Skip,
  // status
  Skip,
  // contacts
  ContactProjector<
    // friend
    Select,
    // user.id
    TyContactUserProjector,
  >,
  // groups
  Skip,
  // devApps
  Skip,
>;

/// Safe user with one level of recursion through contacts
pub type SafeUserProjector = SafeUserRecProjector<Select>;
/// Safe user with two levels of recursion through contacts
pub type SafeUser2Projector = SafeUserRecProjector<SafeUserProjector>;
pub type SafeUser = <SafeUserProjector as Projector<User>>::Projection;
pub type SafeUser2 = <SafeUser2Projector as Projector<User>>::Projection;

// url, likes, title
pub type FullLikeProjector = LikeProjector<Select, Select, Select>;
// url, likes
pub type SafeLikeProjector = LikeProjector<Select, Select, Skip>;

pub type FullSiteInfoProjector = SiteInfoProjector<
  // #[twinoid(required, rename = "id")]
  Select,
  // #[twinoid(rename = "site")]
  Skip,
  // #[twinoid(rename = "lang")]
  Select,
  // #[twinoid(rename = "cover")]
  Select,
  // #[twinoid(rename = "tagLine")]
  Select,
  // #[twinoid(rename = "description")]
  Select,
  // #[twinoid(rename = "tid")]
  Select,
  // #[twinoid(rename = "icons")]
  Select,
>;
pub type FullSiteInfo = <FullSiteInfoProjector as Projector<SiteInfo>>::Projection;

pub type FullSiteProjector = SiteProjector<
  // #[twinoid(required, rename = "id")]
  Select,
  // #[twinoid(rename = "name")]
  Select,
  // #[twinoid(rename = "host")]
  Select,
  // #[twinoid(rename = "icon")]
  Select,
  // #[twinoid(rename = "lang")]
  Select,
  // #[twinoid(rename = "like")]
  FullLikeProjector,
  // #[twinoid(rename = "infos")]
  FullSiteInfoProjector,
  // #[twinoid(rename = "me")]
  Skip,
  // #[twinoid(rename = "status")]
  Select,
>;
pub type FullSite = <FullSiteProjector as Projector<Site>>::Projection;

#[async_trait]
pub trait TwinoidClient:
  Send
  + Sync
  + Handler<GetMeShort>
  + Handler<GetUsers<FullUser2Projector>>
  + Handler<GetUsers<FullUserProjector>>
  + Handler<GetUsers<SafeUser2Projector>>
  + Handler<GetUsers<SafeUserProjector>>
  + Handler<CreateSession>
{
  async fn get_users_full2(&self, req: GetUsers<FullUser2Projector>) -> Result<Vec<FullUser2>, GetUsersError> {
    Handler::<GetUsers<FullUser2Projector>>::handle(self, req).await
  }
  async fn get_users_full1(&self, req: GetUsers<FullUserProjector>) -> Result<Vec<FullUser>, GetUsersError> {
    Handler::<GetUsers<FullUserProjector>>::handle(self, req).await
  }
  async fn get_users_safe2(&self, req: GetUsers<SafeUser2Projector>) -> Result<Vec<SafeUser2>, GetUsersError> {
    Handler::<GetUsers<SafeUser2Projector>>::handle(self, req).await
  }
  async fn get_users_safe1(&self, req: GetUsers<SafeUserProjector>) -> Result<Vec<SafeUser>, GetUsersError> {
    Handler::<GetUsers<SafeUserProjector>>::handle(self, req).await
  }
  async fn get_me_short(&self, req: GetMeShort) -> Result<ShortUser, GetMeShortError> {
    Handler::<GetMeShort>::handle(self, req).await
  }
  async fn create_session(&self, req: CreateSession) -> Result<TwinoidSession, TwinoidClientCreateSessionError> {
    Handler::<CreateSession>::handle(self, req).await
  }
}

impl<T> TwinoidClient for T where
  T: Send
    + Sync
    + Handler<GetMeShort>
    + Handler<GetUsers<FullUser2Projector>>
    + Handler<GetUsers<FullUserProjector>>
    + Handler<GetUsers<SafeUser2Projector>>
    + Handler<GetUsers<SafeUserProjector>>
    + Handler<CreateSession>
{
}

/// Like [`Deref`], but the target has the bound [`TwinoidClient`]
pub trait TwinoidClientRef: Send + Sync {
  type TwinoidClient: TwinoidClient + ?Sized;

  fn twinoid_client(&self) -> &Self::TwinoidClient;
}

impl<TyRef> TwinoidClientRef for TyRef
where
  TyRef: Deref + Send + Sync,
  TyRef::Target: TwinoidClient,
{
  type TwinoidClient = TyRef::Target;

  fn twinoid_client(&self) -> &Self::TwinoidClient {
    self.deref()
  }
}
