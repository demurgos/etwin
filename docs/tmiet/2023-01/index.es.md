# 2023-01: Este Mes En Eternaltwin #16

[Eternaltwin](https://eternaltwin.org) es un proyecto con el objetivo de preservar
los juegos de Motion Twin y sus comunidades de jugadores.

# Eternaltwin

_Evian_ arregló Skywar en [la aplicación de Eternaltwin](https://eternaltwin.org/docs/desktop),
versión 0.6.6.

# eMush

Bienvenidos a esta sección dedicada a [🍄 eMush](https://emush.eternaltwin.org/),
el proyecto dedicado a salvar Mush.

## ¿Qué hay de nuevo?

El año pasado mencionamos que los administradores podrían crear
naves personalizadas.

![eMush](./emush0.png)

¡Y estamos felices de anunciarles que esta característica está casi completa! 🎉
Es un gran paso para la alfa abierta de eMush.

Gracias a nuestro panel de administrador, podremos añadir nuevos objetos, personajes, armas...
¡sin reiniciar la base de datos cada vez!

Esto nos permite enfocarnos en los próximos pasos, mostrando sus logros. Dado que una imagen
vale más que mil palabras, les mostraremos nuestro progreso:

![eMush](./emush1.png)

![eMush](./emush2.png)

![eMush](./emush3.png)

Bastante bien, ¿eh? 😄

## Lo que se viene

Como recordatorio, aquí están las metas que nos hemos propuesto para la próxima versión de eMush:

- 🇬🇧 traducciones al inglés ✅
- 🗃️ archivo de la nave, para mantener un registro de tus logros 🚧
- 💾 migraciones de base de datos, para desplegar nuevas versiones sin tener que reiniciar las
naves existentes 🚧

Estamos viendo buen progreso en todas estas tareas. Todo lo que nos queda es un par
de pequeños problemas técnicos. Nunca antes estuvimos tan cerca de la alfa abierta.

## ¡Genial! ¿Cuándo?

Como siempre, les seguiremos informando a través de EMEET. La mejor manera de
seguir el progreso es unirse al [canal de Discord de EternalTwin](https://discord.gg/SMJavjs3gk).

eMush es un proyecto de Código Abierto. También puedes seguir nuestro progreso en GitLab.
En particular, el progreso de [la actualización **Spores for All!**](https://gitlab.com/eternaltwin/mush/mush/-/milestones/12#tab-issues).

¡Muchas gracias por su atención, los veré luego a bordo del Daedalus!

# MyHordes

Juega [MyHordes](https://myhordes.eu/).

_ruido de radio_

Hola a todos, espero que todo esté bien para ustedes en esta temporada fría y con mucho viento _interferencia_

![MyHordes - Anuncio de tecnología inalámbrica](./myhordes.es.png)

Gracias al trabajo duro de nuestros desarrolladores, la Temporada 15 es inminente. Como
un recordatorio _interferencia_ todo estará listo para el 11 de febrero. Así que
prepárense para _interferencia_

También estarán feliz de notar que el Altar no está dañado y oxidado como siempre.
Después de todo, no podemos dejar a esos muertos vivientes arruinarlo todo. _ruido de radio_

Una pequeña palabra acerca de los 43 actos de canibalismo... _interferencia_ Hmmmmm... No, nada
para agregar.

Creo que no hay nada más, ¿verdad, Ben? _interferencia_ ¡Pero cierra esta
maldita puerta! _interferencia_

# Eternal DinoRPG

Bienvenido a esta sección dedicada a [Eternal DinoRPG](https://dinorpg.eternaltwin.org/).

Hace unos días desplegamos la versión 0.5.0, la cual añade el "Pigmou" y
las primeras misiones. ¡Ya vemos aproximadamente 60 Pigmous y 30 misiones completadas!

Ya nos encontramos trabajando en la próxima versión. El contenido que añadirá: ¡podrán hablarle
a Personajes No Jugadores (PNJs) y acceder a nuevas misiones!

# Eternal Kingdom

¡Hola a todos, aquí están las noticias para [Eternal Kingdom](https://kingdom.eternaltwin.org/)!

Luego del lanzamiento de la versión 5 pre-Alfa ([mira los detalles](https://eternaltwin.org/forum/threads/9074673d-a530-4780-9fc2-4ebc8743152a)),
hubo cientos de jugadores uniéndose a nuestro mapa de prueba. ¡Muchísimas gracias a530-4780-9fc2-4ebc8743152a
todos por su interés!

Un nuevo miembro se ha unido a nuestro equipo: _NSunshine_. Él está trabajando en actualizar
la interfaz de la unidad de transferencia. Talsi se encuentra trabajando en mejorar el mapa (nuevo
motor de renderizado, evolución de pueblos, generales, y demás). También está trabajando en traducir
los nombres de las ciudades en diferentes lenguajes. Estamos trabajando en la versión Alfa 5. Se concentrará
en los generales (transferencias de unidades, movimiento), salud y comercio.

¡Muchas gracias nuevamente por todo el apoyo, mensajes, y reportes de bugs! ¡Nos vemos pronto!

# Eternalfest

Pronto será el décimo aniversario de [Eternalfest](https://eternalfest.net),
el proyecto que inició en el 5 de marzo de 2013. Para celebrarlo, ya hemos desplegado
una actualización _enorme_ en enero.

**Ahora puedes ver los objetos que has coleccionado a lo largo de los años en la nueva pestaña
_Mi Perfil_.** Ésta era la característica más pedida, ¡y finalmente está aquí! 🎉
Seguiremos mejorando la página del perfil en las próximas actualizaciones

La otra gran actualización es el soporte completo para las traducciones dentro del juego.
El primer juego en utilizar esta característica está programado para ser lanzado en febrero.

Hay un montón de otras características. Ahora puedes agregar juegos a tus favoritos, los juegos
pueden tener múltiples versiones en paralelo (por ejemplo: "principal" y "beta"), puedes programar
cualquier cantidad de actualizaciones por adelantado, entre otras.

Estas características fueron posible gracias a _Demurgos_ y _Moulins_, quieren reconstruyeron el
sistema de administración de juegos para que sea más flexible. Esto fue una tarea de largo plazo que está
finalmente completada. Nos permitirá completar la migración de Eternalfest al lenguaje de programación Rust.
Este nuevo sistema de juego también será importando a Eternaltwin y servirá como la base para una integración
mejorada con los diferentes juegos (perfiles de juegos, recompensas, y mucho más).

# Neoparc

Hola, [maestros Dinoz](https://neoparc.eternaltwin.org)!

**¡La primer guerra de clanes ya ha oficialmente comenzado!** Encontrarás los detalles en la sección
de _Ayuda_ de nuestro sitio web. ¡Esperamos que alcancen gloria y fortuna! ¡Mucha suerte y
no tengan piedad alguna!

![Neoparc - Clan War](./neoparc.png)

También hemos publicado varios arreglos, en particular respecto al Centro de
Misiones de Dinovilla. Ahora recibirás un _Pruniac_ cuando un huevo eclosione. Las
recompensas de las incursiones han sido disminuidas (por ejemplo: no más Pociones de Bruja Lola gratis)

Finalmente, la habilidad "Ingeniero" ahora tiene un uso: incrementa la durabilidad de las cervezas Dinojak,
encantamientos prismáticos y objetos vendidos en la _Caverna de la Anomalía_

# Palabras finales

Pueden enviarnos [sus mensajes de febrero](https://gitlab.com/eternaltwin/etwin/-/issues/58).

Este artículo ha sido editado por: Bibni, Biosha, Demurgos, Dylan57, Evian,
Fer, Jonathan, Patate404 y Schroedinger.
