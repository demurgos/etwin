import {Server} from "node:http";

import type {InjectionToken, StaticProvider, Type} from "@angular/core";
import {Config} from "@eternaltwin/core/config/config";
import {$LocaleId} from "@eternaltwin/core/core/locale-id";
import {Url} from "@eternaltwin/core/core/url";
import {ForumConfig} from "@eternaltwin/core/forum/forum-config";
import {koaProxy} from "@eternaltwin/koa-proxy";
import koaCors from "@koa/cors";
import Router, {RouterContext} from "@koa/router";
import {Axios} from "axios";
import chalk from "chalk";
import * as furi from "furi";
import Koa, {type Request} from "koa";
import koaLogger from "koa-logger";
import koaMount from "koa-mount";
import koaStaticCache from "koa-static-cache";

import {ServerAppConfig} from "../server/config.mjs";
import {FrontendConfig} from "./config.mjs";
import {createKoaLocaleNegotiator, LocaleNegotiator} from "./koa-locale-negotiation.mjs";
import {LocaleId, NgLocalId, toNgLocale} from "./locales.mjs";
import {NgKoaEngine} from "./ng-koa-engine.mjs";

/**
 * Interface for the SSR module built by Angular
 *
 * This corresponds to the exports of `./src/server/ssr.ts`.
 */
interface SsrModule {
  CommonEngine: any,
  AppServerModule: Type<{}>,
  BROWSER_DIR: URL,
  SERVER_DIR: URL,
  SERVER_INDEX: URL,
  REQUEST: InjectionToken<Request>,
  APP_BASE_HREF: InjectionToken<string>,
  BACKEND_URI: InjectionToken<string>,
  CONFIG: InjectionToken<Config>,
  INTERNAL_AUTH_KEY: InjectionToken<string>,
  ROUTES: readonly string[],
}

export async function main(config: FrontendConfig): Promise<void> {
  const textEncoder = new TextEncoder();
  const backendSecret = textEncoder.encode(config.backendSecret);

  console.log("Frontent configuration:");
  console.log(`backendPort = ${config.backendPort}`);
  console.log(`port = ${config.port}`);
  console.log(`appDir = ${config.appDir}`);
  console.log(`url = ${config.url}`);
  console.log("");
  console.log("[Forum]");
  console.log(`Threads per page: ${config.forum.threadsPerPage}`);
  console.log(`Posts per page: ${config.forum.postsPerPage}`);

  const ngApps = await importSsr();
  const defaultApp: SsrModule | undefined = ngApps.get("en-US");
  if (defaultApp === undefined) {
    throw new Error("missing app for locale en-US (default)");
  }

  const appConfig: ServerAppConfig = {
    externalUri: config.url,
    isProduction: config.isProduction,
    internalAuthKey: backendSecret,
    forum: {postsPerPage: config.forum.postsPerPage, threadsPerPage: config.forum.threadsPerPage},
    backendPort: config.backendPort,
  };

  const appRouters: Map<LocaleId, Koa> = new Map();
  for (const localeId of $LocaleId.values) {
    const ngLocaleId = toNgLocale(localeId);
    const app = ngApps.get(ngLocaleId);
    if (app === undefined) {
      throw new Error(`missing app for locale: ${localeId} (Angular locale ${ngLocaleId})`);
    }
    const appRouter: Koa = await loadAppRouter(app, appConfig);
    appRouters.set(localeId, appRouter);
  }
  console.log(`Available locales: ${[...appRouters.keys()].join(", ")}`);
  const defaultRouter: Koa | undefined = await loadAppRouter(defaultApp, appConfig);

  const router: Koa = new Koa();

  router.use(koaLogger());
  if (!config.isProduction) {
    // make sure that there is no trailing slash in the cors header, or it will not work
    const angularLiveServer = "http://localhost:4200/";
    console.info(`dev env: enable CORS from ${angularLiveServer}`);
    router.use(koaCors({origin: angularLiveServer, credentials: true}));

    console.info("dev env: enable Koa proxy for the backend");
    const backendUri = new URL("http://localhost/");
    backendUri.port = config.backendPort.toString(10);
    const middleware = koaProxy({target: backendUri,});
    router.use(async (cx, next) => {
      if (cx.path.startsWith("/api/v1/") || cx.path.startsWith("/oauth/") || cx.path.startsWith("/actions/") || cx.path.startsWith("/v1/traces/")) {
        await middleware(cx, next);
        console.log("  " + [chalk.gray("-->"), chalk.bold(cx.method), chalk.gray(cx.originalUrl), chalk.gray("(PROXY)")].join(" "));
      } else {
        await next();
      }
    });
  }

  const ONE_DAY: number = 24 * 3600;
  // Just use the parent dir of the default app to find the browser root (kinda hacky)
  const staticDir = furi.join(defaultApp.BROWSER_DIR, "..");
  router.use(koaStaticCache(furi.toSysPath(staticDir), {maxAge: ONE_DAY}));

  const i18nRouter: Koa = createI18nRouter(defaultRouter, appRouters);
  router.use(koaMount("/", i18nRouter as any as Koa.Middleware));

  console.log("checking backend server");
  try {
    const backendUri = new URL("http://localhost/api/v1");
    backendUri.port = config.backendPort.toString(10);
    console.log(`backendUri = ${backendUri}`);
    const agent = new Axios({});
    const res = await agent.get(backendUri.toString());
    const meta = res.data;
    console.log(meta);
  } catch (e) {
    if (!(e instanceof Error)) {
      throw e;
    }
    console.error("failed to reach backend server");
    const sourceErrors = (e as any).errors;
    if (Array.isArray(sourceErrors)) {
      console.error(`AggregateError: sourceErrors.length = ${sourceErrors.length}`);
      for (const err of sourceErrors) {
        console.error(`- ${err.message}`);
      }
    } else {
      console.error(e.message);
    }
    console.error("if running locally, start the backend server with `cargo eternaltwin backend`");
  }

  const server: Server = router.listen(config.port ?? 0, () => {
    const addressInfo = server.address();
    let listenInfo: string;
    if (typeof addressInfo === "string") {
      listenInfo = `unix socket ${furi.fromSysPath(addressInfo)}`;
    } else if (addressInfo !== null) {
      listenInfo = `port ${addressInfo.port}`;
    } else {
      throw new Error("failed to bind frontend server");
    }
    console.log(`SSR server ready on ${listenInfo}, externally available at ${config.url}`);
  });
}

function createI18nRouter(defaultRouter: Koa, localizedRouters: Map<LocaleId, Koa>): Koa {
  const router: Koa = new Koa();

  const localeNegotiator: LocaleNegotiator<Koa.Context> = createKoaLocaleNegotiator({
    cookieName: "locale",
    queryName: "l",
    supportedLocales: localizedRouters.keys(),
  });

  const defaultMiddleware: Koa.Middleware = koaMount(defaultRouter as any as Koa.Middleware) as Koa.Middleware;
  const localizedMiddlewares: Map<LocaleId, Koa.Middleware> = new Map();
  for (const [locale, app] of localizedRouters) {
    localizedMiddlewares.set(locale, koaMount(app as any as Koa.Middleware) as Koa.Middleware);
  }

  router.use(async (cx, next) => {
    const locale: LocaleId | undefined = localeNegotiator(cx);
    if (locale !== undefined) {
      const middleware: Koa.Middleware | undefined = localizedMiddlewares.get(locale);
      if (middleware !== undefined) {
        return middleware(cx, next);
      }
      // We matched a locale but don't have a corresponding router
      // TODO: Log warning? We should never reach this point since available
      //       locales are generated from available routers.
    }
    return defaultMiddleware(cx, next);
  });

  return router;
}

async function loadAppRouter(app: SsrModule, serverAppConfig: ServerAppConfig): Promise<Koa> {
  return ngToKoa(app, serverAppConfig);
}

async function importSsr(): Promise<Map<NgLocalId, SsrModule>> {
  return new Map([
    ["de", await import("#ssr-de-de")],
    ["en-US", await import("#ssr-en-us")],
    ["es", await import("#ssr-es-sp")],
    ["fr", await import("#ssr-fr-fr")],
  ]);
}

function resolveServerOptions(options?: Partial<ServerAppConfig>): ServerAppConfig {
  let isProduction: boolean = false;
  if (options === undefined) {
    options = {};
  } else {
    isProduction = options.isProduction === true;
  }
  const externalUri: Url | undefined = options.externalUri;
  if (isProduction) {
    if (externalUri === undefined) {
      throw new Error("Aborting: Missing server option `externalBaseUri` in production mode");
    }
  }
  if (options.internalAuthKey === undefined) {
    throw new Error("Missing `internalAuthKey` configuration");
  }
  const internalAuthKey: Uint8Array = options.internalAuthKey;
  if (options.forum === undefined) {
    throw new Error("Missing `forum` configuration");
  }
  const forum: ForumConfig = options.forum;
  const backendPort: undefined | number = options.backendPort;
  if (typeof backendPort !== "number") {
    throw new Error("Missing `backendPort` configuration");
  }
  return {externalUri, isProduction, internalAuthKey, forum, backendPort};
}

/**
 * Resolves the fully qualified URL from the path and query
 */
function fullyQualifyUrl(options: ServerAppConfig, pathAndQuery: string): Url {
  if (options.externalUri !== undefined) {
    return new Url(pathAndQuery, options.externalUri);
  } else {
    return new Url(pathAndQuery, "http://localhost/");
  }
}

// The Express app is exported so that it can be used by serverless Functions.
export async function ngToKoa(app: SsrModule, options?: Partial<ServerAppConfig>): Promise<Koa> {
  const config: ServerAppConfig = resolveServerOptions(options);

  // const serverDir = app.SERVER_DIR;
  const serverIndex = app.SERVER_INDEX;
  const browserDir = app.BROWSER_DIR;

  const koa = new Koa();

  const providers: StaticProvider[] = [];
  if (config.externalUri !== undefined) {
    providers.push({provide: app.APP_BASE_HREF, useValue: config.externalUri.toString()});
  }
  providers.push({provide: app.CONFIG, useValue: {forum: config.forum}});

  const engine: NgKoaEngine = await NgKoaEngine.create({
    CommonEngine: app.CommonEngine,
    serverIndex,
    bootstrap: app.AppServerModule,
    providers,
    browserDir,
  });

  const router = new Router();
  // TODO: Fix `koajs/router` type definitions to accept a readonly ROUTES.
  router.get([...app.ROUTES], ngRender);
  koa.use(router.routes());
  koa.use(router.allowedMethods());

  async function ngRender(cx: RouterContext): Promise<void> {
    const backendUri = new URL("http://localhost/");
    backendUri.port = config.backendPort.toString(10);
    const reqUrl: Url = fullyQualifyUrl(config, cx.request.originalUrl);
    cx.response.body = await engine.render({
      url: reqUrl,
      providers: [
        {provide: app.REQUEST, useValue: cx.request},
        {provide: app.BACKEND_URI, useValue: backendUri.toString()},
        {provide: app.INTERNAL_AUTH_KEY, useValue: Buffer.from(config.internalAuthKey).toString("utf8")},
      ],
    });
  }

  // const ONE_DAY: number = 24 * 3600;
  // koa.use(koaStaticCache(furi.toSysPath(browserDir), {maxAge: ONE_DAY}));

  return koa;
}
