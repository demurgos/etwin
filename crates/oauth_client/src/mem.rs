use crate::{GetAccessTokenError, RfcOauthClient};
use ::url::Url;
use async_trait::async_trait;
use eternaltwin_core::oauth::{RfcOauthAccessToken, RfcOauthAccessTokenKey, RfcOauthTokenType};
use std::collections::HashMap;
use std::str::FromStr;
use std::sync::RwLock;

pub struct MemRfcOauthClient {
  state: RwLock<State>,
}

struct State {
  tokens: HashMap<String, Option<RfcOauthAccessToken>>,
}

impl State {
  pub fn new() -> Self {
    Self { tokens: HashMap::new() }
  }

  pub fn grant_access_token(&mut self) -> String {
    let code = "todo: generate token string".to_string();
    self.tokens.insert(
      code.clone(),
      Some(RfcOauthAccessToken {
        token_type: RfcOauthTokenType::Bearer,
        access_token: RfcOauthAccessTokenKey::from_str("todo").expect("hardcoded token is valid"),
        expires_in: 0,
        refresh_token: None,
      }),
    );
    code
  }

  pub fn get_access_token(&mut self, code: &str) -> Result<RfcOauthAccessToken, GetAccessTokenError> {
    let token = self.tokens.get_mut(code).ok_or(GetAccessTokenError::BadCode)?;
    token.take().ok_or(GetAccessTokenError::CodeReuse)
  }
}

impl Default for MemRfcOauthClient {
  fn default() -> Self {
    Self::new()
  }
}

impl MemRfcOauthClient {
  pub fn new() -> Self {
    Self {
      state: RwLock::new(State::new()),
    }
  }

  pub fn grant_access_token(&self) -> String {
    let mut state = self.state.write().expect("failed to get MemRfcOauthClient state");
    state.grant_access_token()
  }
}

#[async_trait]
impl RfcOauthClient for MemRfcOauthClient {
  fn authorization_uri(&self, _scope: &str, _state: &str) -> Url {
    Url::parse("http://localhost/todo").unwrap()
  }

  fn authorization_server(&self) -> &str {
    "localhost"
  }

  async fn get_access_token(&self, code: &str) -> Result<RfcOauthAccessToken, GetAccessTokenError> {
    let mut state = self.state.write().expect("failed to get MemRfcOauthClient state");
    state.get_access_token(code)
  }
}

#[cfg(feature = "neon")]
impl neon::prelude::Finalize for MemRfcOauthClient {}
