# 2022-09: Dieser Monat in Eternaltwin #12

[Eternaltwin](https://eternaltwin.org) ist ein Projekt mit dem Ziel, die Spiele und Spielergemeinschaften von Motion Twin zu erhalten.

# Eternaltwin

Alle Spiele wurden in das neue Bereitstellungssystem übertragen. Die Projektleiter können nun jederzeit neue Versionen hochladen.

# eMush

Willkommen in diesem Bereich, der [eMush](https://emush.eternaltwin.org/) gewidmet ist, dem Projekt zur Rettung von Mush.

## Was ist neu?

Das letzte Mal haben wir im Juli-August 2022 etwas berichtet. Wir haben gerade damit begonnen
an den Krankheiten für das **Disease-Update** zu arbeiten.

![eMush1](./emush1.png)

Wir haben weiter an diesem Update gearbeitet. Wir freuen uns jetzt, **dass alle Krankheiten** - physische, psychische oder Wunden - **funktionieren**!

![eMush2](./emush2.png)

Ein weiteres wichtiges Merkmal war die Einführung eines **Administrationsbereichs**.

![eMush3](./emush3.png)

Damit können wir die Spieleinstellungen leichter verwalten: Wir müssen die Datenbank nicht jedes Mal neu zurücksetzen.

Dies bringt uns unserem mittelfristigen Ziel näher: eine offene Alpha-Testphase.

## Demnächst

Jetzt, wo die Krankheiten erledigt sind, arbeiten wir daran, wie man sie tatsächlich bekommt. Die meisten
Wege sind bereits abgeschlossen: **Zufall, Lebensmittelvergiftung, Geschlechtskrankheiten, Sporen...**

![eMush4](./emush4.png)

Das wichtigste fehlende Element sind durch **Waffen** verursachte Wunden. Das wird unser nächster
Schwerpunkt der Updates sein.

Wir müssen auch noch Tooltips zu den Krankheitssymptomen bereitstellen (das könnte nützlich sein)
und dann sind wir fertig mit dem **Disease-Update**!

(Eine neue Alpha ist also ziemlich nah.)

## Großartig! Wann? Wie wird gespielt?

Zurzeit können wir noch keinen Termin nennen. Wir werden euch auf dem Laufenden halten, sobald wir
einen Termin festgelegt haben.

Wenn ihr nicht warten könnt, ist der beste Weg, um in Kontakt zu bleiben und den Fortschritt zu verfolgen, [dem Eternaltwin-Discord](https://discord.gg/SMJavjs3gk) beizutreten. Das zeigt auch eure
Unterstützung!

eMush ist ein Open-Source-Projekt, unser Fortschritt ist auch [auf Gitlab](https://gitlab.com/eternaltwin/mush/mush/-/milestones/8#tab-issues) verfügbar.

Vielen Dank für eure Aufmerksamkeit, und bis bald an Bord der Daedalus!

# Neoparc

Es ist der inoffizielle Beginn des **Clankriegs** auf [Neoparc](https://neoparc.eternaltwin.org/)!

Hallo Dinoz-Meister!

Wir haben einige Updates für euren geliebten Clan. Ihr könnt jetzt anfangen, eure Totems zu füllen
und Punkte für den laufenden Clankrieg zu sammeln.

Seid vorsichtig, dieser erste Clankrieg braucht noch einige Anpassungen und Balancing.
Es könnte am Anfang noch ein paar Probleme geben. Bis zum nächsten Update wünsche ich viel Glück bei euren Ausgrabungen!

Aber Vorsicht, der Krieg ist nicht die einzige Gefahr. Es gibt Gerüchte über gespenstische Geister
die im Dinoland umherstreifen! Mach mit bei dem **Halloween-Event**!

Ach ja, noch etwas: Ihr könnt eure Dinoz-Daten jetzt über das Menü "Mein Konto" herunterladen.

Viel Spaß!

# MyHordes

Spiele es hier: [MyHordes](https://myhordes.eu/).

_Radioknistern_

Hallo an alle, ich hoffe, alles ist gut für euch, und vor allem denkt daran, _undeutlich_

![MyHordes - Werbung für drahtlose Technologie](./myhordes.de.png)

Die Jagd nach Bugs geht weiter! Unser Entwicklerteam, begleitet von 2 Wächtern
und einem Späher, reisen durch die gesamte Außenwelt, um euch mit den besten _undeutlich_

Die Vorbereitungen für die 15. Saison sind im Gange, seid sic _undeutlich_

Außerdem hat man mir gesagt, dass die Schamanen endlich verstanden haben, dass es sinnlos ist, dieselbe Fläche zweimal zu bewässern! _Radioknistern_

_hust knister... hust_

# Abschließende Worte

Du kannst deine [Nachrichten für Oktober](https://gitlab.com/eternaltwin/etwin/-/issues/54) senden.

Dieser Artikel wurde bearbeitet von: Demurgos, Dylan57, Evian, Fer, Jonathan, Patate404 und Schroedinger.
