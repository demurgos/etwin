use crate::core::{FinitePeriod, Handler, Instant, Listing};
use crate::digest::{DigestSha2_256, DigestSha3_256};
use crate::email::EmailAddress;
use crate::mailer::store::acquire_outbound_email_request::{
  AcquireOutboundEmailRequest, AcquireOutboundEmailRequestError,
};
use crate::mailer::store::create_outbound_email::{CreateOutboundEmail, CreateOutboundEmailError};
use crate::mailer::store::get_outbound_email_requests::{GetOutboundEmailRequests, GetOutboundEmailRequestsError};
use crate::mailer::store::get_outbound_emails::{GetOutboundEmails, GetOutboundEmailsError};
use crate::mailer::store::release_outbound_email_request::{
  ReleaseOutboundEmailRequest, ReleaseOutboundEmailRequestError,
};
use crate::user::UserIdRef;
use async_trait::async_trait;
#[cfg(feature = "serde")]
use eternaltwin_serde_tools::{Deserialize, Serialize};
use std::ops::Deref;

pub mod acquire_outbound_email_request;
pub mod create_outbound_email;
pub mod get_outbound_email_requests;
pub mod get_outbound_emails;
pub mod release_outbound_email_request;

declare_new_uuid! {
  pub struct OutboundEmailId(Uuid);
  pub type ParseError = OuboundEmailIdParseError;
  const SQL_NAME = "outbound_email_id";
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "User"))]
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct OutboundEmailIdRef {
  pub id: OutboundEmailId,
}

impl OutboundEmailIdRef {
  pub const fn new(id: OutboundEmailId) -> Self {
    Self { id }
  }
}

impl From<OutboundEmailId> for OutboundEmailIdRef {
  fn from(id: OutboundEmailId) -> Self {
    Self::new(id)
  }
}

declare_new_uuid! {
  pub struct OutboundEmailRequestId(Uuid);
  pub type ParseError = OutboundEmailRequestIdParseError;
  const SQL_NAME = "outbound_email_request_id";
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "User"))]
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct OutboundEmailRequestIdRef {
  pub id: OutboundEmailRequestId,
}

impl OutboundEmailRequestIdRef {
  pub const fn new(id: OutboundEmailRequestId) -> Self {
    Self { id }
  }
}

impl From<OutboundEmailRequestId> for OutboundEmailRequestIdRef {
  fn from(id: OutboundEmailRequestId) -> Self {
    Self::new(id)
  }
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct OutboundEmail<Opaque> {
  pub id: OutboundEmailId,
  pub submitted_at: Instant,
  pub created_by: UserIdRef,
  pub deadline: Instant,
  pub sender: EmailAddress,
  pub recipient: EmailAddress,
  pub payload: EmailContentPayload<Opaque>,
  pub read_at: Option<Instant>,
}

declare_new_enum!(
  pub enum EmailDeliveryStatus {
    #[str("Pending")]
    Pending,
    #[str("Cancelled")]
    Cancelled,
    #[str("Sent")]
    Sent,
    #[str("Acknowledged")]
    Acknowledged,
  }
  pub type ParseError = EmailDeliveryStatusParseError;
  const SQL_NAME = "email_delivery_status";
);

declare_new_enum!(
  pub enum EmailPayloadKind {
    #[str("Marktwin")]
    Marktwin,
    #[str("ResetPassword")]
    ResetPassword,
    #[str("VerifyEmail")]
    VerifyEmail,
  }
  pub type ParseError = EmailPayloadKindParseError;
  const SQL_NAME = "email_payload_kind";
);

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct EmailContentPayload<Opaque> {
  pub kind: EmailPayloadKind,
  pub version: u8,
  pub data: Opaque,
  pub text: EmailContentSummary,
  pub html: EmailContentSummary,
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct EmailContentSummary {
  pub size: u16,
  pub sha2_256: DigestSha2_256,
  pub sha3_256: DigestSha3_256,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum EmailContentSummaryError {
  #[error("the provided data is too large: actual size = {0}, max size = {1}")]
  Size(usize, usize),
}

impl EmailContentSummary {
  /// Email content summary for empty data.
  ///
  /// ```
  /// use eternaltwin_core::mailer::store::EmailContentSummary;
  ///
  /// assert_eq!(EmailContentSummary::EMPTY, EmailContentSummary::new(&[]).expect("empty content is valid"));
  /// ```
  pub const EMPTY: Self = Self {
    size: 0,
    #[rustfmt::skip]
    sha2_256: DigestSha2_256::from_array([
      0xe3, 0xb0, 0xc4, 0x42, 0x98, 0xfc, 0x1c, 0x14,
      0x9a, 0xfb, 0xf4, 0xc8, 0x99, 0x6f, 0xb9, 0x24,
      0x27, 0xae, 0x41, 0xe4, 0x64, 0x9b, 0x93, 0x4c,
      0xa4, 0x95, 0x99, 0x1b, 0x78, 0x52, 0xb8, 0x55,
    ]),
    #[rustfmt::skip]
    sha3_256: DigestSha3_256::from_array([
      0xa7, 0xff, 0xc6, 0xf8, 0xbf, 0x1e, 0xd7, 0x66,
      0x51, 0xc1, 0x47, 0x56, 0xa0, 0x61, 0xd6, 0x62,
      0xf5, 0x80, 0xff, 0x4d, 0xe4, 0x3b, 0x49, 0xfa,
      0x82, 0xd8, 0x0a, 0x4b, 0x80, 0xf8, 0x43, 0x4a,
    ]),
  };

  pub fn new(data: &[u8]) -> Result<Self, EmailContentSummaryError> {
    let size =
      u16::try_from(data.len()).map_err(|_| EmailContentSummaryError::Size(data.len(), usize::from(u16::MAX)))?;
    let sha2_256 = DigestSha2_256::digest(data);
    let sha3_256 = DigestSha3_256::digest(data);
    Ok(Self {
      size,
      sha2_256,
      sha3_256,
    })
  }
}

declare_new_enum!(
  pub enum EmailRequestStatus {
    #[str("Pending")]
    Pending,
    #[str("Cancelled")]
    Cancelled,
    #[str("Timeout")]
    Timeout,
    #[str("Ok")]
    Ok,
    #[str("Error")]
    Error,
  }
  pub type ParseError = EmailRequestStatusParseError;
  const SQL_NAME = "email_request_status";
);

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct OutboundEmailRequest {
  pub id: OutboundEmailRequestId,
  pub email: OutboundEmailIdRef,
  pub period: FinitePeriod,
  pub status: EmailRequestStatus,
}

#[async_trait]
pub trait MailerStore<Opaque>:
  Send
  + Sync
  + Handler<CreateOutboundEmail<Opaque>>
  + Handler<AcquireOutboundEmailRequest>
  + Handler<ReleaseOutboundEmailRequest>
  + Handler<GetOutboundEmails<Opaque>>
  + Handler<GetOutboundEmailRequests>
where
  Opaque: Send + Sync + 'static,
{
  async fn create_outbound_email(
    &self,
    cmd: CreateOutboundEmail<Opaque>,
  ) -> Result<OutboundEmail<Opaque>, CreateOutboundEmailError> {
    Handler::<CreateOutboundEmail<Opaque>>::handle(self, cmd).await
  }

  async fn acquire_outbound_email_request(
    &self,
    cmd: AcquireOutboundEmailRequest,
  ) -> Result<OutboundEmailRequest, AcquireOutboundEmailRequestError> {
    Handler::<AcquireOutboundEmailRequest>::handle(self, cmd).await
  }

  async fn release_outbound_email_request(
    &self,
    cmd: ReleaseOutboundEmailRequest,
  ) -> Result<OutboundEmailRequest, ReleaseOutboundEmailRequestError> {
    Handler::<ReleaseOutboundEmailRequest>::handle(self, cmd).await
  }

  async fn get_outbound_emails(
    &self,
    query: GetOutboundEmails<Opaque>,
  ) -> Result<Listing<OutboundEmail<Opaque>>, GetOutboundEmailsError> {
    Handler::<GetOutboundEmails<Opaque>>::handle(self, query).await
  }

  async fn get_outbound_email_requests(
    &self,
    query: GetOutboundEmailRequests,
  ) -> Result<Listing<OutboundEmailRequest>, GetOutboundEmailRequestsError> {
    Handler::<GetOutboundEmailRequests>::handle(self, query).await
  }
}

impl<T, Opaque> MailerStore<Opaque> for T
where
  T: Send
    + Sync
    + Handler<CreateOutboundEmail<Opaque>>
    + Handler<AcquireOutboundEmailRequest>
    + Handler<ReleaseOutboundEmailRequest>
    + Handler<GetOutboundEmails<Opaque>>
    + Handler<GetOutboundEmailRequests>,
  Opaque: Send + Sync + 'static,
{
}

/// Like [`Deref`], but the target has the bound [`MailerStore`]
pub trait MailerStoreRef<Opaque>: Send + Sync
where
  Opaque: Send + Sync + 'static,
{
  type MailerStore: MailerStore<Opaque> + ?Sized;

  fn mailer_store(&self) -> &Self::MailerStore;
}

impl<TyRef, Opaque> MailerStoreRef<Opaque> for TyRef
where
  TyRef: Deref + Send + Sync,
  TyRef::Target: MailerStore<Opaque>,
  Opaque: Send + Sync + 'static,
{
  type MailerStore = TyRef::Target;

  fn mailer_store(&self) -> &Self::MailerStore {
    self.deref()
  }
}
