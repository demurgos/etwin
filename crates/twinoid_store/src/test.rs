use eternaltwin_core::api::SyncRef;
use eternaltwin_core::clock::{Clock, VirtualClock};
use eternaltwin_core::core::{Duration, Instant, PrUrl};
use eternaltwin_core::twinoid::api::Restrict;
use eternaltwin_core::twinoid::api::{Like, Missing, Site, SiteIcon, SiteInfo, TwinoidSiteStatus, UrlRef};
use eternaltwin_core::twinoid::client::SafeUser;
use eternaltwin_core::twinoid::client::{FullSite, FullUser};
use eternaltwin_core::twinoid::store::{
  GetShortUser, GetShortUserError, GetUser, GetUserError, TouchFullUserList, TouchSafeUserList, TouchShortUser,
  TouchSite, TwinoidStoreRef,
};
use eternaltwin_core::twinoid::{
  ArchivedTwinoidUser, ShortTwinoidUser, TwinoidApiAuth, TwinoidLocale, TwinoidSiteHost, TwinoidSiteId,
  TwinoidSiteInfoId, TwinoidStore, TwinoidUserIdRef,
};
use eternaltwin_core::types::DisplayErrorChain;
use once_cell::sync::Lazy;
use std::fs;
use std::path::PathBuf;
use std::str::FromStr;
use url::Url;

pub(crate) static REPO_DIR: Lazy<PathBuf> = Lazy::new(|| PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("../.."));

#[derive(Clone, Copy)]
pub(crate) struct TestApi<TyClock, TyTwinoidStore>
where
  TyClock: SyncRef<VirtualClock>,
  TyTwinoidStore: TwinoidStoreRef,
{
  pub(crate) clock: TyClock,
  pub(crate) twinoid_store: TyTwinoidStore,
}

pub(crate) async fn test_empty<TyClock, TyTwinoidStore>(api: TestApi<TyClock, TyTwinoidStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyTwinoidStore: TwinoidStoreRef,
{
  let options = GetShortUser {
    user: TwinoidUserIdRef {
      id: "123".parse().unwrap(),
    },
    time: api.clock.now(),
  };
  let actual = api.twinoid_store.twinoid_store().get_short_user(options).await;
  let expected = Err(GetShortUserError::NotFound);
  assert_eq!(actual, expected);
}

pub(crate) async fn test_touch_user<TyClock, TyTwinoidStore>(api: TestApi<TyClock, TyTwinoidStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyTwinoidStore: TwinoidStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    api
      .twinoid_store
      .twinoid_store()
      .touch_short_user(TouchShortUser {
        user: ShortTwinoidUser {
          id: "123".parse().unwrap(),
          display_name: "alice".parse().unwrap(),
        },
      })
      .await
      .unwrap();
    // let expected = ArchivedTwinoidUser {
    //   id: "123".parse().unwrap(),
    //   display_name: "alice".parse().unwrap(),
    //   archived_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
    // };
    // assert_eq!(actual, expected);
  }
  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api
      .twinoid_store
      .twinoid_store()
      .get_short_user(GetShortUser {
        user: TwinoidUserIdRef {
          id: "123".parse().unwrap(),
        },
        time: api.clock.now(),
      })
      .await
      .unwrap();
    let expected = ShortTwinoidUser {
      id: "123".parse().unwrap(),
      display_name: "alice".parse().unwrap(),
    };
    assert_eq!(actual, expected);
  }
  {
    let actual = api
      .twinoid_store
      .twinoid_store()
      .get_user(GetUser {
        user: TwinoidUserIdRef {
          id: "123".parse().unwrap(),
        },
        time: api.clock.now(),
      })
      .await
      .unwrap();
    let expected = ArchivedTwinoidUser {
      id: "123".parse().unwrap(),
      display_name: "alice".parse().unwrap(),
      archived_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
      links: Vec::new(),
    };
    assert_eq!(actual, expected);
  }
}

pub(crate) async fn test_get_missing_user<TyClock, TyTwinoidStore>(api: TestApi<TyClock, TyTwinoidStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyTwinoidStore: TwinoidStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    let actual = api
      .twinoid_store
      .twinoid_store()
      .get_short_user(GetShortUser {
        user: TwinoidUserIdRef {
          id: "123".parse().unwrap(),
        },
        time: api.clock.now(),
      })
      .await;
    let expected = Err(GetShortUserError::NotFound);
    assert_eq!(actual, expected);
  }
  {
    let actual = api
      .twinoid_store
      .twinoid_store()
      .get_user(GetUser {
        user: TwinoidUserIdRef {
          id: "123".parse().unwrap(),
        },
        time: api.clock.now(),
      })
      .await;
    let expected = Err(GetUserError::NotFound);
    assert_eq!(actual, expected);
  }
}

pub(crate) async fn test_touch_drpg_fr<TyClock, TyTwinoidStore>(api: TestApi<TyClock, TyTwinoidStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyTwinoidStore: TwinoidStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let site: FullSite = Site {
    id: TwinoidSiteId::new(2).unwrap(),
    name: Restrict::Allow("DinoRPG".to_string()),
    host: Restrict::Allow(TwinoidSiteHost::from_str("www.dinorpg.com").unwrap()),
    icon: Restrict::Allow(UrlRef {
      url: PrUrl::parse("//data.twinoid.com/proxy/www.dinorpg.com/favicon.ico").unwrap(),
    }),
    lang: Restrict::Allow(Some(TwinoidLocale::Fr)),
    like: Restrict::Allow(Like {
      url: Url::parse("http://www.dinorpg.com/").unwrap(),
      likes: 3633,
      title: Some("DinoRPG".to_string()),
    }),
    infos: Restrict::Allow(vec![
      SiteInfo {
        id: TwinoidSiteInfoId::new(2).unwrap(),
        site: Missing,
        lang: TwinoidLocale::Fr,
        cover: UrlRef {
          url: PrUrl::parse("http://data.twinoid.com/file/7_efd4.png").unwrap(),
        },
        tag_line: Some("élevez vos dinoz dans un jeu d'aventure extraordinaire !".to_string()),
        description: "<p>Adoptez le dinoz de votre choix parmi <strong>plus de 18 races uniques</strong> et menez le à l’aventure en remplissant des quêtes et missions variées.</p>\r\n<p>\r\n<strong>Rejoignez un Clan de joueurs</strong> ou formez en un avec vos amis. Vous pourrez alors discuter, échanger des techniques et des astuces, vous entraider et participer aux modes multi-joueurs !</p>\r\n".to_string(),
        tid: "drpg".to_string(),
        icons: vec![
          SiteIcon {
            tag: ":drpg:".to_string(),
            alt_tag: None,
            url: PrUrl::parse("http://www.dinorpg.com/favicon.ico").unwrap(),
            typ: "icon".to_string(),
          },
          SiteIcon {
            tag: ":drpg_gold:".to_string(),
            alt_tag: None,
            url: PrUrl::parse("http://www.dinorpg.com/img/forum/smiley/s_gold.gif").unwrap(),
            typ: "icon".to_string(),
          },
          SiteIcon {
            tag: ":drpg_map_castle:".to_string(),
            alt_tag: None,
            url: PrUrl::parse("http://www.dinorpg.com/img/forum/smiley/map_castle.gif").unwrap(),
            typ: "icon".to_string(),
          },
          SiteIcon {
            tag: ":drpg_map_cavern:".to_string(),
            alt_tag: None,
            url: PrUrl::parse("http://www.dinorpg.com/img/forum/smiley/map_cavern.gif").unwrap(),
            typ: "icon".to_string(),
          },
          SiteIcon {
            tag: ":drpg_map_church:".to_string(),
            alt_tag: None,
            url: PrUrl::parse("http://www.dinorpg.com/img/forum/smiley/map_church.gif").unwrap(),
            typ: "icon".to_string(),
          },
          SiteIcon {
            tag: ":drpg_map_default:".to_string(),
            alt_tag: None,
            url: PrUrl::parse("http://www.dinorpg.com/img/forum/smiley/map_default.gif").unwrap(),
            typ: "icon".to_string(),
          },
          SiteIcon {
            tag: ":drpg_map_door:".to_string(),
            alt_tag: None,
            url: PrUrl::parse("http://www.dinorpg.com/img/forum/smiley/map_door.gif").unwrap(),
            typ: "icon".to_string(),
          },
          SiteIcon {
            tag: ":drpg_map_house:".to_string(),
            alt_tag: None,
            url: PrUrl::parse("http://www.dinorpg.com/img/forum/smiley/map_house.gif").unwrap(),
            typ: "icon".to_string(),
          },
          SiteIcon {
            tag: ":drpg_map_mountain:".to_string(),
            alt_tag: None,
            url: PrUrl::parse("http://www.dinorpg.com/img/forum/smiley/map_mountain.gif").unwrap(),
            typ: "icon".to_string(),
          },
          SiteIcon {
            tag: ":drpg_map_fj:".to_string(),
            alt_tag: None,
            url: PrUrl::parse("http://www.dinorpg.com/img/forum/smiley/map_fj.gif").unwrap(),
            typ: "icon".to_string(),
          },
          SiteIcon {
            tag: ":drpg_feu:".to_string(),
            alt_tag: None,
            url: PrUrl::parse("http://www.dinorpg.com/img/forum/smiley/elem_0.gif").unwrap(),
            typ: "icon".to_string(),
          },
          SiteIcon {
            tag: ":drpg_bois:".to_string(),
            alt_tag: None,
            url: PrUrl::parse("http://www.dinorpg.com/img/forum/smiley/elem_1.gif").unwrap(),
            typ: "icon".to_string(),
          },
          SiteIcon {
            tag: ":drpg_eau:".to_string(),
            alt_tag: None,
            url: PrUrl::parse("http://www.dinorpg.com/img/forum/smiley/elem_2.gif").unwrap(),
            typ: "icon".to_string(),
          },
          SiteIcon {
            tag: ":drpg_foudre:".to_string(),
            alt_tag: None,
            url: PrUrl::parse("http://www.dinorpg.com/img/forum/smiley/elem_3.gif").unwrap(),
            typ: "icon".to_string(),
          },
          SiteIcon {
            tag: ":drpg_air:".to_string(),
            alt_tag: None,
            url: PrUrl::parse("http://www.dinorpg.com/img/forum/smiley/elem_4.gif").unwrap(),
            typ: "icon".to_string(),
          },
          SiteIcon {
            tag: ":drpg_neutre:".to_string(),
            alt_tag: None,
            url: PrUrl::parse("http://www.dinorpg.com/img/forum/smiley/elem_5.gif").unwrap(),
            typ: "icon".to_string(),
          },
          SiteIcon {
            tag: ":drpg_vie:".to_string(),
            alt_tag: None,
            url: PrUrl::parse("http://www.dinorpg.com/img/forum/smiley/small_life.gif").unwrap(),
            typ: "icon".to_string(),
          },
          SiteIcon {
            tag: ":drpg_exp:".to_string(),
            alt_tag: None,
            url: PrUrl::parse("http://www.dinorpg.com/img/forum/smiley/small_xp.gif").unwrap(),
            typ: "icon".to_string(),
          },
          SiteIcon {
            tag: ":drpg_bon:".to_string(),
            alt_tag: None,
            url: PrUrl::parse("http://www.dinorpg.com/img/forum/smiley/small_tix.gif").unwrap(),
            typ: "icon".to_string(),
          },
          SiteIcon {
            tag: ":drpg_mail:".to_string(),
            alt_tag: None,
            url: PrUrl::parse("http://www.dinorpg.com/img/forum/smiley/small_mail.gif").unwrap(),
            typ: "icon".to_string(),
          },
          SiteIcon {
            tag: ":drpg_mana:".to_string(),
            alt_tag: None,
            url: PrUrl::parse("http://www.dinorpg.com/img/forum/smiley/small_mana.gif").unwrap(),
            typ: "icon".to_string(),
          },
          SiteIcon {
            tag: ":drpg_love:".to_string(),
            alt_tag: None,
            url: PrUrl::parse("http://www.dinorpg.com/img/forum/smiley/love.gif").unwrap(),
            typ: "icon".to_string(),
          },
          SiteIcon {
            tag: ":drpg_twinlove:".to_string(),
            alt_tag: None,
            url: PrUrl::parse("http://www.dinorpg.com/img/forum/smiley/twinlove.gif").unwrap(),
            typ: "icon".to_string(),
          },
        ],
      },
    ]),
    me: Missing,
    status: Restrict::Allow(TwinoidSiteStatus::Open),
  };
  let actual = api
    .twinoid_store
    .twinoid_store()
    .touch_site(TouchSite {
      auth: TwinoidApiAuth::Guest,
      site,
    })
    .await;
  let expected = Ok(());
  assert_eq!(actual, expected);
}

pub(crate) async fn test_touch_all_safe_users<TyClock, TyTwinoidStore>(api: TestApi<TyClock, TyTwinoidStore>)
where
  TyClock: Clone + SyncRef<VirtualClock>,
  TyTwinoidStore: Clone + TwinoidStoreRef,
{
  let mut users: Vec<SafeUser> = Vec::new();
  let dir = REPO_DIR.join("test-resources/scraping/twinoid/api/twinoid/safe-user");
  let entries = fs::read_dir(dir).expect("failed to read safe users dir");
  for entry in entries {
    let entry = entry.expect("failed to read safe-user entry");
    let meta = entry.metadata().expect("failed to read safe-user entry meta");
    if !meta.is_dir() {
      continue;
    }
    let input_path = entry.path().join("input.json");
    let input_json = fs::read_to_string(input_path.as_path()).expect("failed to read input safe user");
    let user: SafeUser = match serde_json::from_str(&input_json) {
      Ok(u) => u,
      Err(e) => panic!(
        "failed to parse safe user {}: {}",
        input_path.display(),
        DisplayErrorChain(&e)
      ),
    };
    users.push(user);
  }

  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let actual = api
    .twinoid_store
    .twinoid_store()
    .touch_safe_user_list(TouchSafeUserList {
      auth: TwinoidApiAuth::Guest,
      users,
    })
    .await;
  let expected = Ok(());
  assert_eq!(actual, expected);
}

pub(crate) async fn test_touch_all_full_users<TyClock, TyTwinoidStore>(api: TestApi<TyClock, TyTwinoidStore>)
where
  TyClock: Clone + SyncRef<VirtualClock>,
  TyTwinoidStore: Clone + TwinoidStoreRef,
{
  let mut users: Vec<FullUser> = Vec::new();
  let dir = REPO_DIR.join("test-resources/scraping/twinoid/api/twinoid/full-user");
  let entries = fs::read_dir(dir).expect("failed to read safe users dir");
  for entry in entries {
    let entry = entry.expect("failed to read safe-user entry");
    let meta = entry.metadata().expect("failed to read safe-user entry meta");
    if !meta.is_dir() {
      continue;
    }
    let input_path = entry.path().join("input.json");
    let input_json = fs::read_to_string(input_path.as_path()).expect("failed to read input safe user");
    let user: FullUser = match serde_json::from_str(&input_json) {
      Ok(u) => u,
      Err(e) => panic!(
        "failed to parse safe user {}: {}",
        input_path.display(),
        DisplayErrorChain(&e)
      ),
    };
    users.push(user);
  }

  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let actual = api
    .twinoid_store
    .twinoid_store()
    .touch_full_user_list(TouchFullUserList {
      auth: TwinoidApiAuth::Guest,
      users,
    })
    .await;
  let expected = Ok(());
  assert_eq!(actual, expected);
}
