use crate::core::{Instant, Request};
use crate::oauth::{RfcOauthAccessTokenKey, RfcOauthRefreshTokenKey};
use crate::twinoid::TwinoidUserIdRef;
use crate::types::WeakError;
use thiserror::Error;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TouchTwinoidOauth {
  pub access_token: RfcOauthAccessTokenKey,
  pub refresh_token: Option<RfcOauthRefreshTokenKey>,
  pub expiration_time: Instant,
  pub twinoid_user: TwinoidUserIdRef,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Error)]
pub enum TouchTwinoidOauthError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl Request for TouchTwinoidOauth {
  type Response = Result<(), TouchTwinoidOauthError>;
}
