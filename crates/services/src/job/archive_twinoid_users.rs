use crate::job::error_sleep;
use crate::job::with_twinoid_access_token::{TwinoidTokenCx, TwinoidTokenResult, WithTwinoidAccessToken};
use async_trait::async_trait;
use eternaltwin_core::job::{Sleep, Task, TaskCx, TaskPoll};
use eternaltwin_core::twinoid::client::get_users::{GetUsers, GetUsersError};
use eternaltwin_core::twinoid::client::{FullUser, FullUserProjector, SafeUser, SafeUserProjector, TwinoidClientRef};
use eternaltwin_core::twinoid::store::{TouchFullUserList, TouchSafeUserList, TwinoidStoreRef};
use eternaltwin_core::twinoid::{TwinoidApiAuth, TwinoidClient, TwinoidStore, TwinoidUserId};
use eternaltwin_core::types::WeakError;
use serde::{Deserialize, Serialize};
use thiserror::Error;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct ArchiveTwinoidUsers(WithTwinoidAccessToken<ArchiveTwinoidUsersWorker>);

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Error, Serialize, Deserialize)]
pub enum ArchiveTwinoidUsersError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[async_trait]
impl<'cx, Cx> Task<&'cx mut Cx> for ArchiveTwinoidUsers
where
  Cx: TaskCx,
  Cx::Extra: TwinoidClientRef + TwinoidStoreRef,
{
  const NAME: &'static str = "ArchiveTwinoidUsers";
  const VERSION: u32 = 1;
  type Output = Result<(), ArchiveTwinoidUsersError>;

  async fn poll(&mut self, cx: &'cx mut Cx) -> TaskPoll<Self::Output> {
    self.0.poll(cx).await
  }
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum ArchiveTwinoidUsersWorker {
  Start {
    users: Vec<TwinoidUserId>,
  },
  Read {
    timeout: Option<Sleep>,
    users: Vec<TwinoidUserId>,
    scope: Scope,
    read_errors: u32,
    last_error: Option<WeakError>,
  },
  Write {
    timeout: Option<Sleep>,
    users: Vec<TwinoidUserId>,
    result: WriteScope,
    write_errors: u32,
    last_error: Option<WeakError>,
  },
  Ready(Result<(), ArchiveTwinoidUsersError>),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum Scope {
  Full1,
  Safe1,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum WriteScope {
  Full1(Vec<FullUser>),
  Safe1(Vec<SafeUser>),
}

#[async_trait]
impl<'cx, Cx> Task<TwinoidTokenCx<'cx, Cx>> for ArchiveTwinoidUsersWorker
where
  Cx: TaskCx,
  Cx::Extra: TwinoidClientRef + TwinoidStoreRef,
{
  const NAME: &'static str = "ArchiveTwinoidUsersWorker";
  const VERSION: u32 = 1;
  type Output = TwinoidTokenResult<Result<(), ArchiveTwinoidUsersError>>;

  async fn poll(&mut self, cx: TwinoidTokenCx<'cx, Cx>) -> TaskPoll<Self::Output> {
    let mut iter = 0;
    loop {
      iter += 1;
      if iter >= 50 {
        return TaskPoll::Pending;
      }
      match self {
        Self::Start { users } => {
          *self = Self::Read {
            timeout: None,
            users: users.clone(),
            scope: Scope::Full1,
            read_errors: 0,
            last_error: None,
          }
        }
        Self::Read {
          timeout,
          users,
          scope,
          read_errors,
          last_error,
        } => {
          if let Some(sleep) = timeout {
            match sleep.poll(cx.inner).await {
              TaskPoll::Pending => return TaskPoll::Pending,
              TaskPoll::Ready(()) => *timeout = None,
            }
          }
          let (head, tail) = users.as_slice().split_at(usize::min(GetUsers::MAX_LEN, users.len()));
          if head.is_empty() {
            *self = Self::Ready(Ok(()));
            continue;
          }
          let users = match scope {
            Scope::Full1 => cx
              .inner
              .extra()
              .twinoid_client()
              .get_users_full1(GetUsers {
                auth: TwinoidApiAuth::Token(cx.token.key.clone()),
                ids: Vec::from(head),
                projector: FullUserProjector::default(),
              })
              .await
              .map(WriteScope::Full1),
            Scope::Safe1 => cx
              .inner
              .extra()
              .twinoid_client()
              .get_users_safe1(GetUsers {
                auth: TwinoidApiAuth::Token(cx.token.key.clone()),
                ids: Vec::from(head),
                projector: SafeUserProjector::default(),
              })
              .await
              .map(WriteScope::Safe1),
          };
          match users {
            Ok(users) => {
              *self = Self::Write {
                timeout: None,
                users: Vec::from(tail),
                result: users,
                write_errors: 0,
                last_error: None,
              }
            }
            Err(GetUsersError::InvalidToken) => return TaskPoll::Ready(TwinoidTokenResult::Invalid),
            Err(GetUsersError::Encoding) if matches!(scope, Scope::Full1) => {
              // Limit to only safe fields to avoid encoding issues
              *scope = Scope::Safe1;
              return TaskPoll::Pending;
            }
            Err(e) => {
              eprintln!("[ArchiveTwinoidUsersWorker::read::{scope:?}] {e:#}");
              *read_errors += 1;
              *timeout = Some(cx.inner.sleep(error_sleep(*read_errors)));
              *last_error = Some(WeakError::wrap(e));
              continue;
            }
          };
        }
        Self::Write {
          timeout,
          users,
          result,
          write_errors,
          last_error,
        } => {
          if let Some(sleep) = timeout {
            match sleep.poll(cx.inner).await {
              TaskPoll::Pending => return TaskPoll::Pending,
              TaskPoll::Ready(()) => *timeout = None,
            }
          }
          let write_result = match result {
            WriteScope::Full1(users) => cx
              .inner
              .extra()
              .twinoid_store()
              .touch_full_user_list(TouchFullUserList {
                auth: TwinoidApiAuth::Token(cx.token.key.clone()),
                users: users.clone(),
              })
              .await
              .map_err(WeakError::wrap),
            WriteScope::Safe1(users) => cx
              .inner
              .extra()
              .twinoid_store()
              .touch_safe_user_list(TouchSafeUserList {
                auth: TwinoidApiAuth::Token(cx.token.key.clone()),
                users: users.clone(),
              })
              .await
              .map_err(WeakError::wrap),
          };
          match write_result {
            Ok(()) => {
              *self = Self::Read {
                timeout: None,
                users: users.clone(),
                scope: Scope::Full1,
                read_errors: 0,
                last_error: None,
              };
              return TaskPoll::Pending;
            }
            Err(e) => {
              eprintln!("[ArchiveTwinoidUsersWorker::write] {e:#}");
              *write_errors += 1;
              *timeout = Some(cx.inner.sleep(error_sleep(*write_errors)));
              *last_error = Some(e);
              continue;
            }
          };
        }
        Self::Ready(result) => {
          return TaskPoll::Ready(TwinoidTokenResult::MaybeValid(result.clone()));
        }
      }
    }
  }
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  pub fn test_write() {
    let input = ArchiveTwinoidUsers(WithTwinoidAccessToken::GetToken {
      timeout: None,
      failures: 0,
      last_error: None,
      task: ArchiveTwinoidUsersWorker::Start {
        users: vec![TwinoidUserId::new(38).unwrap()],
      },
    });
    let actual = serde_json::to_string(&input).unwrap();
    // language=json
    let expected = r#"{"GetToken":{"timeout":null,"failures":0,"last_error":null,"task":{"Start":{"users":["38"]}}}}"#;
    assert_eq!(actual, expected);
  }

  #[test]
  pub fn test_read() {
    // language=json
    let input = r#"{"GetToken":{"timeout":null,"failures":0,"last_error":null,"task":{"Start":{"users":["38"]}}}}"#;
    let actual: ArchiveTwinoidUsers = serde_json::from_str(input).unwrap();
    let expected = ArchiveTwinoidUsers(WithTwinoidAccessToken::GetToken {
      timeout: None,
      failures: 0,
      last_error: None,
      task: ArchiveTwinoidUsersWorker::Start {
        users: vec![TwinoidUserId::new(38).unwrap()],
      },
    });
    assert_eq!(actual, expected);
  }
}
