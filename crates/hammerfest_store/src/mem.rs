use async_trait::async_trait;
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::core::Instant;
use eternaltwin_core::email::RawEmailAddress;
use eternaltwin_core::hammerfest::{
  GetActiveSession, GetActiveSessionError, GetHammerfestUserOptions, HammerfestForumThemePageResponse,
  HammerfestForumThreadPageResponse, HammerfestGodchild, HammerfestGodchildrenResponse, HammerfestInventoryResponse,
  HammerfestItemId, HammerfestProfileResponse, HammerfestSession, HammerfestSessionKey, HammerfestSessionUser,
  HammerfestShop, HammerfestShopResponse, HammerfestStore, HammerfestUserId, HammerfestUserRef,
  RawGetHammerfestUserError, RawGetShortHammerfestUserError, ShortHammerfestUser, StoredHammerfestProfile,
  StoredHammerfestUser, TouchEvniUser, TouchHammerfestEvniUserError, TouchHammerfestGodchildrenError,
  TouchHammerfestInventoryError, TouchHammerfestProfileError, TouchHammerfestSessionError, TouchHammerfestShopError,
  TouchHammerfestThemePageError, TouchHammerfestThreadPageError, TouchHammerfestUserError, TouchSession,
};
use eternaltwin_core::temporal::{LatestTemporal, SnapshotLog};
use std::collections::hash_map::Entry;
use std::collections::{BTreeMap, HashMap};
use std::sync::RwLock;

fn clone_latest_temporal<T: Clone + Eq>(log: &SnapshotLog<T>) -> Option<LatestTemporal<T>> {
  log.latest().map(|l| LatestTemporal { latest: l.cloned() })
}

struct StoreState {
  users: HashMap<HammerfestUserId, MemUser>,
  evni_users: HashMap<HammerfestUserId, SnapshotLog<()>>,
  sessions: HashMap<HammerfestSessionKey, MemSession>,
}

struct MemSession {
  key: HammerfestSessionKey,
  user: SnapshotLog<Option<HammerfestUserId>>,
}

impl MemSession {
  fn new(key: HammerfestSessionKey, time: Instant, user: Option<HammerfestUserId>) -> Self {
    Self {
      key,
      user: {
        let mut log = SnapshotLog::new();
        log.snapshot(time, user);
        log
      },
    }
  }
}

struct MemUser {
  short: ShortHammerfestUser,
  archived_at: Instant,
  email: SnapshotLog<Option<RawEmailAddress>>,
  profile: SnapshotLog<StoredHammerfestProfile>,
  inventory: SnapshotLog<BTreeMap<HammerfestItemId, u32>>,
  tokens: SnapshotLog<u32>,
  session: SnapshotLog<Option<HammerfestSessionKey>>,
  shop: SnapshotLog<HammerfestShop>,
  godchildren: SnapshotLog<Vec<HammerfestGodchild>>,
}

impl MemUser {
  fn new(time: Instant, user: ShortHammerfestUser) -> Self {
    Self {
      short: user,
      archived_at: time,
      email: SnapshotLog::new(),
      profile: SnapshotLog::new(),
      inventory: SnapshotLog::new(),
      tokens: SnapshotLog::new(),
      session: SnapshotLog::new(),
      shop: SnapshotLog::new(),
      godchildren: SnapshotLog::new(),
    }
  }

  fn to_archived(&self) -> StoredHammerfestUser {
    StoredHammerfestUser {
      server: self.short.server,
      id: self.short.id,
      username: self.short.username.clone(),
      archived_at: self.archived_at,
      profile: clone_latest_temporal(&self.profile),
      inventory: clone_latest_temporal(&self.inventory),
    }
  }

  fn matches(&self, r: &HammerfestUserRef) -> bool {
    match r {
      HammerfestUserRef::Id(r) => r.server == self.short.server && r.id == self.short.id,
      HammerfestUserRef::Username(r) => r.server == self.short.server && r.username == self.short.username,
    }
  }
}

impl StoreState {
  fn new() -> Self {
    Self {
      users: HashMap::new(),
      evni_users: HashMap::new(),
      sessions: HashMap::new(),
    }
  }

  fn get_user(&self, id: &HammerfestUserId) -> Option<&MemUser> {
    self.users.get(id)
  }

  pub fn get_active_session(&self, query: GetActiveSession) -> Result<HammerfestSession, GetActiveSessionError> {
    for session in self.sessions.values() {
      let (ctime, atime, user_id) = if let Some(user) = session.user.get(query.time) {
        if let Some(user_id) = user.value {
          (user.period.start(), user.retrieved.latest, *user_id)
        } else {
          continue;
        }
      } else {
        continue;
      };
      let user = self.users.get(&user_id).expect("user exists");
      if user.matches(&query.user) {
        return Ok(HammerfestSession {
          key: session.key.clone(),
          user: user.short.clone(),
          ctime,
          atime,
        });
      }
    }
    Err(GetActiveSessionError::NotFound)
  }

  pub fn touch_session(&mut self, cmd: TouchSession) -> Result<(), TouchHammerfestSessionError> {
    let user_id = match cmd.user {
      Some(user) => {
        let user_id = user.id;
        self.touch_user(cmd.now, user);
        Some(user_id)
      }
      None => None,
    };

    let mut invalidate: Option<HammerfestUserId> = None;

    match self.sessions.entry(cmd.key.clone()) {
      Entry::Vacant(e) => {
        e.insert(MemSession::new(cmd.key.clone(), cmd.now, user_id));
      }
      Entry::Occupied(mut e) => {
        let session: &mut MemSession = e.get_mut();
        let old_user = session.user.get(cmd.now).and_then(|s| *s.value);
        if old_user != user_id {
          invalidate = old_user;
        }
        session.user.snapshot(cmd.now, user_id);
      }
    }

    if let Some(invalid) = invalidate {
      let invalid = self.users.get_mut(&invalid).expect("user exists");
      invalid.session.snapshot(cmd.now, None);
    }

    if let Some(user) = user_id {
      let user = self.users.get_mut(&user).expect("user exists");
      user.session.snapshot(cmd.now, Some(cmd.key.clone()));
    }

    Ok(())
  }

  pub fn touch_evni_user(&mut self, cmd: TouchEvniUser) {
    let snapshots = self.evni_users.entry(cmd.user.id).or_default();
    snapshots.snapshot(cmd.now, ());
  }

  fn touch_session_user(&mut self, time: Instant, sess: HammerfestSessionUser) -> &mut MemUser {
    let user = self.touch_user(time, sess.user);
    user.tokens.snapshot(time, sess.tokens);
    user
  }

  fn touch_user(&mut self, time: Instant, user: ShortHammerfestUser) -> &mut MemUser {
    self.users.entry(user.id).or_insert_with(|| MemUser::new(time, user))
  }
}

pub struct MemHammerfestStore<TyClock>
where
  TyClock: ClockRef,
{
  clock: TyClock,
  state: RwLock<StoreState>,
}

impl<TyClock> MemHammerfestStore<TyClock>
where
  TyClock: ClockRef,
{
  pub fn new(clock: TyClock) -> Self {
    Self {
      clock,
      state: RwLock::new(StoreState::new()),
    }
  }
}

#[async_trait]
impl<TyClock> HammerfestStore for MemHammerfestStore<TyClock>
where
  TyClock: ClockRef,
{
  async fn get_short_user(
    &self,
    options: &GetHammerfestUserOptions,
  ) -> Result<ShortHammerfestUser, RawGetShortHammerfestUserError> {
    let state = self.state.read().unwrap();
    match state.get_user(&options.id) {
      Some(u) => Ok(u.short.clone()),
      None => Err(RawGetShortHammerfestUserError::NotFound),
    }
  }

  async fn get_user(
    &self,
    options: &GetHammerfestUserOptions,
  ) -> Result<Option<StoredHammerfestUser>, RawGetHammerfestUserError> {
    let state = self.state.read().unwrap();
    Ok(state.get_user(&options.id).map(|u| u.to_archived()))
  }

  async fn get_active_session(&self, query: GetActiveSession) -> Result<HammerfestSession, GetActiveSessionError> {
    let state = self.state.read().unwrap();
    state.get_active_session(query)
  }

  async fn touch_session(&self, cmd: TouchSession) -> Result<(), TouchHammerfestSessionError> {
    let mut state = self.state.write().unwrap();
    state.touch_session(cmd)
  }

  async fn touch_short_user(
    &self,
    short: &ShortHammerfestUser,
  ) -> Result<StoredHammerfestUser, TouchHammerfestUserError> {
    let mut state = self.state.write().unwrap();
    let now = self.clock.clock().now();
    let user = state.touch_user(now, short.clone());
    Ok(user.to_archived())
  }

  async fn touch_evni_user(&self, cmd: TouchEvniUser) -> Result<(), TouchHammerfestEvniUserError> {
    let mut state = self.state.write().unwrap();
    #[allow(clippy::unit_arg)] // Assert that the return is `()`
    Ok(state.touch_evni_user(cmd))
  }

  async fn touch_shop(&self, response: &HammerfestShopResponse) -> Result<(), TouchHammerfestShopError> {
    let mut state = self.state.write().unwrap();
    let now = self.clock.clock().now();
    let user = state.touch_session_user(now, response.session.clone());
    user.shop.snapshot(now, response.shop.clone());
    Ok(())
  }

  async fn touch_profile(&self, response: &HammerfestProfileResponse) -> Result<(), TouchHammerfestProfileError> {
    let mut state = self.state.write().unwrap();
    let now = self.clock.clock().now();

    let (user, profile) = match (&response.session, &response.profile) {
      (Some(sess), prof) => (state.touch_session_user(now, sess.clone()), prof.as_ref()),
      (None, Some(prof)) => (state.touch_user(now, prof.user.clone()), Some(prof)),
      (None, None) => return Ok(()),
    };

    if let Some(profile) = profile {
      user.profile.snapshot(
        now,
        StoredHammerfestProfile {
          best_score: profile.best_score,
          best_level: profile.best_level,
          game_completed: profile.has_carrot,
          items: profile.items.clone(),
          quests: profile.quests.clone(),
        },
      );

      if let Some(email) = &profile.email {
        user.email.snapshot(now, email.clone());
      }
    }

    Ok(())
  }

  async fn touch_inventory(&self, response: &HammerfestInventoryResponse) -> Result<(), TouchHammerfestInventoryError> {
    let mut state = self.state.write().unwrap();
    let now = self.clock.clock().now();
    let user = state.touch_session_user(now, response.session.clone());
    user.inventory.snapshot(now, response.inventory.clone());
    Ok(())
  }

  async fn touch_godchildren(
    &self,
    response: &HammerfestGodchildrenResponse,
  ) -> Result<(), TouchHammerfestGodchildrenError> {
    let mut state = self.state.write().unwrap();
    let now = self.clock.clock().now();
    let user = state.touch_session_user(now, response.session.clone());
    user.godchildren.snapshot(now, response.godchildren.clone());
    Ok(())
  }

  async fn touch_theme_page(
    &self,
    _response: &HammerfestForumThemePageResponse,
  ) -> Result<(), TouchHammerfestThemePageError> {
    eprintln!("Stub: Missing `MemHammerfestSore::touch_theme_page` implementation");
    Ok(())
  }

  async fn touch_thread_page(
    &self,
    _response: &HammerfestForumThreadPageResponse,
  ) -> Result<(), TouchHammerfestThreadPageError> {
    eprintln!("Stub: Missing `MemHammerfestSore::touch_thread_page` implementation");
    Ok(())
  }
}

#[cfg(test)]
mod test {
  use crate::mem::MemHammerfestStore;
  use crate::test::TestApi;
  use eternaltwin_core::clock::VirtualClock;
  use eternaltwin_core::core::Instant;
  use eternaltwin_core::hammerfest::HammerfestStore;
  use std::sync::Arc;

  fn make_test_api() -> TestApi<Arc<VirtualClock>, Arc<dyn HammerfestStore>> {
    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let hammerfest_store: Arc<dyn HammerfestStore> = Arc::new(MemHammerfestStore::new(Arc::clone(&clock)));

    TestApi {
      clock,
      hammerfest_store,
    }
  }

  test_hammerfest_store!(|| make_test_api());
}
