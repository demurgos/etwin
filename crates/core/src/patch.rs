#[derive(Default, Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum SimplePatch<T> {
  /// Do not change the current value
  #[default]
  Skip,
  /// Set the value to the provided one
  Set(T),
}

impl<T> SimplePatch<T> {
  #[must_use]
  pub fn into_option(self) -> Option<T> {
    match self {
      Self::Skip => None,
      Self::Set(v) => Some(v),
    }
  }

  #[must_use]
  pub fn as_ref(&self) -> SimplePatch<&T> {
    match self {
      Self::Skip => SimplePatch::Skip,
      Self::Set(ref v) => SimplePatch::Set(v),
    }
  }

  #[must_use]
  pub fn map<F, R>(self, f: F) -> SimplePatch<R>
  where
    F: FnOnce(T) -> R,
  {
    match self {
      Self::Skip => SimplePatch::Skip,
      Self::Set(v) => SimplePatch::Set(f(v)),
    }
  }

  #[must_use]
  pub fn from_option(opt: Option<T>) -> Self {
    match opt {
      None => Self::Skip,
      Some(v) => Self::Set(v),
    }
  }

  #[must_use]
  pub fn is_skip(&self) -> bool {
    matches!(self, Self::Skip)
  }

  #[must_use]
  pub fn is_set(&self) -> bool {
    matches!(self, Self::Set(_))
  }

  /// Update the target value based on this patch.
  /// - If `Self::Skip`, do not change anything; return `None`
  /// - If `Self::Set`, replace the value; return `Some(old_value)`
  pub fn apply(self, target: &mut T) -> Option<T> {
    match self {
      Self::Skip => None,
      Self::Set(value) => Some(core::mem::replace(target, value)),
    }
  }
}

#[cfg(feature = "serde")]
impl<T> serde::Serialize for SimplePatch<T>
where
  T: serde::Serialize,
{
  fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
  where
    S: serde::Serializer,
  {
    self.as_ref().into_option().serialize(serializer)
  }
}

#[cfg(feature = "serde")]
impl<'de, T> serde::Deserialize<'de> for SimplePatch<T>
where
  T: serde::Deserialize<'de>,
{
  fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
  where
    D: serde::Deserializer<'de>,
  {
    T::deserialize(deserializer).map(Self::Set)
  }
}

#[cfg(all(test, feature = "serde"))]
mod test {
  use crate::patch::SimplePatch;

  #[derive(Debug, Clone, PartialEq, Eq, ::serde::Serialize, ::serde::Deserialize)]
  struct UserPatch {
    #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
    pub display_name: SimplePatch<String>,
    #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
    pub username: SimplePatch<Option<String>>,
  }

  #[test]
  fn test_read_skip() {
    let input = r#"{}"#;
    let actual: UserPatch = serde_json::from_str(input).unwrap();
    let expected = UserPatch {
      display_name: SimplePatch::Skip,
      username: SimplePatch::Skip,
    };
    assert_eq!(actual, expected);
  }

  #[test]
  fn test_read_set_none() {
    let input = r#"{"display_name":"foo","username":null}"#;
    let actual: UserPatch = serde_json::from_str(input).unwrap();
    let expected = UserPatch {
      display_name: SimplePatch::Set("foo".to_string()),
      username: SimplePatch::Set(None),
    };
    assert_eq!(actual, expected);
  }

  #[test]
  fn test_read_set_some() {
    let input = r#"{"display_name":"foo","username":"bar"}"#;
    let actual: UserPatch = serde_json::from_str(input).unwrap();
    let expected = UserPatch {
      display_name: SimplePatch::Set("foo".to_string()),
      username: SimplePatch::Set(Some("bar".to_string())),
    };
    assert_eq!(actual, expected);
  }

  #[test]
  fn test_write_skip() {
    let input = UserPatch {
      display_name: SimplePatch::Skip,
      username: SimplePatch::Skip,
    };
    let actual = serde_json::to_string(&input).unwrap();
    let expected = r#"{}"#;
    assert_eq!(actual, expected);
  }

  #[test]
  fn test_write_set_none() {
    let input = UserPatch {
      display_name: SimplePatch::Set("foo".to_string()),
      username: SimplePatch::Set(None),
    };
    let actual = serde_json::to_string(&input).unwrap();
    let expected = r#"{"display_name":"foo","username":null}"#;
    assert_eq!(actual, expected);
  }

  #[test]
  fn test_write_set_some() {
    let input = UserPatch {
      display_name: SimplePatch::Set("foo".to_string()),
      username: SimplePatch::Set(Some("bar".to_string())),
    };
    let actual = serde_json::to_string(&input).unwrap();
    let expected = r#"{"display_name":"foo","username":"bar"}"#;
    assert_eq!(actual, expected);
  }
}
