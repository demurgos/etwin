import { NgModule } from "@angular/core";

import { DinoparcService } from "./dinoparc.service.mjs";

@NgModule({
  providers: [
    DinoparcService,
  ],
  imports: [
  ],
})
export class DinoparcModule {
}
