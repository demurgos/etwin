# 2023-01: This Month In Eternaltwin #16

[Eternaltwin](https://eternaltwin.org) is a project with the goal to preserve
Motion Twin's games and player communities.

# Eternaltwin

_Evian_ fixed Skywar in [the Eternaltwin application](https://eternaltwin.org/docs/desktop),
version 0.6.6.

# eMush

Welcome to this section dedicated to [🍄 eMush](https://emush.eternaltwin.org/),
the project to save Mush.

## What's new?

Last year, we mentioned that administrators could create customized ships.

![eMush](./emush0.png)

We're happy to announce that this feature is almost done! 🎉
It's a big step for the eMush open alpha.

Thanks to this admin panel, we'll be able to add new items, characters, weapons...
without reset the database every time!

This lets us focus on the next steps, showcasing your achievements. Since a picture
is worth a thousand words, we'll let you see our progress:

![eMush](./emush1.png)

![eMush](./emush2.png)

![eMush](./emush3.png)

Pretty good, eh? 😄

## Coming next

As a reminder, here are the goals we've set for the next eMush version:

- 🇬🇧 English translations ✅
- 🗃️ spaceship archival, to keep track of your achievements 🚧
- 💾 database migrations, to deploy new version without resetting the existing
  spaceships 🚧

We're seeing good progress on all these tasks. All that's left is a couple
smaller technical issues. We've never been so close to the open alpha.

## Great! When?

As always, we'll keep updating your through TMIET. The best way to follow
progress is to join [the Eternaltwin discord](https://discord.gg/SMJavjs3gk).

eMush is an open source project, you can also follow our progress on GitLab.
In particular, [progress on the **Spores for All!** update](https://gitlab.com/eternaltwin/mush/mush/-/milestones/12#tab-issues).

Thank you for your attention, see you later aboard the Daedalus!

# MyHordes

Play to [MyHordes](https://myhordes.eu/).

_radio crackle_

Hello everyone, I hope everything's fine for you during this cold and windy season _scrambled_

![MyHordes - Wireless technology ad](./myhordes.png)

Thanks to the hard work of our developers, the Season 15 is imminent. As a
reminder _scrambled_ everything will be ready for the 11th February. So get
ready to _scrambled_

You'll also be happy to notice the Altar is not damaged and rusty as always,
after all we can't let those undeads ruin everything. _radio crackle_

A little word about the 43 cannibalism acts... _scrambled_ Mmmmh... No nothing
to add.

I think there is nothing else right, Ben? _scrambled_ But close this
god damn door! _scrambled_

# Eternal DinoRPG

Welcome to this section dedicated to [Eternal DinoRPG](https://dinorpg.eternaltwin.org/).

We released the 0.5.0 version a few days ago. It adds the "Pigmou" and the
fist missions. We see about 60 Pigmou and 30 completed missions already!

We're already working on the next version. It will add content: you'll be able
to talk to Non Playable Characters (NPCs) and have new missions!

# Eternal Kingdom

Hello everyone, here are the news for [Eternal Kingdom](https://kingdom.eternaltwin.org/)!

Following the deployment of the pre-Alpha 5 version ([see change details](https://eternaltwin.org/forum/threads/9074673d-a530-4780-9fc2-4ebc8743152a)),
you were hundreds of players joining our test map. Thank you all for your
interest!

A new member joined our team: _NSunshine_, he's working on updating the unit
transfer interface. Talsi is working on improving our map (new rendering engine,
evolving towns, generals, ...). He's also working on translating city names
into different languages. We're working on the version Alpha 5. It will focus
on generals (unit transfers, movement), health and trading.

Thank you again for all your support, message, bug reports! See you soon!

# Eternalfest

It will soon be the 10th anniversary of [Eternalfest](https://eternalfest.net),
the project started on March 5th 2013. To celebrate this, we already released a
_massive_ update in January.

**You can now view the items you've collected over the years in the new
_My Profile_ tab.** This was the single most wanted features, it's finally there! 🎉
We'll keep improving the profile page in the next updates.

The other major update is full support for in-game translations. The first game
using this feature is scheduled to be released in February.

There's a bunch of other features. You can now add games to your favorites,
games can have multiple versions in parallel (e.g. "main" and "beta"), you
can schedule any number of updates ahead of time, etc.

These feature were possible thanks to _Demurgos_ and _Moulins_ who rebuilt the
game management system to be more flexible. This was a long-standing task that
is finally complete. It will allow completing the migration of Eternalfest
to the Rust programming language. This new game system will also be imported
into Eternaltwin and serve as the basis for an improved integration with the
different games (game profiles, rewards).

# Neoparc

Hello [Dinoz masters](https://neoparc.eternaltwin.org)!

**The first clan war is officially started!** You will find details in the _Help_
section of the website. We hope that will bring you glory and wealth! Good luck
to you and be merciless!

![Neoparc - Clan War](./neoparc.png)

We also published various fixes, in particular around the Dinoville mission
center. You will now receive a _Pruniac_ when an egg hatches. The raid rewards
are also lowered (e.g. no free Irma potions).

Finally, the skill "Engineer" now has a use: it increases the durability of
Dinojak beers, prismatik charms and items sold at the _Anomaly Cavern_.

# Closing words

You can send [your messages for February](https://gitlab.com/eternaltwin/etwin/-/issues/58).

This article was edited by: Bibni, Biosha, Demurgos, Dylan57, Evian,
Fer, Jonathan, Patate404, and Schroedinger.
