use crate::cmd::CliContext;
use clap::Parser;
pub mod constants;
pub mod errors;
pub mod kotlin;
pub mod php;
pub mod structure;

#[derive(Debug, Parser)]
pub struct Args {
  #[clap(subcommand)]
  command: Command,
}

#[derive(Debug, Parser)]
pub enum Command {
  /// Run the `constants` subcommand
  #[clap(name = "constants")]
  Constants(constants::Args),
  /// Generate public error types
  #[clap(name = "errors")]
  Errors(errors::Args),
  /// Generate Kotlin definitions
  #[clap(name = "kotlin")]
  Kotlin(kotlin::Args),
  /// Generate PHP definitions
  #[clap(name = "php")]
  Php(php::Args),
  /// Run the `structure` subcommand
  #[clap(name = "structure")]
  Structure(structure::Args),
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum Error {
  #[error("constants error")]
  Constants(#[from] constants::Error),
  #[error("`errors` command error")]
  Errors(#[from] errors::Error),
  #[error("kotlin error")]
  Kotlin(#[from] kotlin::Error),
  #[error("php error")]
  Php(#[from] php::Error),
  #[error("structure error")]
  Structure(#[from] structure::Error),
}

pub async fn run(cx: CliContext<'_, Args>) -> Result<(), Error> {
  match &cx.args.command {
    Command::Constants(ref args) => constants::run(CliContext {
      args,
      env: cx.env,
      working_dir: cx.working_dir,
      out: cx.out,
    })?,
    Command::Errors(ref args) => errors::run(CliContext {
      args,
      env: cx.env,
      working_dir: cx.working_dir,
      out: cx.out,
    })?,
    Command::Kotlin(ref args) => {
      kotlin::run(CliContext {
        args,
        env: cx.env,
        working_dir: cx.working_dir,
        out: cx.out,
      })
      .await?
    }
    Command::Php(ref args) => {
      php::run(CliContext {
        args,
        env: cx.env,
        working_dir: cx.working_dir,
        out: cx.out,
      })
      .await?
    }
    Command::Structure(ref args) => structure::run(CliContext {
      args,
      env: cx.env,
      working_dir: cx.working_dir,
      out: cx.out,
    })?,
  };
  Ok(())
}
