import {LocaleId} from "@eternaltwin/core/core/locale-id";

export {LocaleId};

/**
 * List of locales supported by Angular, as configured in `angular.json`.
 */
export type NgLocalId =
  "de"
  | "es"
  | "en-US"
  | "fr";

/**
 * Get the Angular locale to use for the provided locale id.
 */
export function toNgLocale(localeId: LocaleId): NgLocalId {
  switch (localeId) {
    case "de-DE":
      return "de";
    case "es-SP":
      return "es";
    case "fr-FR":
      return "fr";
    default:
      return "en-US";
  }
}
