import {CheckId, IoType, KryoContext, Reader, Result, writeError, Writer} from "kryo";
import {CheckKind} from "kryo/checks/check-kind";
import { readVisitor } from "kryo/readers/read-visitor";

export type UrlConstructor = typeof globalThis extends { URL: infer C }
  ? C
  : typeof import("url").URL;

export const Url: UrlConstructor = (globalThis as any).URL;

/**
 * A fully qualified URL following the WHATWG specification.
 */
export type Url = InstanceType<UrlConstructor>;

export const $Url: IoType<Url> = {
  name: "Url",
  test(cx: KryoContext, value: unknown): Result<Url, CheckId> {
    if (value === null || typeof value !== "object") {
      return writeError(cx, {check: CheckKind.BaseType, expected: ["Object"]});
    }
    if (!(value instanceof Url)) {
      return writeError(cx, {check: CheckKind.InstanceOf, class: "URL"});
    }
    return {ok: true, value};
  },
  equals(left: Url, right: Url): boolean {
    return left.toString() === right.toString();
  },
  lte(left: Url, right: Url): boolean {
    return left.toString() <= right.toString();
  },
  clone(value: Url): Url {
    return new Url(value.toString());
  },
  write<W>(writer: Writer<W>, value: Url): W {
    return writer.writeString(value.toString());
  },
  read<R>(cx: KryoContext, reader: Reader<R>, raw: R): Result<Url, CheckId> {
    return reader.readString(
      cx,
      raw,
      readVisitor({
        fromString(input: string): Result<Url, CheckId> {
          if (input.startsWith("//")) {
            // Use `HTTPS` for protocol-relative URLs.
            return {ok: true, value: new Url(input, "https://localhost/")};
          }
          return {ok: true, value: new Url(input)};
        },
      }),
    );
  },
};
