import {CaseStyle} from "kryo";
import {$Date} from "kryo/date";
import {$Uint8} from "kryo/integer";
import {RecordIoType, RecordType} from "kryo/record";
import {SetType} from "kryo/set";

import {$PeriodLower, PeriodLower} from "../core/period-lower.mjs";
import {$OutboundEmailId, OutboundEmailId} from "./outbound-email-id.mjs";

export interface GetOutboundEmailRequests {
  /**
   * API time, for time-travel querying
   */
  time?: Date;
  /**
   * Filter email requests created in the provided period
   */
  createdAt?: PeriodLower;
  /**
   * Filter email requests for any of the outbound emails in the set
   */
  outboundEmailId?: Set<OutboundEmailId>;
  /**
   * Maximum number of results to return
   */
  limit: number;
}

export const $GetOutboundEmailRequests: RecordIoType<GetOutboundEmailRequests> = new RecordType<GetOutboundEmailRequests>({
  properties: {
    time: {type: $Date, optional: true},
    createdAt: {type: $PeriodLower, optional: true},
    outboundEmailId: {type: new SetType({itemType: $OutboundEmailId, maxSize: 100}), optional: true},
    limit: {type: $Uint8},
  },
  changeCase: CaseStyle.SnakeCase,
});
