import { RecordIoType } from "kryo/record";

import {$Listing, Listing} from "../core/listing.mjs";
import {$OutboundEmail, OutboundEmail} from "./outbound-email.mjs";

export type OutboundEmailListing = Listing<OutboundEmail>;

export const $OutboundEmailListing = $Listing.apply($OutboundEmail) as RecordIoType<OutboundEmailListing>;
