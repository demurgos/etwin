use async_trait::async_trait;
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::core::Handler;
use eternaltwin_core::oauth::{
  RfcOauthAccessTokenKey, RfcOauthRefreshTokenKey, TwinoidAccessToken, TwinoidRefreshToken,
};
use eternaltwin_core::twinoid::{store, ArchivedTwinoidUser, ShortTwinoidUser, TwinoidUserId};
use std::collections::BTreeMap;
use std::sync::RwLock;

struct StoreState {
  users: BTreeMap<TwinoidUserId, ArchivedTwinoidUser>,
  access_token: BTreeMap<RfcOauthAccessTokenKey, TwinoidUserId>,
  refresh_token: BTreeMap<RfcOauthRefreshTokenKey, TwinoidUserId>,
}

impl StoreState {
  fn new() -> Self {
    Self {
      users: BTreeMap::new(),
      access_token: BTreeMap::new(),
      refresh_token: BTreeMap::new(),
    }
  }

  fn get_user(&self, id: &TwinoidUserId) -> Option<&ArchivedTwinoidUser> {
    self.users.get(id)
  }

  fn touch_user(&mut self, user: ArchivedTwinoidUser) {
    self.users.insert(user.id, user);
  }
}

pub struct MemTwinoidStore<TyClock>
where
  TyClock: ClockRef,
{
  clock: TyClock,
  state: RwLock<StoreState>,
}

impl<TyClock> MemTwinoidStore<TyClock>
where
  TyClock: ClockRef,
{
  pub fn new(clock: TyClock) -> Self {
    Self {
      clock,
      state: RwLock::new(StoreState::new()),
    }
  }
}

#[async_trait]
impl<TyClock> Handler<store::GetShortUser> for MemTwinoidStore<TyClock>
where
  TyClock: ClockRef,
{
  async fn handle(&self, req: store::GetShortUser) -> Result<ShortTwinoidUser, store::GetShortUserError> {
    let state = self.state.read().unwrap();
    let user = state
      .get_user(&req.user.id)
      .cloned()
      .ok_or(store::GetShortUserError::NotFound)?;
    Ok(ShortTwinoidUser::from(user))
  }
}

#[async_trait]
impl<TyClock> Handler<store::GetTwinoidAccessToken> for MemTwinoidStore<TyClock>
where
  TyClock: ClockRef,
{
  async fn handle(
    &self,
    req: store::GetTwinoidAccessToken,
  ) -> Result<TwinoidAccessToken, store::GetTwinoidAccessTokenError> {
    let state = self.state.read().unwrap();
    let token = state.access_token.iter().find(|(_key, uid)| match req.twinoid_user {
      Some(wanted) => **uid == wanted.id,
      None => true,
    });

    match token {
      None => Err(store::GetTwinoidAccessTokenError::NotFound),
      Some((key, user_id)) => Ok(TwinoidAccessToken {
        key: key.clone(),
        created_at: self.clock.clock().now(),
        accessed_at: self.clock.clock().now(),
        expires_at: self.clock.clock().now(),
        twinoid_user_id: *user_id,
      }),
    }
  }
}

#[async_trait]
impl<TyClock> Handler<store::GetTwinoidRefreshToken> for MemTwinoidStore<TyClock>
where
  TyClock: ClockRef,
{
  async fn handle(
    &self,
    req: store::GetTwinoidRefreshToken,
  ) -> Result<TwinoidRefreshToken, store::GetTwinoidRefreshTokenError> {
    let state = self.state.read().unwrap();
    let token = state.refresh_token.iter().find(|(_key, uid)| match req.twinoid_user {
      Some(wanted) => **uid == wanted.id,
      None => true,
    });

    match token {
      None => Err(store::GetTwinoidRefreshTokenError::NotFound),
      Some((key, user_id)) => Ok(TwinoidRefreshToken {
        key: key.clone(),
        created_at: self.clock.clock().now(),
        accessed_at: self.clock.clock().now(),
        twinoid_user_id: *user_id,
      }),
    }
  }
}

#[async_trait]
impl<TyClock> Handler<store::GetUser> for MemTwinoidStore<TyClock>
where
  TyClock: ClockRef,
{
  async fn handle(&self, req: store::GetUser) -> Result<ArchivedTwinoidUser, store::GetUserError> {
    let state = self.state.read().unwrap();
    let user = state
      .get_user(&req.user.id)
      .cloned()
      .ok_or(store::GetUserError::NotFound)?;
    Ok(user)
  }
}

#[async_trait]
impl<TyClock> Handler<store::TouchTwinoidOauth> for MemTwinoidStore<TyClock>
where
  TyClock: ClockRef,
{
  async fn handle(&self, req: store::TouchTwinoidOauth) -> Result<(), store::TouchTwinoidOauthError> {
    let mut state = self.state.write().unwrap();
    state.access_token.insert(req.access_token.clone(), req.twinoid_user.id);
    if let Some(refresh_token) = req.refresh_token.clone() {
      state.refresh_token.insert(refresh_token, req.twinoid_user.id);
    }
    Ok(())
  }
}

#[async_trait]
impl<TyClock> Handler<store::TouchShortUser> for MemTwinoidStore<TyClock>
where
  TyClock: ClockRef,
{
  async fn handle(&self, req: store::TouchShortUser) -> Result<(), store::TouchShortUserError> {
    let short = req.user;
    let mut state = self.state.write().unwrap();
    let now = self.clock.clock().now();
    let user = ArchivedTwinoidUser {
      id: short.id,
      archived_at: now,
      display_name: short.display_name.clone(),
      links: Vec::new(),
    };
    state.touch_user(user.clone());
    Ok(())
  }
}

#[async_trait]
impl<TyClock> Handler<store::TouchSite> for MemTwinoidStore<TyClock>
where
  TyClock: ClockRef,
{
  async fn handle(&self, req: store::TouchSite) -> Result<(), store::TouchSiteError> {
    eprintln!("skipped: MemTwinoidStore::Handl<TouchSite>: {req:?}");
    Ok(())
  }
}

#[async_trait]
impl<TyClock> Handler<store::TouchFullUserList> for MemTwinoidStore<TyClock>
where
  TyClock: ClockRef,
{
  async fn handle(&self, req: store::TouchFullUserList) -> Result<(), store::TouchFullUserListError> {
    eprintln!("skipped: MemTwinoidStore::Handl<TouchFullUserList>: {req:?}");
    Ok(())
  }
}

#[async_trait]
impl<TyClock> Handler<store::TouchSafeUserList> for MemTwinoidStore<TyClock>
where
  TyClock: ClockRef,
{
  async fn handle(&self, req: store::TouchSafeUserList) -> Result<(), store::TouchSafeUserListError> {
    eprintln!("skipped: MemTwinoidStore::Handl<TouchSafeUserList>: {req:?}");
    Ok(())
  }
}

#[cfg(test)]
mod test {
  use crate::mem::MemTwinoidStore;
  use crate::test::TestApi;
  use eternaltwin_core::clock::VirtualClock;
  use eternaltwin_core::core::Instant;
  use eternaltwin_core::twinoid::TwinoidStore;
  use std::sync::Arc;

  fn make_test_api() -> TestApi<Arc<VirtualClock>, Arc<dyn TwinoidStore>> {
    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let twinoid_store: Arc<dyn TwinoidStore> = Arc::new(MemTwinoidStore::new(Arc::clone(&clock)));

    TestApi { clock, twinoid_store }
  }

  #[tokio::test]
  async fn test_empty() {
    crate::test::test_empty(make_test_api()).await;
  }

  #[tokio::test]
  async fn test_touch_user() {
    crate::test::test_touch_user(make_test_api()).await;
  }

  #[tokio::test]
  async fn test_get_missing_user() {
    crate::test::test_get_missing_user(make_test_api()).await;
  }
}
