# Next

- **[Feature]** Add `HttpJson` Opentelemetry exporter. This uses the OTLP JSON-over-HTTP format and
  uses the same options as the `Grpc` exporter.
- **[Fix]** Refactor API errors, start conversion such that all errors have a code and description.
- **[Fix]** Improve OAuth token claim errors, especially around time period validation.
- **[Fix]** Update to Opentelemetry `0.25`.
- **[Internal]** Implement `sqlx` error wrapper supporting `Clone` and `Eq`.
- **[Internal]** Update Docker images used in Gitlab CI.
- **[Internal]** Remove `AnyError` error type. All errors are now `Clone` and `Eq`.

# 0.14.3 (2024-05-01)

- **[Feature]** Handle events in `Human` Otel processors
- **[Feature]** Print all attributes in `Human` Otel processors
- **[Feature]** Propagate remote Otel context
- **[Fix]** Handle deprecated Otel attributes
- **[Fix]** Otel proxy now supports spans without the `sampled` flag.

# 0.14.2 (2024-04-25)

- **[Fix]** Add more details on callback URI mismatch
- **[Fix]** Add seed app for `brute_dev` and `kingdom_dev`
- **[Fix]** Fix Eternaltwin client for Node
- **[Internal]** Fix release script to update all dependency versions

# 0.14.0 (2024-04-20)

- **[Feature]** Return of the CHANGELOG
- **[Feature]** Add OTLP support. This is used both internally, as well as allowing Eternaltwin to act as an OTLP collector for apps.
- **[Feature]** Add SDK package for PHP
- **[Feature]** Support setting the listen interface with the new `backend.listen` config. This allows listening on IPv4
- **[Fix]** Improve OAuth error messages
- **[Fix]** Fix Windows precompiled binary resolution for the npm package `eternaltwin/exe`
- **[Fix]** Update to Angular 17
- **[Fix]** Update to Axum 0.7
- **[Fix]** Fix default Eternalfest port
- **[Fix]** Refresh dependency graph
- **[Fix]** Update exe packages for Node
- **[Fix]** Fix linking for Hammerfest users on the EN and ES servers
- **[Fix]** Display config source on start (when printing resolved config)

# 2022-11-13

- **[Feature]** Publish TMIET#12 and TMIET#13
- **[Fix]** Update app version to `0.6.4`.
- **[Fix]** Convert all backend endpoints to Rust.
- **[Fix]** Use REST API for server-side rendering. It used custom implementations previously.

# 2022-09-07

- **[Feature]** Publish TMIET#11

# 2022-08-29

- **[Fix]** Do not apply the username lock when there is no username.

# 2022-08-29

- **[Fix]** Update app version to `0.6.3`.

# 2022-08-26

- **[Fix]** Update app version to `0.6.2`.
- **[Fix]** Update dependencies.
