use include_dir::include_dir;
pub use include_dir::{Dir, DirEntry, File};

#[cfg(eternaltwin_app_local_build)]
pub static BROWSER: Dir = include_dir!("$ETERNALTWIN_APP_BROWSER_DIR");
#[cfg(not(eternaltwin_app_local_build))]
pub static BROWSER: Dir = include_dir!("$CARGO_MANIFEST_DIR/browser");

#[cfg(test)]
mod test {
  use crate::BROWSER;

  #[test]
  fn has_favicon() {
    assert!(BROWSER.contains("favicon.ico"));
    assert!(BROWSER.contains("index.html"));
  }
}
