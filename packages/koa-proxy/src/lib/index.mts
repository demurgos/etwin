import { IncomingMessage, ServerResponse } from "http";
import ProxyServer from "http-proxy";
import { DefaultContext, DefaultState, Middleware, ParameterizedContext } from "koa";

export interface ProxyOptions {
  target: URL;
}

export function koaProxy<State = DefaultState, Context = DefaultContext, ResponseBody = unknown>(options: ProxyOptions): Middleware<State, Context, ResponseBody> {
  const proxy: ProxyServer<IncomingMessage, ServerResponse> = ProxyServer.createProxyServer({target: options.target});

  return (cx: ParameterizedContext<State, Context, ResponseBody>): Promise<void> => {
    return new Promise((resolve, reject) => {
      const res: ServerResponse = cx.res;
      let settled: boolean = false;
      const onSuccess = () => {
        if (!settled) {
          settled = true;
          res.removeListener("finish", onSuccess);
          res.removeListener("close", onSuccess);
          resolve();
        }
      };
      const onError = (err: Error): void => {
        if (!settled) {
          settled = true;
          res.removeListener("finish", onSuccess);
          res.removeListener("close", onSuccess);
          reject(err);
        }
      };
      if (res.closed) {
        onSuccess();
        return;
      }
      res.once("finish", onSuccess);
      res.once("close", onSuccess);
      proxy.web(cx.req, cx.res, {}, onError);
    });
  };
}
