use async_trait::async_trait;
use eternaltwin_core::api::SyncRef;
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::core::{Instant, SecretString};
use eternaltwin_core::dinoparc::{DinoparcServer, DinoparcSessionKey, DinoparcUserIdRef, StoredDinoparcSession};
use eternaltwin_core::token::{GetDinoparcTokenError, RevokeDinoparcTokenError, TokenStore, TouchDinoparcTokenError};
use eternaltwin_core::types::WeakError;
use eternaltwin_populate::dinoparc::{populate_dinoparc, PopulateDinoparcError};
use eternaltwin_populate::hammerfest::{populate_hammerfest, PopulateHammerfestError};
use sqlx::PgPool;

pub struct PgTokenStore<TyClock, TyDatabase>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
{
  clock: TyClock,
  database: TyDatabase,
  database_secret: SecretString,
}

#[derive(Debug, thiserror::Error)]
pub enum NewPgTokenStoreError {
  #[error("failed to begin transaction")]
  BeginTx(#[source] WeakError),
  #[error("failed to populate dinoparc tables")]
  PopulateDinoparc(#[from] PopulateDinoparcError),
  #[error("failed to populate hammerfest tables")]
  PopulateHammerfest(#[from] PopulateHammerfestError),
  #[error("failed to commit transaction")]
  CommitTx(#[source] WeakError),
}

impl<TyClock, TyDatabase> PgTokenStore<TyClock, TyDatabase>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
{
  pub async fn new(
    clock: TyClock,
    database: TyDatabase,
    database_secret: SecretString,
  ) -> Result<Self, NewPgTokenStoreError> {
    let mut tx = database
      .begin()
      .await
      .map_err(|e| NewPgTokenStoreError::BeginTx(WeakError::wrap(e)))?;
    populate_dinoparc(&mut tx).await?;
    populate_hammerfest(&mut tx).await?;
    tx.commit()
      .await
      .map_err(|e| NewPgTokenStoreError::CommitTx(WeakError::wrap(e)))?;
    Ok(Self {
      clock,
      database,
      database_secret,
    })
  }
}

#[async_trait]
impl<TyClock, TyDatabase> TokenStore for PgTokenStore<TyClock, TyDatabase>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
{
  async fn touch_dinoparc(
    &self,
    user: DinoparcUserIdRef,
    key: &DinoparcSessionKey,
  ) -> Result<StoredDinoparcSession, TouchDinoparcTokenError> {
    let mut tx = self.database.begin().await.map_err(TouchDinoparcTokenError::other)?;
    let now = self.clock.clock().now();

    {
      let res = sqlx::query(
        r"
          WITH revoked AS (
          DELETE FROM dinoparc_sessions AS hs
          WHERE hs.dinoparc_server = $2::DINOPARC_SERVER
            AND (
              (
              hs._dinoparc_session_key_hash = digest($3::DINOPARC_SESSION_KEY, 'sha256')
                AND hs.dinoparc_user_id <> $4::DINOPARC_USER_ID
              )
              OR (
              hs._dinoparc_session_key_hash <> digest($3::DINOPARC_SESSION_KEY, 'sha256')
                AND hs.dinoparc_user_id = $4::DINOPARC_USER_ID
              )
            )
          RETURNING dinoparc_server, dinoparc_session_key,_dinoparc_session_key_hash, dinoparc_user_id, ctime, atime
        )
        INSERT INTO old_dinoparc_sessions(dinoparc_server, dinoparc_session_key, _dinoparc_session_key_hash, dinoparc_user_id, ctime, atime, dtime)
        SELECT revoked.*, $1::INSTANT AS dtime
        FROM revoked;",
      )
        .bind(now)
        .bind(user.server)
        .bind(key)
        .bind(user.id)
        .execute(&mut *tx)
        .await.map_err(TouchDinoparcTokenError::other)?;
      // Affected row counts:
      // +0-1: Session matched a different previous user
      // +0-1: User matched a different older session
      assert!((0..=2u64).contains(&res.rows_affected()));
    }

    let session = {
      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        ctime: Instant,
        atime: Instant,
      }

      let row: Row = sqlx::query_as::<_, Row>(
        r"
        INSERT INTO dinoparc_sessions(dinoparc_server, dinoparc_session_key, _dinoparc_session_key_hash, dinoparc_user_id, ctime, atime)
        VALUES ($3::DINOPARC_SERVER, pgp_sym_encrypt($4::DINOPARC_SESSION_KEY, $2::TEXT), digest($4::DINOPARC_SESSION_KEY, 'sha256'), $5::DINOPARC_USER_ID, $1::INSTANT, $1::INSTANT)
        ON CONFLICT (dinoparc_server, _dinoparc_session_key_hash)
          DO UPDATE SET atime = $1::INSTANT
        RETURNING ctime, atime;",
      )
        .bind(now)
        .bind(self.database_secret.as_str())
        .bind(user.server)
        .bind(key)
        .bind(user.id)
        .fetch_one(&mut *tx)
        .await.map_err(TouchDinoparcTokenError::other)?;

      StoredDinoparcSession {
        key: key.clone(),
        user,
        ctime: row.ctime,
        atime: row.atime,
      }
    };

    tx.commit().await.map_err(TouchDinoparcTokenError::other)?;

    Ok(session)
  }

  async fn revoke_dinoparc(
    &self,
    server: DinoparcServer,
    key: &DinoparcSessionKey,
  ) -> Result<(), RevokeDinoparcTokenError> {
    let now = self.clock.clock().now();
    let res = sqlx::query(
      r"
        WITH revoked AS (
          DELETE FROM dinoparc_sessions
            WHERE dinoparc_server = $2::DINOPARC_SERVER AND _dinoparc_session_key_hash = digest($3::DINOPARC_SESSION_KEY, 'sha256')
            RETURNING dinoparc_server, dinoparc_session_key,_dinoparc_session_key_hash, dinoparc_user_id, ctime, atime
        )
        INSERT INTO old_dinoparc_sessions(dinoparc_server, dinoparc_session_key, _dinoparc_session_key_hash, dinoparc_user_id, ctime, atime, dtime)
        SELECT revoked.*, $1::INSTANT AS dtime
        FROM revoked;",
    )
      .bind(now)
      .bind(server)
      .bind(key)
      .execute(&*self.database)
      .await.map_err(RevokeDinoparcTokenError::other)?;
    // Affected row counts:
    // 0: The revoked session key does not exist (or is already revoked)
    // 1: The session key was revoked
    assert!((0..=1u64).contains(&res.rows_affected()));
    Ok(())
  }

  async fn get_dinoparc(
    &self,
    user: DinoparcUserIdRef,
  ) -> Result<Option<StoredDinoparcSession>, GetDinoparcTokenError> {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      dinoparc_session_key: DinoparcSessionKey,
      ctime: Instant,
      atime: Instant,
    }

    let row: Option<Row> = sqlx::query_as::<_, Row>(
      r"
          SELECT pgp_sym_decrypt(dinoparc_session_key, $1::TEXT) AS dinoparc_session_key, ctime, atime
          FROM dinoparc_sessions
          WHERE dinoparc_server = $2::DINOPARC_SERVER AND dinoparc_user_id = $3::DINOPARC_USER_ID;
        ",
    )
    .bind(self.database_secret.as_str())
    .bind(user.server)
    .bind(user.id)
    .fetch_optional(&*self.database)
    .await
    .map_err(GetDinoparcTokenError::other)?;

    let result = row.map(|r| StoredDinoparcSession {
      ctime: r.ctime,
      atime: r.atime,
      key: r.dinoparc_session_key,
      user,
    });

    Ok(result)
  }
}

#[cfg(test)]
mod test {
  use super::PgTokenStore;
  use crate::test::TestApi;
  use eternaltwin_core::clock::VirtualClock;
  use eternaltwin_core::core::{Instant, SecretString};
  use eternaltwin_core::dinoparc::DinoparcStore;
  use eternaltwin_core::token::TokenStore;
  use eternaltwin_core::uuid::Uuid4Generator;
  use eternaltwin_db_schema::force_create_latest;
  use eternaltwin_dinoparc_store::pg::PgDinoparcStore;
  use serial_test::serial;
  use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
  use sqlx::PgPool;
  use std::sync::Arc;

  async fn make_test_api() -> TestApi<Arc<VirtualClock>, Arc<dyn DinoparcStore>, Arc<dyn TokenStore>> {
    let config = eternaltwin_config::Config::for_test();
    let admin_database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.admin_user.value)
          .password(&config.postgres.admin_password.value),
      )
      .await
      .unwrap();
    force_create_latest(&admin_database, true).await.unwrap();
    admin_database.close().await;

    let database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.user.value)
          .password(&config.postgres.password.value),
      )
      .await
      .unwrap();
    let database = Arc::new(database);

    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let uuid_generator = Arc::new(Uuid4Generator);
    let database_secret = SecretString::new("dev_secret".to_string());
    let dinoparc_store: Arc<dyn DinoparcStore> = Arc::new(
      PgDinoparcStore::new(Arc::clone(&clock), Arc::clone(&database), Arc::clone(&uuid_generator))
        .await
        .unwrap(),
    );
    let token_store: Arc<dyn TokenStore> = Arc::new(
      PgTokenStore::new(Arc::clone(&clock), Arc::clone(&database), database_secret)
        .await
        .unwrap(),
    );

    TestApi {
      clock,
      dinoparc_store,
      token_store,
    }
  }

  test_token_store!(
    #[serial]
    || make_test_api().await
  );
}
