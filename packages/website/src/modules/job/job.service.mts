import {Injectable} from "@angular/core";
import {$CreateJob, CreateJob} from "@eternaltwin/core/job/create-job";
import {$GetJobs, GetJobs} from "@eternaltwin/core/job/get-jobs";
import {$JobListing, JobListing} from "@eternaltwin/core/job/job-listing";
import {$Any} from "kryo/any";
import {Observable} from "rxjs";

import {RestService} from "../rest/rest.service.mjs";

@Injectable()
export class JobService {
  readonly #rest: RestService;

  constructor(rest: RestService) {
    this.#rest = rest;
  }

  createJob(
    cmd: CreateJob,
  ): Observable<unknown> {
    return this.#rest.post(["job", "jobs"], {
      reqType: $CreateJob,
      req: cmd,
      resType: $Any,
    });
  }

  getJobs(
    query: GetJobs,
  ): Observable<JobListing> {
    return this.#rest.get(["job", "jobs"], {
      queryType: $GetJobs,
      query,
      resType: $JobListing,
    });
  }
}
