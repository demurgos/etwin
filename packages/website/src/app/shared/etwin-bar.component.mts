import { Component, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { AuthContext } from "@eternaltwin/core/auth/auth-context";
import { AuthType } from "@eternaltwin/core/auth/auth-type";
import { CompleteUser } from "@eternaltwin/core/user/complete-user";
import { MaybeCompleteUser } from "@eternaltwin/core/user/maybe-complete-user";
import { Observable, Subscription } from "rxjs";

import { AuthService } from "../../modules/auth/auth.service.mjs";
import { UserService } from "../../modules/user/user.service.mjs";

@Component({
  selector: "etwin-bar",
  templateUrl: "./etwin-bar.component.html",
  styleUrls: [],
})
export class EtwinBarComponent implements OnDestroy {
  public readonly AuthType = AuthType;

  public readonly auth$: Observable<AuthContext>;
  public shouldCompleteProfile: boolean = false;

  private readonly auth: AuthService;
  private readonly router: Router;

  private pendingSubscription: Subscription | null;

  constructor(auth: AuthService, router: Router, userService: UserService) {
    this.auth = auth;
    this.router = router;

    this.pendingSubscription = null;
    this.auth$ = auth.auth();
    this.auth$.subscribe((user: AuthContext) => {
      if (user.type !== AuthType.User) {return;}
      userService.getUserById(user.user.id).subscribe((u: MaybeCompleteUser | null) => {
        if (!u) {return;}
        this.shouldCompleteProfile = !(u as CompleteUser).username || !(u as CompleteUser).hasPassword;
      });
    });
  }

  public onSignOut(event: Event) {
    event.preventDefault();
    if (this.pendingSubscription !== null) {
      return;
    }
    const signOutResult$ = this.auth.logout();
    const subscription: Subscription = signOutResult$.subscribe({
      next: async (): Promise<void> => {
        subscription.unsubscribe();
        this.pendingSubscription = null;
        await this.router.navigateByUrl("/");
      },
      error: (err: Error): void => {
        subscription.unsubscribe();
        this.pendingSubscription = null;
      },
      complete: (): void => {
        subscription.unsubscribe();
        this.pendingSubscription = null;
      }
    });
    this.pendingSubscription = subscription;
  }

  ngOnDestroy(): void {
    if (this.pendingSubscription !== null) {
      this.pendingSubscription.unsubscribe();
      this.pendingSubscription = null;
    }
  }
}
