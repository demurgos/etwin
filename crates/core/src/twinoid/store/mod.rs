pub use self::get_short_user::{GetShortUser, GetShortUserError};
pub use self::get_twinoid_access_token::{GetTwinoidAccessToken, GetTwinoidAccessTokenError};
pub use self::get_twinoid_refresh_token::{GetTwinoidRefreshToken, GetTwinoidRefreshTokenError};
pub use self::get_user::{GetUser, GetUserError};
pub use self::touch_full_user_list::{TouchFullUserList, TouchFullUserListError};
pub use self::touch_safe_user_list::{TouchSafeUserList, TouchSafeUserListError};
pub use self::touch_short_user::{TouchShortUser, TouchShortUserError};
pub use self::touch_site::{TouchSite, TouchSiteError};
pub use self::touch_twinoid_oauth::{TouchTwinoidOauth, TouchTwinoidOauthError};
use crate::core::{Date, Handler, HtmlFragment};
use crate::oauth::{TwinoidAccessToken, TwinoidRefreshToken};
use crate::twinoid::api::{Contact, Like, Missing, Restrict, TwinoidGender, UrlRef, User};
use crate::twinoid::client::FullSiteUser;
use crate::twinoid::{ArchivedTwinoidUser, ShortTwinoidUser};
use crate::twinoid::{TwinoidLocale, TwinoidSiteId, TwinoidUserId};
use async_trait::async_trait;
use std::collections::BTreeSet;
use std::ops::Deref;
use url::Url;

pub mod get_short_user;
pub mod get_twinoid_access_token;
pub mod get_twinoid_refresh_token;
pub mod get_user;
pub mod touch_full_user_list;
pub mod touch_safe_user_list;
pub mod touch_short_user;
pub mod touch_site;
pub mod touch_twinoid_oauth;

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct MissingUserRelations {
  pub sites: BTreeSet<TwinoidSiteId>,
  pub stats: BTreeSet<(TwinoidSiteId, String)>,
  pub achievements: BTreeSet<(TwinoidSiteId, String)>,
}

impl MissingUserRelations {
  #[allow(clippy::type_complexity)]
  pub fn from_users<TyName, TyOldNames, TyCity, TyCountry, TyDesc, TyStatus>(
    users: &[User<
      TwinoidUserId,
      TyName,
      Option<UrlRef>,
      TwinoidLocale,
      Option<HtmlFragment>,
      TyOldNames,
      Vec<FullSiteUser>,
      Like<Url, u32>,
      Option<TwinoidGender>,
      Option<Date>,
      TyCity,
      TyCountry,
      TyDesc,
      TyStatus,
      Restrict<Vec<Contact<bool, Box<User>>>>,
      Missing,
      Missing,
    >],
  ) -> Self {
    let mut missing = Self::default();
    for user in users {
      for site_user in user.sites.iter() {
        missing.sites.insert(site_user.site.id);
        if let Some(stats) = site_user.stats.as_deref() {
          for stat in stats {
            missing.stats.insert((site_user.site.id, stat.id.clone()));
          }
        }
        if let Some(achievements) = site_user.achievements.as_deref() {
          for achievement in achievements {
            missing.achievements.insert((site_user.site.id, achievement.id.clone()));
          }
        }
      }
    }
    missing
  }

  pub fn is_empty(&self) -> bool {
    self.sites.is_empty() && self.stats.is_empty() && self.achievements.is_empty()
  }
}

#[async_trait]
pub trait TwinoidStore:
  Send
  + Sync
  + Handler<GetShortUser>
  + Handler<GetTwinoidAccessToken>
  + Handler<GetTwinoidRefreshToken>
  + Handler<GetUser>
  + Handler<TouchShortUser>
  + Handler<TouchSite>
  + Handler<TouchFullUserList>
  + Handler<TouchSafeUserList>
  + Handler<TouchTwinoidOauth>
{
  async fn get_short_user(&self, req: GetShortUser) -> Result<ShortTwinoidUser, GetShortUserError> {
    Handler::<GetShortUser>::handle(self, req).await
  }
  async fn get_twinoid_access_token(
    &self,
    req: GetTwinoidAccessToken,
  ) -> Result<TwinoidAccessToken, GetTwinoidAccessTokenError> {
    Handler::<GetTwinoidAccessToken>::handle(self, req).await
  }
  async fn get_twinoid_refresh_token(
    &self,
    req: GetTwinoidRefreshToken,
  ) -> Result<TwinoidRefreshToken, GetTwinoidRefreshTokenError> {
    Handler::<GetTwinoidRefreshToken>::handle(self, req).await
  }
  async fn get_user(&self, req: GetUser) -> Result<ArchivedTwinoidUser, GetUserError> {
    Handler::<GetUser>::handle(self, req).await
  }
  async fn touch_short_user(&self, req: TouchShortUser) -> Result<(), TouchShortUserError> {
    Handler::<TouchShortUser>::handle(self, req).await
  }
  async fn touch_site(&self, req: TouchSite) -> Result<(), TouchSiteError> {
    Handler::<TouchSite>::handle(self, req).await
  }
  async fn touch_safe_user_list(&self, req: TouchSafeUserList) -> Result<(), TouchSafeUserListError> {
    Handler::<TouchSafeUserList>::handle(self, req).await
  }
  async fn touch_full_user_list(&self, req: TouchFullUserList) -> Result<(), TouchFullUserListError> {
    Handler::<TouchFullUserList>::handle(self, req).await
  }
  async fn touch_twinoid_oauth(&self, req: TouchTwinoidOauth) -> Result<(), TouchTwinoidOauthError> {
    Handler::<TouchTwinoidOauth>::handle(self, req).await
  }
}

impl<T> TwinoidStore for T where
  T: Send
    + Sync
    + Handler<GetShortUser>
    + Handler<GetTwinoidAccessToken>
    + Handler<GetTwinoidRefreshToken>
    + Handler<GetUser>
    + Handler<TouchShortUser>
    + Handler<TouchSite>
    + Handler<TouchSafeUserList>
    + Handler<TouchFullUserList>
    + Handler<TouchTwinoidOauth>
{
}

/// Like [`Deref`], but the target has the bound [`TwinoidStore`]
pub trait TwinoidStoreRef: Send + Sync {
  type TwinoidStore: TwinoidStore + ?Sized;

  fn twinoid_store(&self) -> &Self::TwinoidStore;
}

impl<TyRef> TwinoidStoreRef for TyRef
where
  TyRef: Deref + Send + Sync,
  TyRef::Target: TwinoidStore,
{
  type TwinoidStore = TyRef::Target;

  fn twinoid_store(&self) -> &Self::TwinoidStore {
    self.deref()
  }
}
