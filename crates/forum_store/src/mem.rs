use async_trait::async_trait;
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::core::{Instant, Listing, ListingCount, LocaleId};
use eternaltwin_core::forum::{
  ForumPostId, ForumPostIdRef, ForumPostRevisionContent, ForumPostRevisionId, ForumRole, ForumSection,
  ForumSectionDisplayName, ForumSectionId, ForumSectionIdRef, ForumSectionKey, ForumSectionRef, ForumSectionSelf,
  ForumStore, ForumThreadId, ForumThreadIdRef, ForumThreadKey, ForumThreadRef, ForumThreadTitle,
  GetForumSectionMetaOptions, GetSectionMetaError, GetThreadMetaError, RawAddModeratorError, RawAddModeratorOptions,
  RawCreateForumPostResult, RawCreateForumPostRevisionResult, RawCreateForumThreadResult, RawCreatePostError,
  RawCreatePostOptions, RawCreatePostRevisionError, RawCreatePostRevisionOptions, RawCreateThreadError,
  RawCreateThreadOptions, RawDeleteModeratorError, RawDeleteModeratorOptions, RawForumPost, RawForumPostRevision,
  RawForumRoleGrant, RawForumSectionMeta, RawForumThreadMeta, RawGetForumPostOptions, RawGetForumThreadMetaOptions,
  RawGetPostError, RawGetPostsError, RawGetPostsOptions, RawGetRoleGrantsError, RawGetRoleGrantsOptions,
  RawGetSectionsError, RawGetSectionsOptions, RawGetThreadsError, RawGetThreadsOptions,
  RawLatestForumPostRevisionListing, RawShortForumPost, UpsertSystemSectionError, UpsertSystemSectionOptions,
};
use eternaltwin_core::user::UserId;
use eternaltwin_core::uuid::{UuidGenerator, UuidGeneratorRef};
use std::collections::HashMap;
use std::sync::RwLock;

pub struct MemForumStore<TyClock, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  clock: TyClock,
  state: RwLock<StoreState>,
  uuid_generator: TyUuidGenerator,
}

impl<TyClock, TyUuidGenerator> MemForumStore<TyClock, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  pub fn new(clock: TyClock, uuid_generator: TyUuidGenerator) -> Self {
    Self {
      clock,
      state: RwLock::new(StoreState::new()),
      uuid_generator,
    }
  }
}

#[derive(Debug, Clone)]
struct MemForumSection {
  id: ForumSectionId,
  key: Option<ForumSectionKey>,
  display_name: ForumSectionDisplayName,
  ctime: Instant,
  locale: Option<LocaleId>,
  threads: Vec<ForumThreadIdRef>,
  grants: HashMap<UserId, RawForumRoleGrant>,
}

#[derive(Debug, Clone)]
struct MemForumThread {
  id: ForumThreadId,
  key: Option<ForumThreadKey>,
  title: ForumThreadTitle,
  section: ForumSectionIdRef,
  ctime: Instant,
  is_pinned: bool,
  is_locked: bool,
  posts: Vec<ForumPostIdRef>,
}

#[derive(Debug, Clone)]
struct MemForumPost {
  id: ForumPostId,
  thread: ForumThreadIdRef,
  revisions: Vec<ForumPostRevisionId>,
}

struct StoreState {
  sections: HashMap<ForumSectionId, MemForumSection>,
  sections_by_key: HashMap<ForumSectionKey, ForumSectionId>,
  threads: HashMap<ForumThreadId, MemForumThread>,
  threads_by_key: HashMap<ForumThreadKey, ForumThreadId>,
  posts: HashMap<ForumPostId, MemForumPost>,
  revisions: HashMap<ForumPostRevisionId, RawForumPostRevision>,
}

impl StoreState {
  fn new() -> Self {
    Self {
      sections: HashMap::new(),
      sections_by_key: HashMap::new(),
      threads: HashMap::new(),
      threads_by_key: HashMap::new(),
      posts: HashMap::new(),
      revisions: HashMap::new(),
    }
  }

  fn upsert_system_section(
    &mut self,
    now: Instant,
    id: ForumSectionId,
    options: &UpsertSystemSectionOptions,
  ) -> ForumSection {
    let id = *self.sections_by_key.entry(options.key.clone()).or_insert(id);
    let section = self.sections.entry(id).or_insert_with(|| MemForumSection {
      id,
      key: Some(options.key.clone()),
      ctime: now,
      locale: options.locale,
      display_name: options.display_name.clone(),
      threads: vec![],
      grants: HashMap::new(),
    });
    ForumSection {
      id,
      key: section.key.clone(),
      display_name: section.display_name.clone(),
      ctime: section.ctime,
      locale: section.locale,
      threads: Listing {
        offset: 0,
        limit: 20,
        count: 0,
        items: vec![],
      },
      role_grants: vec![],
      this: ForumSectionSelf { roles: vec![] },
    }
  }

  fn get_section_meta(&self, options: &GetForumSectionMetaOptions) -> Result<RawForumSectionMeta, GetSectionMetaError> {
    let section_id = match &options.section {
      ForumSectionRef::Id(r) => r.id,
      ForumSectionRef::Key(r) => *self.sections_by_key.get(&r.key).ok_or(GetSectionMetaError::NotFound)?,
    };
    let section = self.sections.get(&section_id).ok_or(GetSectionMetaError::NotFound)?;
    Ok(RawForumSectionMeta {
      id: section.id,
      key: section.key.clone(),
      display_name: section.display_name.clone(),
      ctime: section.ctime,
      locale: section.locale,
      threads: ListingCount {
        count: u32::try_from(section.threads.len()).unwrap(),
      },
      role_grants: {
        let mut grants = section.grants.values().cloned().collect::<Vec<_>>();
        grants.sort_by(|left, right| Ord::cmp(&left.start_time, &right.start_time));
        grants
      },
    })
  }

  fn get_sections(&self, options: &RawGetSectionsOptions) -> Listing<RawForumSectionMeta> {
    let mut sections = self.sections.values().collect::<Vec<_>>();
    sections.sort_by(|left, right| Ord::cmp(&left.ctime, &right.ctime));
    let sections = sections
      .into_iter()
      .skip(usize::try_from(options.offset).unwrap())
      .take(usize::try_from(options.limit).unwrap())
      .map(|s| RawForumSectionMeta {
        id: s.id,
        key: s.key.clone(),
        display_name: s.display_name.clone(),
        ctime: s.ctime,
        locale: s.locale,
        threads: ListingCount {
          count: u32::try_from(s.threads.len()).unwrap(),
        },
        role_grants: {
          let mut grants = s.grants.values().cloned().collect::<Vec<_>>();
          grants.sort_by(|left, right| Ord::cmp(&left.start_time, &right.start_time));
          grants
        },
      })
      .collect::<Vec<_>>();

    Listing {
      offset: options.offset,
      limit: options.limit,
      count: u32::try_from(self.sections.len()).unwrap(),
      items: sections,
    }
  }

  fn create_thread(
    &mut self,
    now: Instant,
    thread_id: ForumThreadId,
    post_id: ForumPostId,
    revision_id: ForumPostRevisionId,
    options: &RawCreateThreadOptions,
  ) -> RawCreateForumThreadResult {
    let section_id = match &options.section {
      ForumSectionRef::Id(r) => r.id,
      ForumSectionRef::Key(r) => *self.sections_by_key.get(&r.key).expect("unknown section key"),
    };
    let section = self.sections.get_mut(&section_id).unwrap();
    section.threads.push(thread_id.into());
    let old = self.threads.insert(
      thread_id,
      MemForumThread {
        id: thread_id,
        key: None,
        title: options.title.clone(),
        section: section_id.into(),
        ctime: now,
        is_pinned: false,
        is_locked: false,
        posts: vec![post_id.into()],
      },
    );
    assert!(old.is_none());
    let old = self.posts.insert(
      post_id,
      MemForumPost {
        id: post_id,
        thread: thread_id.into(),
        revisions: vec![revision_id],
      },
    );
    assert!(old.is_none());
    let revision = RawForumPostRevision {
      id: revision_id,
      time: now,
      author: options.actor.clone(),
      content: Some(ForumPostRevisionContent {
        marktwin: options.body_mkt.clone(),
        html: options.body_html.clone(),
      }),
      moderation: None,
      comment: None,
    };
    let old = self.revisions.insert(revision_id, revision.clone());
    assert!(old.is_none());
    RawCreateForumThreadResult {
      id: thread_id,
      key: None,
      title: options.title.clone(),
      section: section_id.into(),
      ctime: now,
      is_pinned: false,
      is_locked: false,
      post_id,
      post_revision: revision,
    }
  }

  fn get_thread_meta(&self, options: &RawGetForumThreadMetaOptions) -> Result<RawForumThreadMeta, GetThreadMetaError> {
    let thread_id = match &options.thread {
      ForumThreadRef::Id(r) => r.id,
      ForumThreadRef::Key(r) => *self.threads_by_key.get(&r.key).expect("unknown thread key"),
    };
    let thread = self.threads.get(&thread_id).ok_or(GetThreadMetaError::NotFound)?;
    Ok(RawForumThreadMeta {
      id: thread.id,
      key: thread.key.clone(),
      title: thread.title.clone(),
      section: thread.section,
      ctime: thread.ctime,
      is_pinned: thread.is_pinned,
      is_locked: thread.is_locked,
      posts: ListingCount {
        count: u32::try_from(thread.posts.len()).unwrap(),
      },
    })
  }

  fn get_threads(&self, options: &RawGetThreadsOptions) -> Result<Listing<RawForumThreadMeta>, RawGetThreadsError> {
    let section_id = match &options.section {
      ForumSectionRef::Id(r) => r.id,
      ForumSectionRef::Key(r) => *self.sections_by_key.get(&r.key).expect("unknown section key"),
    };
    let section = self.sections.get(&section_id).unwrap();
    let mut items = section.threads.clone();
    items.sort_by(|left, right| {
      let left = self.threads.get(&left.id).unwrap();
      let left = self.posts.get(&left.posts.last().unwrap().id).unwrap();
      let left = self.revisions.get(left.revisions.first().unwrap()).unwrap().time;
      let right = self.threads.get(&right.id).unwrap();
      let right = self.posts.get(&right.posts.last().unwrap().id).unwrap();
      let right = self.revisions.get(right.revisions.first().unwrap()).unwrap().time;
      left.cmp(&right).reverse()
    });
    let items = items
      .iter()
      .skip(usize::try_from(options.offset).unwrap())
      .take(usize::try_from(options.limit).unwrap())
      .map(|thread_ref| {
        let thread = self.threads.get(&thread_ref.id).unwrap();
        RawForumThreadMeta {
          id: thread.id,
          key: thread.key.clone(),
          title: thread.title.clone(),
          section: thread.section,
          ctime: thread.ctime,
          is_pinned: thread.is_pinned,
          is_locked: thread.is_locked,
          posts: ListingCount {
            count: u32::try_from(thread.posts.len()).unwrap(),
          },
        }
      })
      .collect::<Vec<_>>();
    Ok(Listing {
      offset: options.offset,
      limit: options.limit,
      count: u32::try_from(section.threads.len()).unwrap(),
      items,
    })
  }

  fn create_post(
    &mut self,
    now: Instant,
    post_id: ForumPostId,
    revision_id: ForumPostRevisionId,
    options: &RawCreatePostOptions,
  ) -> RawCreateForumPostResult {
    let thread_id = match &options.thread {
      ForumThreadRef::Id(r) => r.id,
      ForumThreadRef::Key(r) => *self.threads_by_key.get(&r.key).expect("unknown thread key"),
    };
    let thread = self.threads.get_mut(&thread_id).expect("thread not found");
    thread.posts.push(post_id.into());
    let old = self.posts.insert(
      post_id,
      MemForumPost {
        id: post_id,
        thread: thread.id.into(),
        revisions: vec![revision_id],
      },
    );
    assert!(old.is_none());
    let revision = RawForumPostRevision {
      id: revision_id,
      time: now,
      author: options.actor.clone(),
      content: Some(options.body.clone()),
      moderation: None,
      comment: None,
    };
    let old = self.revisions.insert(revision_id, revision.clone());
    assert!(old.is_none());
    RawCreateForumPostResult {
      id: post_id,
      thread: thread_id.into(),
      section: thread.section,
      revision,
    }
  }

  fn get_posts(&self, options: &RawGetPostsOptions) -> Result<Listing<RawShortForumPost>, RawGetPostsError> {
    let thread_id = match &options.thread {
      ForumThreadRef::Id(r) => r.id,
      ForumThreadRef::Key(r) => *self.threads_by_key.get(&r.key).expect("unknown thread key"),
    };
    let thread = self.threads.get(&thread_id).unwrap();
    let items = thread
      .posts
      .iter()
      .skip(usize::try_from(options.offset).unwrap())
      .take(usize::try_from(options.limit).unwrap())
      .map(|post_ref| {
        let post = self.posts.get(&post_ref.id).unwrap();
        to_short_post(post, &self.revisions)
      })
      .collect::<Vec<_>>();
    Ok(Listing {
      offset: options.offset,
      limit: options.limit,
      count: u32::try_from(thread.posts.len()).unwrap(),
      items,
    })
  }

  fn get_post(&self, options: &RawGetForumPostOptions) -> Result<RawForumPost, RawGetPostError> {
    let post = self.posts.get(&options.post.id).expect("post not found");
    let thread = self.threads.get(&post.thread.id).expect("thread not found");
    let first_revision = self.revisions.get(post.revisions.first().unwrap()).unwrap();

    let items = post
      .revisions
      .iter()
      .skip(usize::try_from(options.offset).unwrap())
      .take(usize::try_from(options.limit).unwrap())
      .map(|revision_id| self.revisions.get(revision_id).unwrap().clone())
      .collect::<Vec<_>>();
    Ok(RawForumPost {
      id: post.id,
      ctime: first_revision.time,
      author: first_revision.author.clone(),
      revisions: Listing {
        offset: options.offset,
        limit: options.limit,
        count: u32::try_from(post.revisions.len()).unwrap(),
        items,
      },
      thread: RawForumThreadMeta {
        id: thread.id,
        key: thread.key.clone(),
        title: thread.title.clone(),
        section: thread.section,
        ctime: thread.ctime,
        is_pinned: thread.is_pinned,
        is_locked: thread.is_locked,
        posts: ListingCount {
          count: u32::try_from(thread.posts.len()).unwrap(),
        },
      },
    })
  }

  fn create_post_revision(
    &mut self,
    now: Instant,
    revision_id: ForumPostRevisionId,
    options: &RawCreatePostRevisionOptions,
  ) -> RawCreateForumPostRevisionResult {
    let post = self.posts.get_mut(&options.post.id).expect("post not found");
    let thread = self.threads.get_mut(&post.thread.id).expect("thread not found");
    post.revisions.push(revision_id);
    let revision = RawForumPostRevision {
      id: revision_id,
      time: now,
      author: options.actor.clone(),
      content: options.body.clone(),
      moderation: options.mod_body.clone(),
      comment: options.comment.clone(),
    };
    let old = self.revisions.insert(revision_id, revision.clone());
    assert!(old.is_none());
    RawCreateForumPostRevisionResult {
      thread: thread.id.into(),
      section: thread.section,
      revision,
      post: post.id.into(),
    }
  }

  fn add_moderator(&mut self, now: Instant, options: &RawAddModeratorOptions) {
    let section_id = match &options.section {
      ForumSectionRef::Id(r) => r.id,
      ForumSectionRef::Key(r) => *self.sections_by_key.get(&r.key).expect("unknown section key"),
    };
    let section = self.sections.get_mut(&section_id).unwrap();
    section
      .grants
      .entry(options.target.id)
      .or_insert_with(|| RawForumRoleGrant {
        role: ForumRole::Moderator,
        user: options.target,
        start_time: now,
        granted_by: options.granter,
      });
  }

  fn delete_moderator(&mut self, options: &RawDeleteModeratorOptions) {
    let section_id = match &options.section {
      ForumSectionRef::Id(r) => r.id,
      ForumSectionRef::Key(r) => *self.sections_by_key.get(&r.key).expect("unknown section key"),
    };
    let section = self.sections.get_mut(&section_id).unwrap();
    section.grants.remove(&options.target.id);
  }

  fn get_role_grants(&self, options: &RawGetRoleGrantsOptions) -> Vec<RawForumRoleGrant> {
    let section_id = options.section.id;
    let section = self.sections.get(&section_id).expect("section not found");
    let mut grants = section.grants.values().cloned().collect::<Vec<_>>();
    grants.sort_by(|left, right| Ord::cmp(&left.start_time, &right.start_time));
    grants
  }
}

#[async_trait]
impl<TyClock, TyUuidGenerator> ForumStore for MemForumStore<TyClock, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn add_moderator(&self, options: &RawAddModeratorOptions) -> Result<(), RawAddModeratorError> {
    let now = self.clock.clock().now();
    let mut state = self.state.write().unwrap();
    state.add_moderator(now, options);
    Ok(())
  }

  async fn delete_moderator(&self, options: &RawDeleteModeratorOptions) -> Result<(), RawDeleteModeratorError> {
    let mut state = self.state.write().unwrap();
    state.delete_moderator(options);
    Ok(())
  }

  async fn get_sections(
    &self,
    options: &RawGetSectionsOptions,
  ) -> Result<Listing<RawForumSectionMeta>, RawGetSectionsError> {
    let state = self.state.read().unwrap();
    let res = state.get_sections(options);
    Ok(res)
  }

  async fn get_section_meta(
    &self,
    options: &GetForumSectionMetaOptions,
  ) -> Result<RawForumSectionMeta, GetSectionMetaError> {
    let state = self.state.read().unwrap();
    state.get_section_meta(options)
  }

  async fn get_threads(
    &self,
    options: &RawGetThreadsOptions,
  ) -> Result<Listing<RawForumThreadMeta>, RawGetThreadsError> {
    let state = self.state.read().unwrap();
    state.get_threads(options)
  }

  async fn get_thread_meta(
    &self,
    options: &RawGetForumThreadMetaOptions,
  ) -> Result<RawForumThreadMeta, GetThreadMetaError> {
    let state = self.state.read().unwrap();
    state.get_thread_meta(options)
  }

  async fn create_thread(
    &self,
    options: &RawCreateThreadOptions,
  ) -> Result<RawCreateForumThreadResult, RawCreateThreadError> {
    let now = self.clock.clock().now();
    let mut state = self.state.write().unwrap();
    let thread_id = ForumThreadId::from_uuid(self.uuid_generator.uuid_generator().next());
    let post_id = ForumPostId::from_uuid(self.uuid_generator.uuid_generator().next());
    let revision_id = ForumPostRevisionId::from_uuid(self.uuid_generator.uuid_generator().next());
    let res = state.create_thread(now, thread_id, post_id, revision_id, options);
    Ok(res)
  }

  async fn get_posts(&self, options: &RawGetPostsOptions) -> Result<Listing<RawShortForumPost>, RawGetPostsError> {
    let state = self.state.read().unwrap();
    state.get_posts(options)
  }

  async fn create_post(&self, options: &RawCreatePostOptions) -> Result<RawCreateForumPostResult, RawCreatePostError> {
    let now = self.clock.clock().now();
    let mut state = self.state.write().unwrap();
    let post_id = ForumPostId::from_uuid(self.uuid_generator.uuid_generator().next());
    let revision_id = ForumPostRevisionId::from_uuid(self.uuid_generator.uuid_generator().next());
    let res = state.create_post(now, post_id, revision_id, options);
    Ok(res)
  }

  async fn get_post(&self, options: &RawGetForumPostOptions) -> Result<RawForumPost, RawGetPostError> {
    let state = self.state.read().unwrap();
    state.get_post(options)
  }

  async fn get_role_grants(
    &self,
    options: &RawGetRoleGrantsOptions,
  ) -> Result<Vec<RawForumRoleGrant>, RawGetRoleGrantsError> {
    let state = self.state.read().unwrap();
    Ok(state.get_role_grants(options))
  }

  async fn create_post_revision(
    &self,
    options: &RawCreatePostRevisionOptions,
  ) -> Result<RawCreateForumPostRevisionResult, RawCreatePostRevisionError> {
    let now = self.clock.clock().now();
    let mut state = self.state.write().unwrap();
    let id = ForumPostRevisionId::from_uuid(self.uuid_generator.uuid_generator().next());
    let res = state.create_post_revision(now, id, options);
    Ok(res)
  }

  async fn upsert_system_section(
    &self,
    options: &UpsertSystemSectionOptions,
  ) -> Result<ForumSection, UpsertSystemSectionError> {
    let now = self.clock.clock().now();
    let mut state = self.state.write().unwrap();
    let id = ForumSectionId::from_uuid(self.uuid_generator.uuid_generator().next());
    let res = state.upsert_system_section(now, id, options);
    Ok(res)
  }
}

fn to_short_post(
  post: &MemForumPost,
  revisions: &HashMap<ForumPostRevisionId, RawForumPostRevision>,
) -> RawShortForumPost {
  let first = revisions.get(post.revisions.first().unwrap()).unwrap();
  let last = revisions.get(post.revisions.last().unwrap()).unwrap();

  RawShortForumPost {
    id: post.id,
    ctime: first.time,
    author: first.author.clone(),
    revisions: RawLatestForumPostRevisionListing {
      count: u32::try_from(post.revisions.len()).unwrap(),
      last: last.clone(),
    },
  }
}

#[cfg(test)]
mod test {
  use super::MemForumStore;
  use crate::test::TestApi;
  use eternaltwin_core::clock::VirtualClock;
  use eternaltwin_core::core::Instant;
  use eternaltwin_core::forum::ForumStore;
  use eternaltwin_core::user::UserStore;
  use eternaltwin_core::uuid::Uuid4Generator;
  use eternaltwin_user_store::mem::MemUserStore;
  use std::sync::Arc;

  fn make_test_api() -> TestApi<Arc<VirtualClock>, Arc<dyn ForumStore>, Arc<dyn UserStore>> {
    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let uuid_generator = Arc::new(Uuid4Generator);
    let forum_store: Arc<dyn ForumStore> = Arc::new(MemForumStore::new(clock.clone(), Arc::clone(&uuid_generator)));
    let user_store: Arc<dyn UserStore> = Arc::new(MemUserStore::new(clock.clone(), uuid_generator));

    TestApi {
      clock,
      forum_store,
      user_store,
    }
  }

  test_forum_store!(|| make_test_api());
}
