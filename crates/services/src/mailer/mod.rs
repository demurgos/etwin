pub mod send_email;

use crate::job::DynJobRuntimeExtra;
use crate::mailer::send_email::{SendEmailError, SendEmailWorker};
use eternaltwin_core::auth::AuthContext;
use eternaltwin_core::clock::{Clock, Scheduler, SchedulerRef};
use eternaltwin_core::core::{Duration, IdempotencyKey, Instant, Listing};
use eternaltwin_core::email::{EmailAddress, MailerRef};
pub use eternaltwin_core::forum::{CreatePostError, UpdatePostError};
use eternaltwin_core::job::{JobRuntime, JobStore, JobStoreRef, OpaqueTask, OpaqueValue};
use eternaltwin_core::mailer::store::get_outbound_email_requests::GetOutboundEmailRequests as StoreGetOutboundEmailRequests;
use eternaltwin_core::mailer::store::get_outbound_emails::GetOutboundEmails as StoreGetOutboundEmails;
use eternaltwin_core::mailer::store::{
  MailerStore, MailerStoreRef, OutboundEmail as StoreOutboundEmail, OutboundEmailRequest as StoreOutboundEmailRequest,
};
use eternaltwin_core::opentelemetry::DynTracer;
use eternaltwin_core::rate_limiter::{LeakyBucketRateLimiter, LeakyBucketRateLimiterConfig};
use eternaltwin_core::types::WeakError;
use eternaltwin_core::user::{UserStore, UserStoreRef};
use once_cell::sync::Lazy;
use opentelemetry::trace::Tracer;
use serde::{Deserialize, Serialize};
use serde_json::Value as JsonValue;
use std::future::Future;
use std::marker::PhantomData;
use std::num::NonZeroU64;
use std::pin::Pin;
use std::str::FromStr;
use std::sync::{Arc, Mutex};
use thiserror::Error;

#[allow(unused)]
static GLOBAL_MAILER_RATE_LIMITER: Lazy<Mutex<LeakyBucketRateLimiter>> = Lazy::new(|| {
  // Create 96 tokens/day (4 per hour), with a limit of 50 max
  let limiter = LeakyBucketRateLimiter::new(LeakyBucketRateLimiterConfig {
    limit: NonZeroU64::new(50).unwrap(),
    tokens_per_period: NonZeroU64::new(4).unwrap(),
    period_duration: Duration::from_hours(1),
  });
  Mutex::new(limiter)
});

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct SendMarktwinEmail {
  pub deadline: Instant,
  pub idempotency_key: IdempotencyKey,
  pub recipient: EmailAddress,
}

#[derive(Error, Debug)]
pub enum SendMarktwinEmailError {
  #[error("current actor does not have the permission to send outbound emails")]
  Forbidden,
  #[error("internal: failed to spawn email job")]
  SpawnJob(#[source] WeakError),
  #[error("internal: failed to join email job")]
  JoinJob(#[source] WeakError),
  #[error("send email job failed")]
  SendEmail(#[source] SendEmailError),
  #[error(transparent)]
  Other(WeakError),
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct GetOutboundEmails {
  pub time: Option<Instant>,
  pub offset: u32,
  pub limit: u32,
}

#[derive(Error, Debug)]
pub enum GetOutboundEmailsError {
  #[error("current actor does not have the permission to get outbound emails")]
  Forbidden,
  #[error(transparent)]
  Other(WeakError),
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct GetOutboundEmailRequests {
  pub time: Option<Instant>,
  pub offset: u32,
  pub limit: u32,
}

#[derive(Error, Debug)]
pub enum GetOutboundEmailRequestsError {
  #[error("current actor does not have the permission to get outbound email requests")]
  Forbidden,
  #[error(transparent)]
  Other(WeakError),
}

pub struct MailerService<'reg, TyClock, TyJobRuntimeExtra, TyJobStore, TyMailerStore, TyTracer, TyUserStore>
where
  TyClock: SchedulerRef,
  TyJobStore: JobStoreRef<OpaqueTask, OpaqueValue>,
  TyMailerStore: MailerStoreRef<JsonValue>,
  TyUserStore: UserStoreRef,
{
  clock: TyClock,
  job_runtime: Arc<JobRuntime<'reg, TyClock, TyJobStore, TyTracer, TyJobRuntimeExtra>>,
  mailer_store: TyMailerStore,
  #[allow(unused)]
  user_store: TyUserStore,
}

pub type DynMailerService = MailerService<
  'static,
  Arc<dyn Scheduler<Timer = Pin<Box<dyn Future<Output = ()> + Send>>>>,
  DynJobRuntimeExtra,
  Arc<dyn JobStore<OpaqueTask, OpaqueValue>>,
  Arc<dyn MailerStore<JsonValue>>,
  DynTracer,
  Arc<dyn UserStore>,
>;

impl<'reg, TyClock, TyJobRuntimeExtra, TyJobStore, TyMailerStore, TyTracer, TyUserStore>
  MailerService<'reg, TyClock, TyJobRuntimeExtra, TyJobStore, TyMailerStore, TyTracer, TyUserStore>
where
  TyClock: SchedulerRef,
  TyJobRuntimeExtra: Send + Sync + MailerStoreRef<JsonValue> + MailerRef,
  TyJobStore: JobStoreRef<OpaqueTask, OpaqueValue>,
  TyMailerStore: MailerStoreRef<JsonValue>,
  TyTracer: Tracer + Sync,
  TyTracer::Span: Send + Sync + 'static,
  TyUserStore: UserStoreRef,
{
  #[allow(clippy::too_many_arguments)]
  pub fn new(
    clock: TyClock,
    job_runtime: Arc<JobRuntime<'reg, TyClock, TyJobStore, TyTracer, TyJobRuntimeExtra>>,
    mailer_store: TyMailerStore,
    user_store: TyUserStore,
  ) -> Self {
    Self {
      clock,
      job_runtime,
      mailer_store,
      user_store,
    }
  }

  pub async fn send_marktwin_email(
    &self,
    acx: &AuthContext,
    cmd: &SendMarktwinEmail,
  ) -> Result<StoreOutboundEmail<JsonValue>, SendMarktwinEmailError> {
    let user = match acx {
      AuthContext::User(acx) if acx.is_administrator => &acx.user,
      _ => return Err(SendMarktwinEmailError::Forbidden),
    };

    let job = SendEmailWorker::Start {
      idempotency_key: cmd.idempotency_key,
      now: self.clock.clock().now(),
      actor: user.as_ref(),
      deadline: cmd.deadline,
      sender: EmailAddress::from_str("contact@eternaltwin.org").unwrap(),
      recipient: cmd.recipient.clone(),
    };
    let (_job, handle) = self
      .job_runtime
      .spawn(job, None)
      .await
      .map_err(SendMarktwinEmailError::SpawnJob)?;
    let email = self
      .job_runtime
      .join(handle)
      .await
      .map_err(SendMarktwinEmailError::JoinJob)?
      .map_err(SendMarktwinEmailError::SendEmail)?;
    Ok(email)
  }

  pub async fn get_outbound_emails(
    &self,
    acx: &AuthContext,
    query: &GetOutboundEmails,
  ) -> Result<Listing<StoreOutboundEmail<JsonValue>>, GetOutboundEmailsError> {
    let now = self.clock.clock().now();
    let _user = match acx {
      AuthContext::User(acx) if acx.is_administrator => &acx.user,
      _ => return Err(GetOutboundEmailsError::Forbidden),
    };

    let emails: Listing<StoreOutboundEmail<JsonValue>> = self
      .mailer_store
      .mailer_store()
      .get_outbound_emails(StoreGetOutboundEmails {
        now,
        time: query.time.unwrap_or(now),
        limit: query.limit,
        opaque: PhantomData::<JsonValue>,
      })
      .await
      .map_err(|e| GetOutboundEmailsError::Other(WeakError::wrap(e)))?;

    Ok(emails)
  }

  pub async fn get_outbound_email_requests(
    &self,
    acx: &AuthContext,
    query: &GetOutboundEmailRequests,
  ) -> Result<Listing<StoreOutboundEmailRequest>, GetOutboundEmailRequestsError> {
    let now = self.clock.clock().now();
    let _user = match acx {
      AuthContext::User(acx) if acx.is_administrator => &acx.user,
      _ => return Err(GetOutboundEmailRequestsError::Forbidden),
    };

    let emails: Listing<StoreOutboundEmailRequest> = self
      .mailer_store
      .mailer_store()
      .get_outbound_email_requests(StoreGetOutboundEmailRequests {
        now,
        time: query.time.unwrap_or(now),
        limit: query.limit,
      })
      .await
      .map_err(|e| GetOutboundEmailRequestsError::Other(WeakError::wrap(e)))?;

    Ok(emails)
  }
}
