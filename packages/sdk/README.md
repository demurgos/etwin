# Eternaltwin SDK Core

This package contains all the logic for the Eternaltwin SDK, allowing
integration between the Eternaltwin binary and Node. This package does not
include the executable itself.
