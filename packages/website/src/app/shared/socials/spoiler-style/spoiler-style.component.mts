import { CommonModule } from "@angular/common";
import { Component, Input } from "@angular/core";

@Component({
  selector: "spoiler-style",
  standalone: true,
  imports: [CommonModule],
  templateUrl: "./spoiler-style.component.html",
  styleUrls: ["./spoiler-style.component.scss"],
})
export class SpoilerStyleComponent {
  @Input() isBlock: Boolean = false;
}