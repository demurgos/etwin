import { LiteralUnionType } from "kryo/literal-union";
import { $Ucs2String } from "kryo/ucs2-string";

/**
 * A Dinoparc server.
 */
export type DinoparcServer = "dinoparc.com" | "en.dinoparc.com" | "sp.dinoparc.com";

export const $DinoparcServer: LiteralUnionType<DinoparcServer> = new LiteralUnionType({
  type: $Ucs2String,
  values: ["dinoparc.com", "en.dinoparc.com", "sp.dinoparc.com"],
});
