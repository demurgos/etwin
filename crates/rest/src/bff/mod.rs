//! Backend-for-frontend
//! This modules contains frontend-specific code.

pub mod actions;
pub mod oauth;
