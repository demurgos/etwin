import {Component} from "@angular/core";
import {FormGroup} from "@angular/forms";
import {JobListing} from "@eternaltwin/core/job/job-listing";
import {map, NEVER, Observable, of} from "rxjs";

import {JobService} from "../../modules/job/job.service.mjs";

@Component({
  selector: "et-admin-jobs-view",
  template:
`
  <etwin-main-layout>
    <h1>Jobs</h1>
    <form
      method="POST"
      enctype="application/x-www-form-urlencoded"
      action="#"
      [formGroup]="form"
      (ngSubmit)="onSubmit($event)"
    >
      <input type="submit" class="btn primary" name="query"
             value="Query"/>
    </form>


    @if (result$ | async; as result) {
      <pre>{{result | json}}</pre>
    }
    @if (error$ | async; as error) {
      <pre>{{error | json}}</pre>
    }

  </etwin-main-layout>
`,
  styleUrls: [],
})
export class JobsViewComponent {
  public readonly form: FormGroup;
  public error$: Observable<string>;
  public result$: Observable<JobListing>;
  #jobService: JobService;

  constructor(jobService: JobService) {
    this.form = new FormGroup({});
    this.result$ = NEVER;
    this.error$ = NEVER;
    this.#jobService = jobService;
  }

  onSubmit(ev: SubmitEvent): void {
    ev.preventDefault();
    this.result$ = NEVER;
    this.error$ = NEVER;
    const $req = this.#jobService.getJobs({limit: 10});
    this.result$ = $req;
  }
}
