import { Injectable, NgModule } from "@angular/core";
import { Resolve, Router, RouterModule, Routes } from "@angular/router";
import { AuthContext } from "@eternaltwin/core/auth/auth-context";
import { AuthType } from "@eternaltwin/core/auth/auth-type";
import { $CompleteUser, CompleteUser } from "@eternaltwin/core/user/complete-user";
import { MaybeCompleteUser } from "@eternaltwin/core/user/maybe-complete-user";
import {NOOP_CONTEXT} from "kryo";
import { firstValueFrom, Observable, of as rxOf, throwError as rxThrowError } from "rxjs";
import { catchError as rxCatchError, first as rxFirst, map as rxMap, switchMap as rxSwitchMap } from "rxjs/operators";

import { AuthService } from "../../modules/auth/auth.service.mjs";
import { UserService } from "../../modules/user/user.service.mjs";
import { SettingsViewComponent } from "./settings-view.component.mjs";

@Injectable()
export class UserResolverService implements Resolve<CompleteUser | null> {
  private readonly auth: AuthService;
  private readonly user: UserService;

  constructor(auth: AuthService, router: Router, user: UserService) {
    this.auth = auth;
    this.user = user;
  }

  async resolve(): Promise<CompleteUser | null> {
    const completeCurrentUser$ = this.auth.auth().pipe(
      rxFirst(),
      rxSwitchMap((curUser: AuthContext) => {
        if (curUser.type !== AuthType.User) {
          return rxThrowError(() => new Error("Unauthenticated"));
        }
        return this.user.getUserById(curUser.user.id);
      }),
      rxMap((user: MaybeCompleteUser | null): CompleteUser => {
        const ERROR_MESSAGE = "AssertionError: Retrieving the current user should yield a complete user";
        if (user === null) {
          throw new Error(ERROR_MESSAGE);
        }
        const {ok, value} = $CompleteUser.test(NOOP_CONTEXT, user);
        if (ok) {
          return value;
        } else {
          throw new Error(ERROR_MESSAGE);
        }
      }),
      rxCatchError((err: Error): Observable<null> => {
        return rxOf(null);
      }),
    );
    return firstValueFrom(completeCurrentUser$);
  }
}

const routes: Routes = [
  {
    path: "",
    component: SettingsViewComponent,
    resolve: {
      user: UserResolverService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [UserResolverService],
})
export class SettingsRoutingModule {
}
