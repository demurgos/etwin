# Edition-icons-a7-fontello webfont

## About the font

The Edition-icons webfont is a font for the Etwin project and all associated projects and websites. It contains symbols and glyphs only. All these elements are custom-made for the project or picked from open-source libraries availables through the Fontello service.

Advantages of an icons font include vector-based scalability, small size, universal browsers support. Limitations include monocromacy, fill-content vectors only, fixed-size artwork container.

## How to edit the font

- Vector artworks are available in `/eternaltwin/assets/edition-icons-a7-fontello.ai` for reference and use. 
- go to https://fontello.com/ and upload the webfont `edition-icons-a7-fontello.svg` into the web app. All the icons currently in the font file will appear under "Custom Icons"
- To add icons, simply drag-and-drop each .svg file into the web app to create a new custom icon. The svg file shall be 24x24 px and contains black-filled, no contour, tracings only. The icon name and keywords shall match its content. Default Hex code shall not be changed.
- Add the font name `edition-icons-a7-fontello` in the name input (top right)
- Select all the icons in the "Custom Icons". Icons from other free libraries may be added as well, be sure to check the license if you do so. Click the "Download webfont" button (top right), replace all five font files in the repo with the freshly downloaded font files.
- All icons included in the font are now available for use by imputting their hex code into the html templates (ie. `&#xe808;` for quote icon).
