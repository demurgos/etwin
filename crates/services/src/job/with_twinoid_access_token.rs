use crate::job::error_sleep;
use eternaltwin_core::job::{Sleep, Task, TaskCx, TaskPoll};
use eternaltwin_core::oauth::TwinoidAccessToken;
use eternaltwin_core::twinoid::store::{GetTwinoidAccessToken, TwinoidStoreRef};
use eternaltwin_core::twinoid::TwinoidStore;
use eternaltwin_core::types::WeakError;
use serde::{Deserialize, Serialize};
use std::ops::ControlFlow;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum WithTwinoidAccessToken<Task> {
  GetToken {
    timeout: Option<Sleep>,
    failures: u32,
    last_error: Option<WeakError>,
    task: Task,
  },
  UseToken {
    token: TwinoidAccessToken,
    failures: u32,
    last_error: Option<WeakError>,
    task: Task,
  },
}

pub struct TwinoidTokenCx<'cx, Cx> {
  pub inner: &'cx mut Cx,
  pub token: TwinoidAccessToken,
}

pub enum TwinoidTokenResult<T> {
  Valid(T),
  MaybeValid(T),
  Invalid,
}

impl<TyTask> WithTwinoidAccessToken<TyTask> {
  #[allow(unused)]
  pub async fn poll<'cx, Cx, Out>(&mut self, cx: &'cx mut Cx) -> TaskPoll<Out>
  where
    Cx: TaskCx,
    Cx::Extra: TwinoidStoreRef,
    TyTask: Clone + for<'a> Task<TwinoidTokenCx<'a, Cx>, Output = TwinoidTokenResult<Out>>,
  {
    let mut iter = 0;
    loop {
      iter += 1;
      if iter >= 50 {
        return TaskPoll::Pending;
      }
      match self.poll_iter(cx).await {
        ControlFlow::Continue(()) => continue,
        ControlFlow::Break(out) => return out,
      }
    }
  }
  /// Run a single `poll` iteration
  async fn poll_iter<'cx, Cx, Out>(&mut self, cx: &'cx mut Cx) -> ControlFlow<TaskPoll<Out>, ()>
  where
    Cx: TaskCx,
    Cx::Extra: TwinoidStoreRef,
    TyTask: Clone + Task<TwinoidTokenCx<'cx, Cx>, Output = TwinoidTokenResult<Out>>,
  {
    match self {
      Self::GetToken {
        timeout,
        failures,
        last_error,
        task,
      } => {
        if let Some(sleep) = timeout {
          match sleep.poll(cx).await {
            TaskPoll::Pending => return ControlFlow::Break(TaskPoll::Pending),
            TaskPoll::Ready(()) => *timeout = None,
          }
        }
        let token = cx
          .extra()
          .twinoid_store()
          .get_twinoid_access_token(GetTwinoidAccessToken {
            time: cx.now(),
            twinoid_user: None,
          })
          .await
          .map_err(WeakError::wrap);
        match token {
          Ok(token) => {
            *self = Self::UseToken {
              token: TwinoidAccessToken {
                key: token.key,
                created_at: token.created_at,
                accessed_at: token.accessed_at,
                expires_at: token.expires_at,
                twinoid_user_id: token.twinoid_user_id,
              },
              failures: *failures,
              last_error: last_error.clone(),
              task: task.clone(),
            }
          }
          Err(e) => {
            eprintln!("[ArchiveTwinoidUsersWorker::write] {e:#}");
            *failures += 1;
            *timeout = Some(cx.sleep(error_sleep(*failures)));
            *last_error = Some(e);
          }
        }
      }
      Self::UseToken {
        token,
        failures,
        last_error,
        task,
      } => {
        match task
          .poll(TwinoidTokenCx {
            inner: cx,
            token: token.clone(),
          })
          .await
        {
          TaskPoll::Pending => return ControlFlow::Break(TaskPoll::Pending),
          TaskPoll::Ready(TwinoidTokenResult::Valid(out)) => {
            *failures = 0;
            *last_error = None;
            return ControlFlow::Break(TaskPoll::Ready(out));
          }
          TaskPoll::Ready(TwinoidTokenResult::MaybeValid(out)) => return ControlFlow::Break(TaskPoll::Ready(out)),
          TaskPoll::Ready(TwinoidTokenResult::Invalid) => {
            eprintln!("[WithTwinoidAccessToken::invalid_token]");
            *failures += 1;
            *self = Self::GetToken {
              timeout: None,
              failures: *failures,
              last_error: last_error.clone(),
              task: task.clone(),
            };
          }
        }
      }
    };
    ControlFlow::Continue(())
  }
}
