import {RfcOauthAccessTokenKey} from "@eternaltwin/core/oauth/rfc-oauth-access-token-key";
import {$ShortUser, ShortUser} from "@eternaltwin/core/user/short-user";
import {UserId} from "@eternaltwin/core/user/user-id";
import {Observable} from "rxjs";

import {HttpHandler} from "../http-handler.mjs";
import {RequestType} from "../request-type.mjs";

export const TYPE: RequestType.GetUserById = RequestType.GetUserById as const;

export interface Request {
  readonly type: typeof TYPE;
  readonly userId: UserId;
  readonly auth: RfcOauthAccessTokenKey;
}

export type Response = ShortUser;

export function handle(handler: HttpHandler, query: Request): Observable<Response> {
  return handler("get", ["users", query.userId], {auth: query.auth, resType: $ShortUser});
}
