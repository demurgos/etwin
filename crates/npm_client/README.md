# npm registry client

This npm registry client is currently used for the needs of [https://eternaltwin.org/](https://eternaltwin.org/).
If you want to claim the name, send an email at `contact@eternaltwin.org`.

This client is based on [the official API documentation](https://github.com/npm/registry/blob/master/docs/REGISTRY-API.md).
