<?php declare(strict_types=1);

namespace Eternaltwin;

final class Cli {
  public static function main() {
    $exe = \Eternaltwin\Exe::path();
    $args = $_SERVER['argv'];
    $command = [$exe];
    foreach ($args as $i => $arg) {
      if ($i > 0) {
        $command[] = $arg;
      }
    }
    $descriptors = [
      0 => STDIN,
      1 => STDOUT,
      2 => STDERR,
    ];
    $pipes = [];
    $proc = proc_open(
      $command,
      $descriptors,
      $pipes,
      null,
      null,
      [
        "suppress_errors" => false,
        "bypass_shell" => true,
        "blocking_pipes" => false,
        "create_process_group" => true,
        "create_new_console" => false,
      ]
    );

    if ($proc === false) {
      fwrite(STDERR, 'failed to spawn Eternaltwin child process' . PHP_EOL);
      die(1);
    }

//    $pipePairs = [
//      [STDIN, $pipes[0]],
//      [$pipes[1], STDOUT],
//      [$pipes[2], STDERR],
//    ];

    $exitCode = null;

    $cycle = 0;
    while($exitCode === null) {
      $status = proc_get_status($proc);
      if ($status["running"]) {
//        $read = [];
//        $write = [];
//        $except = null;
//        foreach ($pipePairs as $pair) {
//          $read[] = $pair[0];
//          $read[] = $pair[1];
//          stream_set_blocking($pair[0], false);
//          stream_set_blocking($pair[1], false);
//        }
//        $changedCount = stream_select($read, $write, $except, 0, 200000);
//        if ($changedCount === false) {
//          fwrite(STDERR, 'failed to select I/O streams (may be caused by a signal)' . PHP_EOL);
//          die(1);
//        }
        $cycle += 1;
//        foreach ($pipePairs as $idx => $pair) {
//          $from = $pair[0];
//          $to = $pair[1];
//          if (in_array($from, $read, true)) {
//            self::pipe($from, $to);
//          }
//        }
      } else {
        $exitCode = $status["exitcode"];
        if (!($exitCode >= 0)) {
          fwrite(STDERR, 'failed to close Eternaltwin child process' . PHP_EOL);
          die(1);
        }
        break;
      }
    }

    die($exitCode);
  }

  private static function pipe($from, $to) {
    stream_copy_to_stream($from, $to, 4096, 0);
    fflush($to);
  }
}
