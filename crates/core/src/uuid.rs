use crate::rng::{Rng, RngRef};
use std::ops::Deref;
use uuid::Uuid;

/// Infinite UUID generator
pub trait UuidGenerator: Send + Sync {
  fn next(&self) -> Uuid;
}

/// Like [`Deref`], but the target has the bound [`UuidGenerator`]
pub trait UuidGeneratorRef: Send + Sync {
  type UuidGenerator: UuidGenerator + ?Sized;

  fn uuid_generator(&self) -> &Self::UuidGenerator;
}

impl<TyRef> UuidGeneratorRef for TyRef
where
  TyRef: Deref + Send + Sync,
  TyRef::Target: UuidGenerator,
{
  type UuidGenerator = TyRef::Target;

  fn uuid_generator(&self) -> &Self::UuidGenerator {
    self.deref()
  }
}

// todo: remove `Uuid4Generator` and replace it with `Uuid4GeneratorNew`
pub struct Uuid4Generator;

impl UuidGeneratorRef for Uuid4Generator {
  type UuidGenerator = Self;

  fn uuid_generator(&self) -> &Self::UuidGenerator {
    self
  }
}

impl UuidGenerator for Uuid4Generator {
  fn next(&self) -> Uuid {
    Uuid::new_v4()
  }
}

pub struct Uuid4GeneratorNew<TyRng>
where
  TyRng: RngRef,
{
  rng: TyRng,
}

impl<TyRng> Uuid4GeneratorNew<TyRng>
where
  TyRng: RngRef,
{
  pub fn new(rng: TyRng) -> Self {
    Self { rng }
  }
}

impl<TyRng> UuidGenerator for Uuid4GeneratorNew<TyRng>
where
  TyRng: RngRef,
{
  /// TODO: Make `next` fallible (rename to `try_next`)
  fn next(&self) -> Uuid {
    let mut bytes = [0u8; 16];
    self
      .rng
      .rng()
      .try_fill_bytes(&mut bytes)
      .expect("uuid rng does not fail");
    uuid::Builder::from_random_bytes(bytes).into_uuid()
  }
}
